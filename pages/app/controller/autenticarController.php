<?php
	require_once("../includes/conexion.class.php");
	require_once("../includes/constantes.php");	
	require_once("../includes/captcha/securimage.php");	
	require_once("../model/usuarioModel.php");
?>
<?php
if(isset($_POST["submit"])){

	try{

		$NU_Cedula 	= $_POST["NU_Cedula"];
		$AF_Clave 	= $_POST["AF_Clave"];
		$code 		= $_POST['code'];

		$objConexion	= new conexion(SERVER,USER,PASS,DB);
		$objUsuario		= new Usuario();
		$objImgCode 	= new Securimage();

		$existe		= $objUsuario->existeUsuario($objConexion,$NU_Cedula);
		$valid 		= $objImgCode->check($code);		

		if ($existe)
		{
			$encontrado	= $objUsuario->validarUsuario($objConexion,$NU_Cedula,$AF_Clave);

			if (($encontrado) and ($valid))
			{
				$RS 			= $objUsuario->buscarUsuario($objConexion,$NU_Cedula);
				$NU_IdUsuario 	= $objConexion->obtenerElemento($RS,0,"NU_IdUsuario");
				$AL_Nombre 		= $objConexion->obtenerElemento($RS,0,"AL_Nombre");
				$AL_Apellido 	= $objConexion->obtenerElemento($RS,0,"AL_Apellido");			
				$AF_Correo 		= $objConexion->obtenerElemento($RS,0,"AF_Correo");
				$AF_Telefono 	= $objConexion->obtenerElemento($RS,0,"AF_Telefono"); 
				$BI_Admin 		= $objConexion->obtenerElemento($RS,0,"BI_Admin"); 
				$NU_Activo 		= $objConexion->obtenerElemento($RS,0,"NU_Activo"); 
	
				if ($NU_Activo==1){
					session_start();
					$_SESSION["NU_IdUsuario"] 		= $NU_IdUsuario;
					$_SESSION["NU_Cedula"] 			= $NU_Cedula;
					$_SESSION["AL_NombreApellido"] 	= $AL_Nombre.' '.$AL_Apellido;
					$_SESSION['rol_NU_IdRol'] 		= $rol_NU_IdRol;
					$_SESSION['BI_Admin'] 			= $BI_Admin;
					
					if ($AF_Correo=='' or $AF_Telefono==''){
						$mensaje="IMPORTANTE: Recuerde actualizar su Perfil para poder continuar.";
						header("Location: ../views/index.php?mensaje=$mensaje");
					}else{
						header("Location: ../views/index.php");
					}
				}else{
					$mensaje="IMPORTANTE: Usted ya no pertenece a VENALCASA.";
					header("Location: ../../login.php?mensaje=$mensaje");
				}
			}else{
				$mensaje="El nombre de Usuario, Clave o Codigo Invalido.";
				header("Location: ../../login.php?mensaje=$mensaje");	
			}
			
		}else{
				$mensaje="Los datos introducidos no Existen en nuestra Base de Datos";
				header("Location: ../../login.php?mensaje=$mensaje");			
			//}
		}
	}
	
	catch(Exception $e){
		$mensaje=$e->getMessage();
	}	
}
?>