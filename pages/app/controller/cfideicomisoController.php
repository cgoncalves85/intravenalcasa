<?php
	session_start();
	
	require_once("../includes/constantes.php");
	require_once("../includes/conexion.class.php");

	$objConexion= new conexion(SERVER,USER,PASS,DB);
	
	require_once('../model/cfideicomisoModel.php');
	require_once('../model/usuarioModel.php');

	$objCFideicomiso 	= new CFideicomiso();
	$objUsuario		    = new Usuario();

?>
<?php
////////////////////// CASO DE USO: CREAR CONSTANCIA DE FIDEICOMISO ///////

	if ($_POST['origen']=='CFideicomiso')
	{
		
		$RSUsuario 		= $objUsuario->buscarUsuario($objConexion,$_SESSION['NU_Cedula']);
		$cantRSUsuario 	= $objConexion->cantidadRegistros($RSUsuario);
		
		if ($cantRSUsuario>0){
			$NU_IdUsuario 				= $objConexion->obtenerElemento($RSUsuario,0,"NU_IdUsuario");
			$NU_Cedula					= $objConexion->obtenerElemento($RSUsuario,0,"NU_Cedula");
			$BS_TotalPrestaciones		= $objConexion->obtenerElemento($RSUsuario,0,"BS_TotalPrestaciones");
			$BS_TotalAnticipo 			= $objConexion->obtenerElemento($RSUsuario,0,"BS_TotalAnticipo");
			$BS_TotalSaldo 				= $objConexion->obtenerElemento($RSUsuario,0,"BS_TotalSaldo");
			$BS_TotalDisponible 		= $objConexion->obtenerElemento($RSUsuario,0,"BS_TotalDisponible");
			
		}

		$NU_IdCFideicomiso 	= ($objCFideicomiso->obtenerUltimo($objConexion)+1);
		$NU_IdTipoCFideicomiso	= $_POST["NU_IdTipoCFideicomiso"];

		$objCFideicomiso->insertar($objConexion,$_SESSION["NU_IdUsuario"],$BS_TotalPrestaciones,$BS_TotalAnticipo,$BS_TotalSaldo,$BS_TotalDisponible,$NU_IdTipoCFideicomiso);

		header("Location: ../views/cfideicomiso/Constancia_Fideicomiso".$NU_IdTipoCFideicomiso.".php?NU_IdCFideicomiso=".$NU_IdCFideicomiso);
	}
	
	
?>
