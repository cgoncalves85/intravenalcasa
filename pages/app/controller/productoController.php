<?php
	require_once('../controller/sessionController.php'); 
	require_once('../model/productoModel.php'); 
?>
<?php

////////////////////// CASO DE USO AGREGAR MEDIDA ///////
	if ($_POST['origen']=='agregar_producto')
	{
		$objProducto 		= new Producto();

		
		$medida_NU_IdMedida = $_POST["medida_NU_IdMedida"];
		$AF_NombreProducto	= $_POST["AF_NombreProducto"];
		$NU_Max 			= $_POST["NU_Max"];
		$NU_Min 			= $_POST["NU_Min"];
		$NU_Salto 			= $_POST["NU_Salto"];
		$NU_Contenido 		= $_POST["NU_Contenido"];
		$BS_PrecioUnitario  = $_POST["BS_PrecioUnitario"];
		$NU_BultoCaja 		= $_POST["NU_BultoCaja"];
		$NU_IdMedidaBulto 	= $_POST["NU_IdMedidaBulto"];
		$AF_Foto		 	= $_POST["AF_Foto"];
		

		$objProducto->insertar($objConexion,
								$medida_NU_IdMedida,
								$AF_NombreProducto,
								$NU_Max,$NU_Min,$NU_Salto,
								$NU_Contenido,
								$BS_PrecioUnitario,
								$NU_BultoCaja,
								$NU_IdMedidaBulto,$AF_Foto);
			
	
			$mensaje='El Producto se agregó Correctamente.';
			header("Location: ../views/productos/index.php?mensaje=$mensaje");
		
	}
	else {
		$mensaje='El Producto NO se agregó.';
		header("Location: ../views/productos/index.php?mensaje=$mensaje");
	}
		
?>