<?php
require_once('../controller/sessionController.php');
////////////////////// RECUPERAR CLAVE DE USUARIO /////////////////////////////////
if ($_POST['origen']=='descuento_nomina') {
  $NU_IdMercado = $_POST['mercado_NU_IdMercado'];
  $gerencia_NU_IdGerencia = $_POST['gerencia_NU_IdGerencia'];		
  $formato = $_POST['formato'];
  switch($formato) {
    case 'pdf':  header("Location: ../views/consulta/2/documento_pdf.php?NU_IdMercado=".$NU_IdMercado."&gerencia_NU_IdGerencia=".$gerencia_NU_IdGerencia); break;
    case 'txt':  header("Location: ../views/consulta/2/documento_txt.php?NU_IdMercado=".$NU_IdMercado); break;
    case 'xls':  header("Location: ../views/consulta/2/documento_xls.php?NU_IdMercado=".$NU_IdMercado."&gerencia_NU_IdGerencia=".$gerencia_NU_IdGerencia); break;
  }
}

if ($_POST['origen']=='RelacionOrdenes') {
  $NU_IdMercado	= $_POST['mercado_NU_IdMercado'];
  $gerencia_NU_IdGerencia = $_POST['gerencia_NU_IdGerencia'];		
  $formato = $_POST['formato'];
  
  switch($formato) {
    case 'pdf':  header("Location: ../views/consulta/4/documento_pdf.php?NU_IdMercado=".$NU_IdMercado); break;
    case 'txt':  header("Location: ../views/consulta/4/documento_txt.php?NU_IdMercado=".$NU_IdMercado); break;
    case 'xls':  header("Location: ../views/consulta/4/documento_xls.php?NU_IdMercado=".$NU_IdMercado."&gerencia_NU_IdGerencia=".$gerencia_NU_IdGerencia); break;
  }
}