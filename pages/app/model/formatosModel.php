<?php 
class Formatos{
	private $NU_IdFormato;
	private $NU_IdTipoFormato;
	private $NO_Formato;
	private $DI_Formato;		

	function insertar($objConexion,$NU_IdTipoFormato,$NO_Formato,$DI_Formato){
		$this->NU_IdTipoFormato	= $NU_IdTipoFormato;
		$this->NO_Formato		= $NO_Formato;
		$this->DI_Formato		= $DI_Formato;

		$query="INSERT INTO formatos (NU_IdTipoFormato, NO_Formato, DI_Formato)
				VALUES('".$this->NU_IdTipoFormato."', '".$this->NO_Formato."', '".$this->DI_Formato."')";
						
		$resultado=$objConexion->ejecutar($query);
		
		return true;
		
	}
	
	function listarTipos($objConexion){
		$query="SELECT *
				FROM formatos_tipo
				ORDER BY NU_IdTipoFormato ASC";
		$resultado=$objConexion->ejecutar($query);
		return $resultado;		
	}	
	
	function listarFormato1($objConexion){
		$query="SELECT *
				FROM formatos
				WHERE NU_IdTipoFormato = 1
				ORDER BY NU_IdFormato ASC";
		$resultado=$objConexion->ejecutar($query);
		return $resultado;		
	}
	
	function listarFormato2($objConexion){
		$query="SELECT *
				FROM formatos
				WHERE NU_IdTipoFormato = 2
				ORDER BY NU_IdFormato ASC";
		$resultado=$objConexion->ejecutar($query);
		return $resultado;		
	}

	function listarFormato3($objConexion){
		$query="SELECT *
				FROM formatos
				WHERE NU_IdTipoFormato = 3
				ORDER BY NU_IdFormato ASC";
		$resultado=$objConexion->ejecutar($query);
		return $resultado;		
	}

	function listarFormato4($objConexion){
		$query="SELECT *
				FROM formatos
				WHERE NU_IdTipoFormato = 4
				ORDER BY NU_IdFormato ASC";
		$resultado=$objConexion->ejecutar($query);
		return $resultado;		
	}	

	function obtenerUltimo($objConexion){
		$query="SELECT MAX(NU_IdFormato) as id
				FROM formatos";
		$resultado=$objConexion->ejecutar($query);	
		//return $resultado;

		if($objConexion->cantidadRegistros($resultado)>0){
			$this->id=$objConexion->obtenerElemento($resultado,0,'id');
		}
		return $this->id;	
	}	
	
}
?>