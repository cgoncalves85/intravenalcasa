<?php 
class CFideicomiso{
	private $NU_IdCFideicomiso;
	private $usuario_NU_IdUsuario;	
	private $NU_IdTipoCFideicomiso;
	private $BS_TotalPrestaciones;
	private $BS_TotalAnticipo;
	private $BS_TotalSaldo;
	private $BS_TotalDisponible;
	private $FE_CargaDatos;
	private $FE_Solicitud;
	private $FE_Registro;
	
	function listarConstancias($objConexion, $usuario_NU_IdUsuario){
		$this->usuario_NU_IdUsuario = $usuario_NU_IdUsuario;
		$query="SELECT CF.*, CFF.AL_Tipo
				FROM cfideicomiso AS CF
				LEFT JOIN cfideicomiso_tipo AS CFF ON (CF.NU_IdTipoCFideicomiso=CFF.NU_IdTipoCFideicomiso)
				WHERE usuario_NU_IdUsuario=".$this->usuario_NU_IdUsuario."
				ORDER BY NU_IdCFideicomiso DESC";
		$resultado=$objConexion->ejecutar($query);
		return $resultado;		
	}

	function obtenerUltimo($objConexion){
		$this->NU_IdCFideicomiso = 0;
		
		$query="SELECT * 
				FROM cfideicomiso
				ORDER BY NU_IdCFideicomiso DESC
				LIMIT 1";		
				
		$resultado	= $objConexion->ejecutar($query);

		if($objConexion->cantidadRegistros($resultado) > 0){
			$this->NU_IdCFideicomiso = $objConexion->obtenerElemento($resultado,0,'NU_IdCFideicomiso');
		}

		return $this->NU_IdCFideicomiso;	
	}
		
	function insertar($objConexion,$usuario_NU_IdUsuario,$BS_TotalPrestaciones,$BS_TotalAnticipo,$BS_TotalSaldo,$BS_TotalDisponible,$NU_IdTipoCFideicomiso){
		$this->usuario_NU_IdUsuario		= $usuario_NU_IdUsuario;
		$this->BS_TotalPrestaciones		= $BS_TotalPrestaciones;
		$this->BS_TotalAnticipo			= $BS_TotalAnticipo;
		$this->BS_TotalSaldo			= $BS_TotalSaldo;
		$this->BS_TotalDisponible		= $BS_TotalDisponible;
		$this->NU_IdTipoCFideicomiso	= $NU_IdTipoCFideicomiso;		
		$this->FE_Solicitud				= date("Y-m-d");
		
		$query="INSERT INTO cfideicomiso (
					usuario_NU_IdUsuario, 
					BS_TotalPrestaciones,
					BS_TotalAnticipo,
					BS_TotalSaldo,
					BS_TotalDisponible, 
					NU_IdTipoCFideicomiso, 
					FE_Solicitud)
				VALUES (
					".$this->usuario_NU_IdUsuario.",
					'".$this->BS_TotalPrestaciones."',
					".$this->BS_TotalAnticipo.",
					".$this->BS_TotalSaldo.",
					'".$this->BS_TotalDisponible."',
					".$this->NU_IdTipoCFideicomiso.",
					'".$this->FE_Solicitud."')";
		
		$resultado=$objConexion->ejecutar($query);
		
		return true;
	}
	
	function eliminar($objConexion,$usuario_NU_IdUsuario,$BS_TotalPrestaciones,$BS_TotalAnticipo,$BS_TotalSaldo,$BS_TotalDisponible,$NU_IdTipoCFideicomiso){
		$this->usuario_NU_IdUsuario		= $usuario_NU_IdUsuario;
		$this->BS_TotalPrestaciones		= $BS_TotalPrestaciones;
		$this->BS_TotalAnticipo			= $BS_TotalAnticipo;
		$this->BS_TotalSaldo			= $BS_TotalSaldo;
		$this->BS_TotalDisponible		= $BS_TotalDisponible;
		$this->NU_IdTipoCFideicomiso	= $NU_IdTipoCFideicomiso;		
		$this->FE_Solicitud				= date("Y-m-d");
		
		$query="DELETE FROM cfideicomiso (
					usuario_NU_IdUsuario, 
					BS_TotalPrestaciones,
					BS_TotalAnticipo,
					BS_TotalSaldo,
					BS_TotalDisponible, 
					NU_IdTipoCFideicomiso, 
					FE_Solicitud)
				VALUES (
					".$this->usuario_NU_IdUsuario.",
					'".$this->BS_TotalPrestaciones."',
					".$this->BS_TotalAnticipo.",
					".$this->BS_TotalSaldo.",
					'".$this->BS_TotalDisponible."',
					".$this->NU_IdTipoCFideicomiso.",
					'".$this->FE_Solicitud."')";
		
		$resultado=$objConexion->ejecutar($query);
		
		return true;
	}	
	
	function buscar($objConexion,$NU_IdCFideicomiso){
		$this->NU_IdCFideicomiso=$NU_IdCFideicomiso;
		$query="SELECT CF.*, U.AL_Apellido, U.AL_Nombre, U.NU_Cedula
				FROM cfideicomiso AS CF
				LEFT JOIN usuario AS U ON (CF.usuario_NU_IdUsuario=U.NU_IdUsuario)
				WHERE CF.NU_IdCFideicomiso=".$this->NU_IdCFideicomiso;
		$resultado=$objConexion->ejecutar($query);
		return $resultado;		
	}	
		
}
?>