<?php 
	require_once('../../controller/sessionController.php'); 
?>
<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-10">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/shop-homepage.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

	<!-- Custom JavaScript -->
	<script type="text/javascript" src="../../js/jquery.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.1/respond.min.js"></script>
    <![endif]-->


<script type="text/javascript">
    function abrir_dialog() {
		var mensaje = "<?php echo $_GET['mensaje']; ?>";
		if(mensaje){
		  $( "#dialog" ).dialog({
			  show: "blind",
			  hide: "explode",
			  modal: true,
			  buttons: {
				Aceptar: function() {
				  $( this ).dialog( "close" );
				}
			  }
		  });
		}
    };
</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <span class="ui-icon ui-icon-circle-check"></span>
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>

    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1"></div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../../img/head.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1"></div>			
			
		</div>
	</div>
	<br>
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../../img/head3.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
	</div>

	<br>
    <!-- Page Content -->
    <div class="container">

      <div class="row">
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
			<div class="col-sm-10 col-lg-10 col-md-10">

			<nav class="navbar navbar-default" role="navigation">
  				<!-- El logotipo y el icono que despliega el menú se agrupan
       			para mostrarlos mejor en los dispositivos móviles -->
  				<div class="navbar-header">
    				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      					<span class="sr-only">Desplegar navegación</span>
      					<span class="icon-bar"></span>
      					<span class="icon-bar"></span>
      					<span class="icon-bar"></span>
    				</button>
    				<a class="navbar-brand" href="../index.php"><img class="img-responsive" src="../../images/mercado1.jpg" alt=""></a>
  				</div>
 				<!-- Agrupar los enlaces de navegación, los formularios y cualquier
       			otro elemento que se pueda ocultar al minimizar la barra -->
  				<div class="collapse navbar-collapse navbar-ex1-collapse">
    			<ul class="nav navbar-nav">
      				<li><a href="../ctrabajo/index.php">Constancia de Trabajo</a></li>
      				<li><a href="../cfideicomiso/index.php">Consulta Fideicomiso</a></li>
      				<li><a href="../egreso/index.php">Constancia de Egreso</a></li>
					<li class="active">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Formatos y Manuales <b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="../formatos/index.php">Formatos RRHH</a></li>
							<li class="divider"></li>
							<li><a href="../formatos/index2.php">Tripticos F.A.S</a></li>
							<li class="divider"></li>
							<li><a href="../formatos/index3.php">Manuales Normas y Proc.</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Perfil de Usuario <b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="../usuario/perfil/index.php">Ver Perfil</a></li>
							<li class="divider"></li>
							<li><a href="../salirView.php">Cerrar Sesión</a></li>
						</ul>
					</li>
    			</ul>
  				</div>
			</nav>
		</div>
		<div class="col-sm-1 col-lg-1 col-md-1"></div>				


			<div class="col-sm-12 col-lg-12 col-md-12"></div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
			<div class="col-sm-10 col-lg-10 col-md-10">
				<p class="navbar-text pull-left">
  					Bienvenido(a), <a href="../usuario/perfil/index.php" class="navbar-link"><?php echo $_SESSION['AL_NombreApellido']; ?></a>
				</p>
			</div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
			
			<div class="col-sm-12 col-lg-12 col-md-12"></div>
			
			<div class="col-sm-1 col-lg-1 col-md-1"></div>		
		
			<div class="col-sm-10 col-lg-10 col-md-10">
			<div class="panel panel-primary">
				
				<div class="panel-body">
					<div class="col-sm-1 col-lg-1 col-md-1"></div>					
					<div class="col-sm-10 col-lg-10 col-md-10">
							<br>
							<table class="table table-bordered">
							<tr align="center" style="background-color: #FFF; color: #000; box-shadow: 0 .1em 1em gray">
								<td><strong>MANUALES DE NORMAS Y PROCEDIMIENTOS.</strong></td>
							</tr>
							</table>

							<table class="table table-bordered">
								<thead>
								
								<tr align="center" style="background-color: #9C0707; color: #FFF">
									<td style="vertical-align:middle;" ><strong> N° </strong></td>
									<td style="vertical-align:middle;" ><strong>Nombre o Descripción del Manual</strong></td>
									<td style="vertical-align:middle;" ><strong>Ver</strong></td>
								</tr>
								</thead>
								<tbody align="center">
									<tr>
									<td style="vertical-align:middle;" ><strong>1</strong></td>
									<td style="vertical-align:middle;" >Manual de Normas y Procedimientos de Presentación de Puntos de Cuenta a Junta Directiva</td>
									<td style="vertical-align:middle;" ><a href="manuales/MANUAL DE NORMAS Y PROCEDIMIENTOS DE PRESENTACION DE PUNTOS DE CUENTAS A JUNTA DIRECTIVA.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 
									</tr>

									<tr>
									<td style="vertical-align:middle;" ><strong>2</strong></td>									
									<td style="vertical-align:middle;" >Manual de Normas y Procedimientos - Recepción y Envio de Correspondencia</td>
									<td style="vertical-align:middle;" ><a href="manuales/MANUAL DE NORMAS Y PROCEDIMIENTOS DE RECEPCION Y ENVIO DE CORRESPONDENCIA.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 
									</tr>

									<tr>
									<td style="vertical-align:middle;" ><strong>3</strong></td>
									<td style="vertical-align:middle;" >Manual de Normas y Procedimientos para el Mantenimiento Automotor</td>
									<td style="vertical-align:middle;" ><a href="manuales/MANUAL DE NORMAS Y PROCEDIMIENTOS PARA EL MANTENIMIENTO AUTOMOTOR.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 
									</tr>

									<tr>
									<td style="vertical-align:middle;" ><strong>4</strong></td>
									<td style="vertical-align:middle;" >Manual de Normas y Procedimientos para el personal de Vigilancia - Seguridad Venalcasa</td>
									<td style="vertical-align:middle;" ><a href="manuales/MANUAL DE NORMAS Y PROCEDIMIENTOS PARA EL PERSONAL DE VIGILANCIA DE SEGURIDAD DE VENALCASA.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 
									</tr>

									<tr>
									<td style="vertical-align:middle;" ><strong>5</strong></td>
									<td style="vertical-align:middle;" >Manual de Normas y Procedimientos - Producción</td>
									<td style="vertical-align:middle;" ><a href="manuales/MANUAL DE PROCEDIMIENTOS PARA LA  PRODUCCION.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 
									</tr>

									<tr>
									<td style="vertical-align:middle;" ><strong>6</strong></td>
									<td style="vertical-align:middle;" >Manual Descriptivo de Puestos de Trabajo</td>
									<td style="vertical-align:middle;" ><a href="manuales/MANUAL DESCRIPTIVO DE PUESTOS DE TRABAJOS.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 									
									</tr>

									<tr>
									<td style="vertical-align:middle;" ><strong>7</strong></td>
									<td style="vertical-align:middle;" >Manual de Formulación POAI (2da, Versión)</td>
									<td style="vertical-align:middle;" ><a href="manuales/Manual Formulacion POAI 2DA VERSION.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 
									</tr>
									
									<tr>
									<td style="vertical-align:middle;" ><strong>8</strong></td>
									<td style="vertical-align:middle;" >Manual para la Ejecución del Presupuesto</td>
									<td style="vertical-align:middle;" ><a href="manuales/MANUAL PARA LA EJECUCION DEL PRESUPUESTO.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 
									</tr>

									<tr>
									<td style="vertical-align:middle;" ><strong>9</strong></td>
									<td style="vertical-align:middle;" >Manual para las Modificaciones Presupuestarias</td>
									<td style="vertical-align:middle;" ><a href="manuales/MANUAL PARA LAS  MODIFICACIONES PRESUPUESTARIAS.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 
									</tr>
									
									<tr>
									<td style="vertical-align:middle;" ><strong>10</strong></td>
									<td style="vertical-align:middle;" >Manual de Seguimiento POAI</td>
									<td style="vertical-align:middle;" ><a href="manuales/Manual Seguimiento POAI.pdf" target="_blank"><img src="../../images/ver.png" class="img-responsive"></a> </td> 
									</tr>
								</tbody>
							</table>
							<div align="center">
							<ul class="pagination">
 								<li><a href="index3.php">&laquo;</a></li>
  								<li><a href="index3.php">1</a></li>
  								<li class="active"><a href="index4.php">2</a></li>
  								<li><a href="index5.php">3</a></li>
  								<li><a href="index6.php">4</a></li>
  								<li><a href="index5.php">&raquo;</a></li>
							</ul>
							</div>
							
							
					<div class="col-sm-1 col-lg-1 col-md-1"></div>
				</div>
			</div>						
						
			<div class="col-sm-1 col-lg-1 col-md-1"></div>						
				
		</div>			
	</div>
	
	<div class="col-sm-12 col-lg-12 col-md-12"></div>


    <!-- /.container -->

        <!-- Footer -->
        <footer>
        <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-primary">
					<div class="panel-body">
						<p align="center"><strong>
						Venezolana de Alimentos La Casa - &copy;2015<br>
						Oficina de Tecnología de la Información
						</strong></p>
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div></div>
        </footer>
	
	<!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>
</body>

</html>
