﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/formatosModel.php'); 	

	$objFormatos 	= new Formatos();	
	
	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="center"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php 
			include "nav.php";
			$titulo = $_GET['titulo'];
			$id = $_GET['id'];
		
		?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header"><?php echo $titulo ?></h4>

						<div class="panel panel-default">
							<div class="panel-heading">
								Documentos &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $titulo ?>
							</div>						

							<div class="panel-body">
								<div class="row" align="center">

									<div class="dataTable_wrapper" style="margin:50px; width:70%">
						
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<?php 
													$k = 0;
													$dir="listarFormato".$id;
													$rsFormatos		= $objFormatos->$dir ($objConexion);
													$cantFormatos	= $objConexion->cantidadRegistros($rsFormatos);
												?> 											
												<tr >
													<td style="vertical-align:middle; width:40px" ><strong>N°</strong></td>
													<td style="vertical-align:middle;" ><strong>Nombre</strong></td>
													<td style="vertical-align:middle; width:40px"><strong>Ver</strong></td>
													<td style="vertical-align:middle; width:90px"><strong>Descargar</strong></td>
												</tr>
											</thead>
     
											<tbody>
												<?php
													$cont=1;
													for($i=0; $i<$cantFormatos; $i++){
														$NU_IdFormato		= $objConexion->obtenerElemento($rsFormatos,$i,"NU_IdFormato");
														$NO_Formato			= $objConexion->obtenerElemento($rsFormatos,$i,"NO_Formato");
														$DI_Formato			= $objConexion->obtenerElemento($rsFormatos,$i,"DI_Formato");
													
												?>
													<tr align="center">
														<td style="vertical-align:middle;" ><strong><?php echo $cont ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $NO_Formato ?></strong></td>
														<td style="vertical-align:middle;" ><a href="<?php echo $DI_Formato ?>" target="_blank"><span class="glyphicon glyphicon-search" title="Ver Documento"></span></a></td>
														<td style="vertical-align:middle;" ><a href="download.php?f=<?php echo $DI_Formato ?>"><span class="glyphicon glyphicon-download-alt" title="Descargar Documento"></span></a></td>
													</tr>
												<?php
													$cont++;
													}
												?>          
											</tbody>
										</table>
									</div>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>	
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
