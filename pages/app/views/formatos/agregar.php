﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/formatosModel.php'); 	

	$objFormatos 	= new Formatos();	
	
	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}	
	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="center"><br><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Documentos</h4>
						<form role="form" id="form" method="POST" action="../../controller/formatosController.php" enctype="multipart/form-data">
							<div class="panel panel-default">
								<div class="panel-heading">
									Configuración &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Documento.
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-1"></div>
										<div class="col-lg-11">	
											<br>
											<div class="form-group">
												<div class="col-lg-6">
													<label for="NO_Formato">Nombre del Documento :</label><br>
													<input type="text" name="NO_Formato" required id="NO_Formato" class="form-control">
												</div>
												<div class="col-lg-4">
													<label for="NU_IdTipoFormato">Categoria :</label><br>
													<select name="NU_IdTipoFormato" required id="NU_IdTipoFormato" class="form-control placeholder">
														<option value=""></option>
														<?php
															$RS	 = $objFormatos->listarTipos($objConexion);
															$cant = $objConexion->CantidadRegistros($RS);																								
															for($i=0; $i<$cant; $i++){
																$value	= $objConexion->obtenerElemento($RS,$i,"NU_IdTipoFormato");
																$des	= $objConexion->obtenerElemento($RS,$i,"AL_Tipo");
																$selected	= "";
																	
																echo "<option value=".$value." ".$selected.">".$des."</option>";
															}																																	
														?>				
													</select>
												</div>
												
												<div class="col-lg-5">
													<br>
													<label class="control-label" for="DI_Formato">Archivo :</label>			
													<input type="file" name="DI_Formato" id="DI_Formato" class="form-control">										
												</div>
												<div class="col-lg-5" align="right">
													<br><br>
													<input name="origen" type="hidden" id="origen" value="agregar1">
													
													<input name="Agregar_Documento" type="submit" class="btn btn-primary"  value="Agregar Documento" id="Agregar_Documento">
												</div>
												<div class="col-lg-10">&nbsp;</div>
												
												
											</div>
										
										</div>
									</div>
								</div>
							</div>
						</form>	
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
