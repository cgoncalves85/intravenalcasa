        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="col-lg-6">
			<div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="../images/intrave.png" class="img-responsive" /></a>
            </div>
			</div>
            <!-- /.navbar-header -->
			<div class="col-lg-6">
			<br>
            <ul class="nav navbar-top-links navbar-right">
				Bienvenido(a) :  <?php echo $_SESSION['AL_NombreApellido']; ?>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="usuario/perfil/index.php"><i class="fa fa-user fa-fw"></i> Perfil de Usuario</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="SalirView.php"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
			</div>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
							<form role="form" method="POST" action="buscar/busqueda.php" class="form-horizontal">
								<div class="input-group custom-search-form">
									<input type="text" name="search" class="form-control" placeholder="Buscar...">
									<span class="input-group-btn">
										<button name="Buscar" class="btn btn-default" type="submit">
											<i class="fa fa-search"></i>
										</button>
									</span>
								</div>
							</form>						
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-home fa-lg"></i>&nbsp;&nbsp;&nbsp; Menú Principal</a>
                        </li>
						<?php if ($BI_ASGestionMercado==1){  ?>  
                        <li>
                            <a href="#"><i class="fa fa-user fa-lg"></i>&nbsp;&nbsp;&nbsp; Usuarios<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="configuracion/usuario/index.php"> Resetear Usuario</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						<?php } ?> 
                        <li>
                            <a href="#"><i class="fa fa-shopping-cart fa-lg"></i>&nbsp;&nbsp;&nbsp; Mercado Corporativo<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <?php if ($BI_ASGestionMercado==1){  ?>
								<li>
                                    <a href="#"><i class="fa fa-gears fa-lg"></i>&nbsp;&nbsp;&nbsp; Configuración <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="medida/index.php">Unidades de Medida</a>
                                        </li>
                                        <li>
                                            <a href="productos/index.php">Productos</a>
                                        </li>
										<li>
                                            <a href="generar_pago/index.php">Generar TXT-Mercado</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>	
                                <li>
                                    <a href="apertura_mercado/index.php">Gestionar Mercado</a>
                                </li>
								<?php } ?>  
                                <li>
                                    <a href="historial/index.php">Mis Compras</a>
                                </li>
                                <li>
                                    <a href="pedido/index.php">Comprar</a>
                                </li>
								<?php if ($BI_ASGestionMercado==1){  ?> 
                                <li>
									<a href="#"><i class="fa fa-binoculars fa-lg"></i>&nbsp;&nbsp;&nbsp; Verificar Compras<span class="fa arrow"></span></a>
									<ul class="nav nav-second-level">
										<li>
											<a href="verificar_compra/index.php">Usuarios que NO Compraron</a>
										</li>
										<li>
											<a href="verificar_compra/index2.php">Usuarios que SI Compraron</a>
										</li>
									</ul>
									<!-- /.nav-second-level -->									
                                   
                                </li>
								<?php } ?> 
								<?php if ($BI_ASGestionMercado==1){  ?>  
								<li>
									<a href="#"><i class="fa fa-search fa-lg"></i>&nbsp;&nbsp;&nbsp; Consultas<span class="fa arrow"></span></a>
									<ul class="nav nav-second-level">
										<li>
											<a href="consulta/1/index.php">Solicitud de Inventario. VPO</a>
										</li>
										<li>
											<a href="consulta/2/index.php">Relación de Descuento de Ord.Compra</a>
										</li>
										<li>
											<a href="consulta/3/index.php">Inventario Desglosado</a>
										</li>
										<li>
											<a href="consulta/4/index.php">Relación de Ordenes de Compras</a>
										</li>										
									</ul>
									<!-- /.nav-second-level -->
								</li>
								<?php } ?> 
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						
                        <li>
                            <a href="#"><i class="fa fa-group fa-lg"></i>&nbsp;&nbsp;&nbsp; Servicios al Empleado<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <?php if ($BI_ASParametrosSis==1){  ?>
								<li>
                                    <a href="#"><i class="fa fa-gears fa-lg"></i>&nbsp;&nbsp;&nbsp; Configuración <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Personal (Listado de Usuarios)</a>
                                        </li>
                                        <li>
                                            <a href="formatos/agregar.php">Cargar Documentos</a>
                                        </li>										
										
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>	
								<?php } ?> 
                                <li>
                                    <a href="ctrabajo/index.php">Constancia de <br>Trabajo</a>
                                </li>
                                <li>
                                    <a href="cfideicomiso/index.php">Consulta de <br>Fideicomiso</a>
                                </li>
                                <li>
                                    <a href="#">Solicitud de <br>Vacaciones</a>
                                </li>	
                                <li>
                                    <a href="egreso/index.php">Constancia de <br>Egreso</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>	
						
						<li>
							<a href="#"><i class="fa fa-file fa-lg"></i>&nbsp;&nbsp;&nbsp; Documentos<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="formatos/index.php?titulo=Formatos - RRHH&id=1">Formatos - RRHH</a>
								</li>
								<li>
									<a href="formatos/index.php?titulo=Formatos - Fondo Autogestionado de Salud&id=2">Formatos - Fondo Autogestionado de Salud</a>
								</li>
								<li>
									<a href="formatos/index.php?titulo=Tripticos Informativos - F.A.S&id=3">Tripticos Informativos - F.A.S</a>
								</li>
								<li>
									<a href="formatos/index.php?titulo=Manuales de Normas y Procedimientos&id=4">Manuales de Normas y Procedimientos</a>
								</li>	
							</ul>
							<!-- /.nav-second-level -->
						</li>
						<?php if ($BI_GPNotaCredito==1){  ?>
                        <li>
                            <a href="#"><i class="fa fa-gear fa-lg"></i>&nbsp;&nbsp;&nbsp; Soporte Técnico<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#"><i class="fa fa-gears fa-lg"></i>&nbsp;&nbsp;&nbsp; Configuración <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Personal ST</a>
                                        </li>
                                        <li>
                                            <a href="#">Categorias y Subcategorias</a>
                                        </li>										
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
								<li>
									<a href="solicitudes/index.php">Generar Solicitud</a>
								</li>
								<li>
									<a href="solicitudes/listado/index.php">Mis Solicitudes</a>
								</li>
							
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-lg"></i> Reportes del Sistema<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Usuarios</a>
                                </li>
                                <li>
                                    <a href="#">Productos</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-gears fa-lg"></i> Configuración</a>
                        </li>
						<?php } ?> 
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>