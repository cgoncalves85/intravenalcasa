﻿<?php 
	require_once('../../../controller/sessionController.php'); 
	
	$RSUsuario 	= $objUsuario->listarU($objConexion);
	$cRSUsuario = $objConexion->cantidadRegistros($RSUsuario);
?>
<html>
<head>
<title>Mercado Corporativo de VENALCASA</title>
<link rel="stylesheet" type="text/css" href="../../../css/estilo.css">

<script type="text/javascript" src="../../../js/jquery.js"></script>
<script type="text/javascript" src="../../../js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../../js/dataTables.tableTools.js"></script>
<script type="text/javascript" src="../../../js/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="../../../css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="../../../css/dataTables.tableTools.css">
<link rel="stylesheet" href="../../../css/jquery-ui.css" />
<script LANGUAGE="JavaScript">
	function confirmEdit()
	{
	var agree=confirm("¿Estas seguro de editar?");
	if (agree)
		return true ;
	else
		return false ;
	}

	function confirmBorrar()
	{
	var agree=confirm("¿Estas seguro de borrar?");
	if (agree)
		return true ;
	else
		return false ;
	}
</script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "../../../video/copy_csv_xls_pdf.swf",			
            "aButtons": [
                "copy",
                "print",
                {
                    "sExtends":    "collection",
                    "sButtonText": "Guardar Como",
                    "aButtons":    [ "csv", "xls", "pdf" ]
                }
            ]
        }
    } );
} );
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td height="25" bgcolor="#CCCCCC" class="Negrita" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USUARIOS</td>
    </tr>
  </table>
  <br>

<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#D90F0F">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
         
            <tr>
              <td width="6%" class="Textonegro"><a href="add.php"><img src="../../../images/bton_add.gif" width="35" height="31" border="0"></a></td>
              <td width="15%" class="Textonegro"><div align="left"><a href="#">Agregar un Usuario</a></div></td>
              <td width="79%" class="Textonegro"><input name="button2" type="button" class="BotonRojo" id="button2" value="      [ Atr&aacute;s ]      " onClick="javascript:window.location='../index.php'" /></td>
            </tr>
	
            <tr>
              <td colspan="3" align="center">
			  
			  <table width="95%" border="1" cellpadding="2" cellspacing="2" id="example" class="display TablaRojaGrid">
              <thead>
                <tr class="TablaRojaGridTRTitulo">
                  <td class="TablaRojaGridTD">Ubicación</td>
                  <td class="TablaRojaGridTD">Nombre y Apellido </td>
                  <td class="TablaRojaGridTD">Cédula</td>
				  <td class="TablaRojaGridTD">Correo</td>
				  <td class="TablaRojaGridTD" width="6%">Resetear</td>
                  <td class="TablaRojaGridTD" width="6%">Editar</td>
                  <td class="TablaRojaGridTD" width="7%">Borrar</td>
                </tr>
               </thead> 
				<tfoot>               
                 <tr class="TablaRojaGridTRTitulo">
                    <td class="TablaRojaGridTD">Ubicación</td>
                  <td class="TablaRojaGridTD">Nombre y Apellido </td>
                  <td class="TablaRojaGridTD">Cédula</td>
				  <td class="TablaRojaGridTD">Correo</td>
				  <td class="TablaRojaGridTD" width="6%">Resetear</td>
                  <td class="TablaRojaGridTD" width="6%">Editar</td>
                  <td class="TablaRojaGridTD" width="7%">Borrar</td>
                </tr>               
				</tfoot>  
                <tbody>             
               <?php 
			   	if ($cRSUsuario>0){
					for ($i=0; $i<$cRSUsuario; $i++){
						$NU_IdUsuario 		= $objConexion->obtenerElemento($RSUsuario,$i,'NU_IdUsuario');
						$AL_NombreSede 		= $objConexion->obtenerElemento($RSUsuario,$i,'AL_NombreSede');
						$AL_NombreGerencia 	= $objConexion->obtenerElemento($RSUsuario,$i,'AL_NombreGerencia');
						$ubicacion 			= $AL_NombreSede.', '.$AL_NombreGerencia;
						$NU_Cedula 			= $objConexion->obtenerElemento($RSUsuario,$i,'NU_Cedula');
						$AL_Nombre 			= $objConexion->obtenerElemento($RSUsuario,$i,'AL_Nombre');
						$AL_Apellido 		= $objConexion->obtenerElemento($RSUsuario,$i,'AL_Apellido');
						$NombreApellido 	= $AL_Nombre.' '.$AL_Apellido;
						$AF_Correo 			= $objConexion->obtenerElemento($RSUsuario,$i,'AF_Correo');
						$AF_Telefono		= $objConexion->obtenerElemento($RSUsuario,$i,'AF_Telefono');						
			   ?>
                 <tr>
                   <td>&nbsp;<?php echo $ubicacion; ?></td>
                   <td>&nbsp;<?php echo $NombreApellido; ?></td>
                   <td>&nbsp;<?php echo $NU_Cedula; ?></td>
                   <td>&nbsp;<?php echo $AF_Correo; ?></td>
                   <td><div align="center"><a href="../../usuario/reset/reset.php?NU_IdUsuario=<?php echo $NU_IdUsuario; ?>" onClick="return confirmReset()"><img src="../../../images/bton_reset.gif" width="35" height="34" border="0"></a></div></td>
                   <td><div align="center"><a href="#" onClick="return confirmEdit()"><img src="../../../images/bton_edit.gif" width="35" height="31" border="0"></a></div></td>
                    <td><div align="center"><a href="#" onClick="return confirmBorrar()"><img src="../../../images/bton_del.gif" width="35" height="31" border="0"></a></div></td>
                  </tr>
				<?php 
					} 
				}
				?>			  
				</tbody>
              </table>
			    
			  		  </td>
            </tr>
            <tr>
              <td colspan="3" align="center" class="Textonegro">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3" align="center" class="Textonegro">&nbsp;</td>
            </tr>
          </table>
        </td>
      </tr>
  </table>
<tr>
    <td></td>
  </tr>
</table>
		
</body>
</html>
