﻿<?php 
	require_once('../../../controller/sessionController.php'); 
	
	$RSU 		= $objUsuario->listarU($objConexion);
	$cRSUsuario = $objConexion->cantidadRegistros($RSU);

	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}	
	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
	<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
		<p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
	</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Resetear Usuario</h4>
						<div class="panel panel-default">
							<div class="panel-heading">
								Listados de Usuarios del Sistema.
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-10">	
										<br>
										<?php if ($cRSUsuario>0){ ?>
										<div>
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead align="center">
													<tr>
														<td style="vertical-align:middle;" ><strong>Nombres y Apellidos</strong></td>
														<td style="vertical-align:middle;" ><strong>Cédula</strong></td>
														<td style="vertical-align:middle;" ><strong>Correo Electrónico</strong></td>
														<td style="vertical-align:middle;" ><strong>Resetear</strong></td>
														<td style="vertical-align:middle;" ><strong>Editar</strong></td>
														<td style="vertical-align:middle;" ><strong>Borrar</strong></td>	
													</tr>
												</thead>
												<tbody align="center">
													<?php
														for ($i=0; $i<$cRSUsuario; $i++){
														$NU_IdUsuario 		= $objConexion->obtenerElemento($RSU,$i,'NU_IdUsuario');
														$NU_Cedula 			= $objConexion->obtenerElemento($RSU,$i,'NU_Cedula');
														$AL_Nombre 			= $objConexion->obtenerElemento($RSU,$i,'AL_Nombre');
														$AL_Apellido 		= $objConexion->obtenerElemento($RSU,$i,'AL_Apellido');
														$NombreApellido 	= $AL_Nombre.' '.$AL_Apellido;
														$AF_Correo 			= $objConexion->obtenerElemento($RSU,$i,'AF_Correo');
														$AF_Telefono		= $objConexion->obtenerElemento($RSU,$i,'AF_Telefono');	
													?>
													<tr>
														<td style="vertical-align:middle;" ><?php echo $NombreApellido; ?></td>
														<td style="vertical-align:middle;" ><?php echo $NU_Cedula; ?></td>
														<td style="vertical-align:middle;" ><?php echo $AF_Correo; ?></td>
														<td style="vertical-align:middle;" ><a href="../../usuario/reset/reset.php?NU_IdUsuario=<?php echo $NU_IdUsuario; ?>" onClick="return confirmReset()"><span class="glyphicon glyphicon-refresh" title="Reset Usuario"></span></a></td>
														<td style="vertical-align:middle;" ><a href="#" onClick="return confirmEdit()"><span class="glyphicon glyphicon-edit" title="Editar Usuario"></span></a></td>	
														<td style="vertical-align:middle;" ><a href="#" onClick="return confirmBorrar()"><span class="glyphicon glyphicon-trash" title="Eliminar Usuario"></span></a></td>														
													</tr>
													<?php
													}
													?>          
												</tbody>
											</table>
										</div>
										<?php
											}else{ echo '<tr align="center"><td>No se encontraron registros.</td></tr>'; 
											}
										?>	
									</div>
									<div class="col-lg-1"></div>
								</div>
							</div>
						</div>

					</div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
