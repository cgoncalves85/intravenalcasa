<?php 
	require_once('../../controller/sessionController.php');
	require_once('../../model/usuarioModel.php');
	require_once('../../includes/tcpdf/tcpdf.php'); 
	require_once('../../includes/NumLetras.php');	
	

	$FE_Ingreso 				= $_POST['FE_Ingreso'];
	$FE_Egreso 					= $_POST['FE_Egreso'];
	$NU_IdTipo 					= $_POST['NU_IdTipo'];
	$Cedula 					= $_POST['NU_Cedula'];
	$Cargo 						= $_POST['Cargo'];
	$Cargo 						= utf8_decode(strtoupper($Cargo));

	$objUsuario		= new Usuario();
	
	$RS 		= $objUsuario->buscarUsuario($objConexion,$Cedula);
	$cantRS 	= $objConexion->cantidadRegistros($RS);

	///// CONVIERTE FECHA 1980-07-04 A 04/07/1980 (FORMATO ESPANOL)
	function setFechaNOSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}	
	
	///////////////////////// DATOS CONSULTAS ///////////////////////////////////////////
	$AL_Nombre 					= utf8_decode(strtoupper($objConexion->obtenerElemento($RS,0,"AL_Nombre")));
	$AL_Apellido 				= utf8_decode(strtoupper($objConexion->obtenerElemento($RS,0,"AL_Apellido")));
	$NU_Cedula 					= number_format($objConexion->obtenerElemento($RS,0,"NU_Cedula"),0,'','.');
	//$FE_Ingreso 				= setFechaNOSQL($objConexion->obtenerElemento($RS,0,"FE_Ingreso"));
	//$cargo_NU_IdCargo 			= trim(utf8_decode(strtoupper($objConexion->obtenerElemento($RS,0,"cargo_NU_IdCargo"))));
	$AL_Adscripcion 			= trim(utf8_decode(strtoupper($objConexion->obtenerElemento($RS,0,"AL_Adscripcion"))));	
	$BS_SalarioBasico 			= $objConexion->obtenerElemento($RS,0,"BS_SalarioBasico");
	$BS_PrimaAntiguedad 		= $objConexion->obtenerElemento($RS,0,"BS_PrimaAntiguedad");	
	$BS_PrimaResponsabilidad 	= $objConexion->obtenerElemento($RS,0,"BS_PrimaResponsabilidad");
	$BS_PrimaEspecializacion 	= $objConexion->obtenerElemento($RS,0,"BS_PrimaEspecializacion");
	$BS_PrimaTransporte 		= $objConexion->obtenerElemento($RS,0,"BS_PrimaTransporte");
	$BS_PrimaOtra 				= $objConexion->obtenerElemento($RS,0,"BS_PrimaOtra");	
	/////////// DATOS PARA LA INTEGRAL ///////////////////////////////////////
	$SueldoIntegral				= $BS_SalarioBasico+$BS_PrimaAntiguedad+$BS_PrimaResponsabilidad+$BS_PrimaEspecializacion+$BS_PrimaTransporte+$BS_PrimaOtra;
	$SalarioIntegralLetras		= utf8_decode(strtoupper(convertir_a_letras($SueldoIntegral)));	
	$SueldoIntegral				= number_format($SueldoIntegral,2,',','.');
	/////////// DATOS PARA LA DESGLOSADA ///////////////////////////////////
	$BS_SalarioBasicoLetras			= utf8_decode(strtoupper(convertir_a_letras($BS_SalarioBasico)));
	$prima1 = '';
	$prima2 = '';	
	$prima3 = '';	

	
	if ($BS_PrimaResponsabilidad!='0.00'){
		$UTResponsabilidad				= $BS_PrimaResponsabilidad/177;
		$UTResponsabilidadLetras		= strtoupper(convertir_a_letras($UTResponsabilidad));
		$UTResponsabilidadLetras		= str_replace('BOLIVARES','',$UTResponsabilidadLetras);
		$BS_PrimaResponsabilidadLetras 	= strtoupper(convertir_a_letras($BS_PrimaResponsabilidad));
		$prima1 = ', m�s una Prima de Responsabilidad de <b>'.$UTResponsabilidadLetras.' UNIDADES TRIBUTARIAS ('.$UTResponsabilidad.' U.T.)</b> equivalentes a <b>'.$BS_PrimaResponsabilidadLetras.' (Bs. '.number_format($BS_PrimaResponsabilidad,2,',','.').')</b>';
	}
	if ($BS_PrimaAntiguedad!='0.00'){
		$BS_PrimaAntiguedadLetras	 	= utf8_decode(strtoupper(convertir_a_letras($BS_PrimaAntiguedad)));
		$prima2 = ', m�s una Prima de Antiguedad de <b>'.$BS_PrimaAntiguedadLetras.' (Bs. '.number_format($BS_PrimaAntiguedad,2,',','.').')</b>';
	}
	if ($BS_PrimaEspecializacion!='0.00'){
		$BS_PrimaEspecializacionLetras 	= utf8_decode(strtoupper(convertir_a_letras($BS_PrimaEspecializacion)));
		$prima3 = ', m�s una Prima de Especializaci�n de <b>'.$BS_PrimaEspecializacionLetras.' ('.number_format($BS_PrimaEspecializacion,2,',','.').')</b>';	
	}
	
	$BS_SalarioBasico					= number_format($BS_SalarioBasico,2,',','.');	
	////////////// FECHA DE LA CONSTANCIA //////////////////////////////////////	
	$FE_Solicitud = date("Y-m-d");
	$FE_Solicitud = explode("-",$FE_Solicitud);
	$mes = $FE_Solicitud[1];
	
	$meses = array('01' => 'Enero','02' => 'Febrero','03' => 'Marzo','04' => 'Abril','05' => 'Mayo','06' => 'Junio','07' => 'Julio','08' => 'Agosto','09' => 'Septiembre','10' => 'Octubre','11' => 'Noviembre','12' => 'Diciembre');

	$dia = $FE_Solicitud[2];
	$mes = $meses[$mes];
	$ano = $FE_Solicitud[0];
?>
<?php

class MYPDF extends TCPDF {

    public function Header() {
		$this->SetMargins('25', '', '20');
		$this->Image('../../images/head.jpg', 25, 20, 160, 0, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->SetFillColor(232,232,232);
		$this->Line(25,33,185,33);
		$this->SetFont('helvetica','B',10);
		$this->Ln(14);
		$this->Cell(0,0,'Rif. G-20008504-5',0,0,'L');
		$this->Ln(20);
		$this->SetFont('helvetica','BI',14);
		$this->Cell(0,0,'C O N S T A N C I A',0,0,'C');
		$this->Ln(20);		
    }

    public function Footer() {
		$this->SetMargins('25', '', '20');
		$this->SetY(-65);
		$this->SetFont('helvetica','BI',12);
		$this->Ln(5);
		$this->Cell(0,0,'LIC. ISIS LEDEZMA',0,0,'C');
		$this->Ln(5);
		$this->Cell(0,0,'DIRECTOR(A) GENERAL DE RECURSOS HUMANOS',0,0,'C');
		$this->Ln(20);
		$this->SetFont('helvetica','',8);
		//$this->Cell(0,0,utf8_encode('Valido por 90 d�as a partir de la presente fecha.'),0,0,'C');
		//$this->Cell(0,0,utf8_encode('Verifique la presente en: www.venalcasa.net.ve/servirrhh e introduzca: ').$_SESSION['AF_Codigo'],0,0,'C');
		$this->Ln(3);
		$this->SetFillColor(232,232,232);	
		$this->Line(25,268,185,268);
		$this->Ln(5);
		$this->Cell(0,0,'VENEZOLANA DE ALIMENTOS LA CASA, S.A.',0,0,'C');
		$this->Ln(3);
		$this->Cell(0,0,utf8_encode('Av. Sucre, entre la Polic�a Nacional Bolivariana y Calle Mauri, Centro de Acopio Catia Parroquia Sucre'),0,0,'C');
		$this->Ln(3);
		$this->Cell(0,0,'Municipio Libertador. Caracas - Venezuela',0,0,'C');
		$this->Ln(3);
		$this->Cell(0,0,'Telfs.: 0426-5173225 / 0212-8714385 / 0212-808-72-16',0,0,'C');	
		//$this->Image('../../images/sello.png', 85, 205, 50, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}

	//$fontname = $pdf->addTTFfont('../../includes/tcpdf/fonts/Arial.ttf', 'TrueType', '', 32);
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Direccion de RRHH de VENALCASA, S.A. Rif. G-20008504-5');
	$pdf->SetTitle('Constancia de Egreso');
	$pdf->SetSubject('Generado Automaticamente por el Sistema de Atencion al Empleado');
	$pdf->SetKeywords('Constancia');
	
	////// AGREGAR PAGINA
	$pdf->AddPage();
	$pdf->SetMargins('25', '', '20');
	$pdf->SetFont('helvetica','', 11);
	
	$pdf->Ln(70);
	
	if ($NU_IdTipo==1){
		
		$pdf->writeHTMLCell(0,20,'','',utf8_encode('<span style="line-height:25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Quien suscribe el (la) <b>DIRECTOR (A) GENERAL DE RECURSOS HUMANOS</b> de <b>VENEZOLANA DE ALIMENTOS LA CASA, S.A.</b>, hace constar por medio de la presente, que la (el) ciudadana (o) <b>'.$AL_Apellido.' '.$AL_Nombre.'</b>, C�dula de Identidad <b>N� V-'.$NU_Cedula.'</b> prest� sus servicios en esta empresa desde el <b>'.$FE_Ingreso.'</b> hasta el <b>'.$FE_Egreso.'</b>, desempe�ando el cargo de <b>'.$Cargo.'</b>, adscrito a la <b>'.$AL_Adscripcion.'</b>, con un Sueldo Mensual Integral de <b>'.$SalarioIntegralLetras.' (Bs. '.$SueldoIntegral.')</b> m�s una asignaci�n por concepto de Bono de Alimentaci�n correspondiente a 12 Unidades Tributarias Vigentes por d�a laborado, Monto Promedio Mensual de <b>SESENTA Y TRES MIL SETESCIENTOS VEINTE BOLIVARES EXACTOS (Bs. 63.720,00)</b>.</i></span>'),0,10,0,true,'J',true);
	
	}else{
		
		$pdf->writeHTMLCell(0,20,'','',utf8_encode('<span style="line-height:25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Quien suscribe el (la) <b>DIRECTOR (A) GENERAL DE RECURSOS HUMANOS</b> de <b>VENEZOLANA DE ALIMENTOS LA CASA, S.A.</b>, hace constar por medio de la presente, que la (el) ciudadana (o) <b>'.$AL_Apellido.' '.$AL_Nombre.'</b>, C�dula de Identidad <b>N� V-'.$NU_Cedula.'</b> prest� sus servicios en esta empresa desde el <b>'.$FE_Ingreso.'</b> hasta el <b>'.$FE_Egreso.'</b>, desempe�ando el cargo de <b>'.$Cargo.'</b>, adscrito a la <b>'.$AL_Adscripcion.'</b>, con un Sueldo Mensual de <b>'.$BS_SalarioBasicoLetras.' (Bs. '.$BS_SalarioBasico.')</b>'.$prima1.$prima2.$prima3.', y una asignaci�n por concepto de Bono de Alimentaci�n correspondiente a 12 Unidades Tributarias Vigentes por d�a laborado, Monto Promedio Mensual de <b>SESENTA Y TRES MIL SETECIENTOS VEINTE BOLIVARES EXACTOS (Bs. 63.720,00)</b>.</i></span>'),0,10,0,true,'J',true);		
	
	}
	
	$pdf->Ln(3);
	$pdf->writeHTMLCell(0,20,'','',utf8_encode('<span style="line-height:25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>Constancia que se expide a petici�n de la parte interesada en Caracas a los '.$dia.' d�as del mes de '.$mes.' del a�o '.$ano.'.</i></span>'),0,10,0,true,'J',true);
	
	$pdf->Ln(3);
	$pdf->Cell(0,7,'Atentamente,',0,0,'C',0);
	
	/////// ENVIAR DOCUMENTO
	$pdf->Output('Constancia_Egreso.pdf', 'I');
?>
