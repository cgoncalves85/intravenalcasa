﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/cfideicomiso_tipoModel.php');
	require_once('../../model/cfideicomisoModel.php');	
	require_once('../../model/usuarioModel.php');	
	
	$objCFideicomiso_Tipo 	= new CFideicomiso_Tipo();
	$objCFideicomiso 		= new CFideicomiso();
	
	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
		$AL_Adscripcion 		= $objConexion->obtenerElemento($RSUsuario,0,"AL_Adscripcion");	
	}	
	
	$RS 	= $objCFideicomiso->listarConstancias($objConexion,$_SESSION["NU_IdUsuario"]);
	$cantRS = $objConexion->cantidadRegistros($RS);
	
	$objUsuario		= new Usuario();
	$RS1 			= $objUsuario->listar($objConexion);
	$cantRS1		= $objConexion->cantidadRegistros($RS1);
	for($i=0; $i<$cantRS1; $i++){
		$FE_CargaDatos		= $objConexion->obtenerElemento($RS1,$i,'FE_CargaDatos');	
	}	

	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	
	function setFechaNoSQL2($FE_CargaDatos)
	{
		$partes = explode("-", $FE_CargaDatos);
		$FE_CargaDatos = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_CargaDatos;
	}
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <span class="ui-icon ui-icon-circle-check"></span>
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Notificación de Fideicomiso</h4>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								Generar Notificación de Fideicomiso
							</div>
							<div class="panel-body">
								<div class="row">
									<form role="form" method="POST" action="../../controller/cfideicomisoController.php" target="_blank">
										<div class="col-lg-2"></div>
										<div class="col-lg-8">
											<br>
											<p>Para generar una nueva notificación de fideicomiso haz click en el siguiente botón:</p>
											<button align="right" type="submit" name="submit" id="submit" class="btn btn-primary">Generar Notificación</button>
											&nbsp;&nbsp;&nbsp;&nbsp;Ultima Actualización : <strong><?php echo setFechaNoSQL2($FE_CargaDatos); ?></strong>
											
											<select name="NU_IdTipoCFideicomiso" style="visibility:hidden" required id="NU_IdTipoCFideicomiso"> 
												<?php 
													$RSTipoCFideicomiso		= $objCFideicomiso_Tipo->listarTipos($objConexion);
													$cantRSTipoCFideicomiso = $objConexion->cantidadRegistros($RSTipoCFideicomiso);
													for($i=0;$i<$cantRSTipoCFideicomiso;$i++){
														$value = $objConexion->obtenerElemento($RSTipoCFideicomiso,$i,"NU_IdTipoCFideicomiso");
														$des   = $objConexion->obtenerElemento($RSTipoCFideicomiso,$i,"AL_Tipo");
														echo "<option value=".$value.">".$des."</option>";
													}  
												?>
											</select>
											<input name="origen" type="hidden" id="origen" value="CFideicomiso">											
										</div>
										<div class="col-lg-2"></div>
									</form>	
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								Mis Notificaciones de Fideicomiso
							</div>						

							<div class="panel-body">
								<div class="row">							
									<br>
									<div class="col-lg-1"></div>
									<div class="col-lg-10">
										<div class="dataTable_wrapper">
											<?php if ($cantRS>0){ ?>
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr align="center">
														<td style="vertical-align:middle;" ><strong>Fecha de<br> Solicitud</strong></td>
														<td style="vertical-align:middle;" ><strong>Tipo de Constancia</strong></td>
														<td style="vertical-align:middle;" ><strong>Ver</strong></td>
														<td style="vertical-align:middle;" ><strong>Eliminar</strong></td>	
													</tr>
												</thead>
												<tbody>
													<?php
														for($i=0; $i<$cantRS; $i++){
														$NU_IdCFideicomiso		= $objConexion->obtenerElemento($RS,$i,'NU_IdCFideicomiso');
														$FE_Solicitud 			= $objConexion->obtenerElemento($RS,$i,'FE_Solicitud');
														$NU_IdTipoCFideicomiso 	= $objConexion->obtenerElemento($RS,$i,'NU_IdTipoCFideicomiso');
														$AL_Tipo				= $objConexion->obtenerElemento($RS,$i,'AL_Tipo');			
													?>
													<tr align="center">
														<td style="vertical-align:middle;" ><?php echo setFechaNoSQL($FE_Solicitud); ?></td>
														<td style="vertical-align:middle;" ><?php echo $AL_Tipo; ?></td>
														<td style="vertical-align:middle;" ><a href="Constancia_Fideicomiso<?php echo $NU_IdTipoCFideicomiso; ?>.php?NU_IdCFideicomiso=<?php echo $NU_IdCFideicomiso; ?>" target="_blank"><span class="glyphicon glyphicon-search" title="Ver Notificación"></span></a></td>
														<td style="vertical-align:middle;" ><a href="Constancia_Fideicomiso<?php echo $NU_IdTipoCFideicomiso; ?>.php?NU_IdCFideicomiso=<?php echo $NU_IdCFideicomiso; ?>"><span class="glyphicon glyphicon-trash" title="Eliminar Notificación"></span></a></td>
													</tr>
													<?php
														}
													?>          
												</tbody>
												<?php
													}else{ echo '<br><tr align="center"><td><strong class="rojo">No se encontraron Constancias de Fideicomiso.</strong></td></tr><br>'; }
												?>
											</table>
										</div>	
										<br>
									</div>
									<div class="col-lg-1">&nbsp;</div>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>	
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
