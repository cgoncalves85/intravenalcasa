<?php 
	//require_once('../../controller/sessionController.php');
		session_start();
	
	require_once("../../includes/constantes.php");
	require_once("../../includes/conexion.class.php");

	$objConexion= new conexion(SERVER,USER,PASS,DB);
	
	
	require_once('../../includes/tcpdf/tcpdf.php'); 
	
	require_once('../../model/cfideicomisoModel.php');
	require_once('../../includes/NumLetras.php');	

	$objCFideicomiso 	= new CFideicomiso();
	
	$NU_IdCFideicomiso = $_GET['NU_IdCFideicomiso'];
	
	$RS 		= $objCFideicomiso->buscar($objConexion,$NU_IdCFideicomiso);
	$cantRS 	= $objConexion->cantidadRegistros($RS);

	///// CONVIERTE FECHA 1980-07-04 A 04/07/1980 (FORMATO ESPANOL)
	function setFechaNOSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}

	///////////////////////// DATOS CONSULTAS ///////////////////////////////////////////
	$AL_Nombre 					= utf8_decode(strtoupper($objConexion->obtenerElemento($RS,0,"AL_Nombre")));
	$AL_Apellido 				= utf8_decode(strtoupper($objConexion->obtenerElemento($RS,0,"AL_Apellido")));
	$NU_Cedula 					= number_format($objConexion->obtenerElemento($RS,0,"NU_Cedula"),0,'','.');
	$BS_TotalPrestaciones		= $objConexion->obtenerElemento($RS,0,"BS_TotalPrestaciones");
	$BS_TotalAnticipo 			= $objConexion->obtenerElemento($RS,0,"BS_TotalAnticipo");	
	$BS_TotalSaldo 				= $objConexion->obtenerElemento($RS,0,"BS_TotalSaldo");
	$BS_TotalDisponible 		= $objConexion->obtenerElemento($RS,0,"BS_TotalDisponible");
	$FE_Solicitud 				= $objConexion->obtenerElemento($RS,0,"FE_Solicitud");
	
	$BS_TotalPrestaciones = number_format($BS_TotalPrestaciones,2,',','.');
	$BS_TotalAnticipo = number_format($BS_TotalAnticipo,2,',','.');
	$BS_TotalSaldo = number_format($BS_TotalSaldo,2,',','.');
	$BS_TotalDisponible = number_format($BS_TotalDisponible,2,',','.');

	//$SalarioIntegralLetras		= utf8_decode(strtoupper(convertir_a_letras($SueldoIntegral)));	
	//$SueldoIntegral				= number_format($SueldoIntegral,2,',','.');

	$FE_Solicitud = explode("-",$FE_Solicitud);
	$mes = $FE_Solicitud[1];
	
	$meses = array('01' => 'Enero','02' => 'Febrero','03' => 'Marzo','04' => 'Abril','05' => 'Mayo','06' => 'Junio','07' => 'Julio','08' => 'Agosto','09' => 'Septiembre','10' => 'Octubre','11' => 'Noviembre','12' => 'Diciembre');

	$dia = $FE_Solicitud[2];
	$mes = $meses[$mes];
	$ano = $FE_Solicitud[0];
?>
<?php

class MYPDF extends TCPDF {

    public function Header() {
		$this->SetMargins('25', '', '20');
		$this->Image('../../images/head.jpg', 25, 20, 160, 0, 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->SetFillColor(232,232,232);
		$this->Line(25,33,185,33);
		$this->SetFont('helvetica','B',10);
		$this->Ln(14);
		$this->Cell(0,0,'Rif. G-20008504-5',0,0,'L');
		$this->Ln(20);
		$this->SetFont('helvetica','BI',14);
		$this->Cell(0,0,'NOTIFICACION INDIVIDUAL ACUMULADO',0,0,'C');
		$this->Ln(8);	
		$this->Cell(0,0,'DE PRESTACIONES SOCIALES',0,0,'C');
		$this->Ln(38);	
    }

    public function Footer() {
		$this->SetMargins('25', '', '20');
		$this->SetY(-85);
		$this->SetFont('helvetica','BI',12);
		$this->Ln(5);
		$this->Cell(0,0,'LIC. ISIS LEDEZMA',0,0,'C');
		$this->Ln(8);
		$this->Cell(0,0,'DIRECTOR(A) GENERAL DE RECURSOS HUMANOS (E)',0,0,'C');
		
		$this->Ln(25);
		$this->SetFont('helvetica','',8);
		$this->Cell(0,0,utf8_encode('El presente documento es de car�cter informativo,  es propenso a cambios y/o modificaci�n en caso de ser'),0,0,'C');
		$this->Ln(3);
		$this->Cell(0,0,utf8_encode('necesario y su actualizaci�n es trimestral.'),0,0,'C');
		$this->SetFillColor(232,232,232);	
		$this->Line(25,258,188,258);

		$this->Ln(7);
		$this->Cell(0,0,'VENEZOLANA DE ALIMENTOS LA CASA, S.A.',0,0,'C');
		$this->Ln(3);
		$this->Cell(0,0,utf8_encode('Av. Sucre, entre la Polic�a Nacional Bolivariana y Calle Mauri, Centro de Acopio Catia Parroquia Sucre'),0,0,'C');
		$this->Ln(3);
		$this->Cell(0,0,'Municipio Libertador. Caracas - Venezuela',0,0,'C');
		$this->Ln(3);
		$this->Cell(0,0,'Telfs.: 0212.870.55.14',0,0,'C');
		$this->Image('../../images/sello.png', 85, 190, 50, 0, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }
}

	//$fontname = $pdf->addTTFfont('../../includes/tcpdf/fonts/Arial.ttf', 'TrueType', '', 32);
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Direccion de RRHH de VENALCASA, S.A. Rif. G-20008504-5');
	$pdf->SetTitle('Notificacion');
	$pdf->SetSubject('Generado Automaticamente por el Sistema de Atencion al Empleado');
	$pdf->SetKeywords('Notificacion');
	
	////// AGREGAR PAGINA
	$pdf->AddPage();
	$pdf->SetMargins('25', '', '20');
	$pdf->SetFont('helvetica', '', 11);
	
	$pdf->Ln(80);
	$pdf->writeHTMLCell(0,20,'','',utf8_encode('<span style="line-height:25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Quien suscribe <b>DIRECTOR(A) GENERAL DE RECURSOS HUMANOS</b> de <b>VENEZOLANA DE ALIMENTOS LA CASA, S.A.</b>, hace de su conocimiento por medio de la presente, que la (el) ciudadana (o) <b>'.$AL_Apellido.' '.$AL_Nombre.'</b>, C�dula de Identidad N� V- C.I. <b>'.$NU_Cedula.'</b> presenta un acumulado total  por concepto de prestaciones sociales a la fecha de Bs <b>'.$BS_TotalPrestaciones.'</b>, su total de anticipo solicitado a la fecha es de Bs <b>'.$BS_TotalAnticipo.'</b>, su saldo disponible de prestaciones  es de Bs <b>'.$BS_TotalSaldo.'</b>, y el 75 % que puede solicitar por concepto de anticipo es de Bs <b>'.$BS_TotalDisponible.'</b>.</span>'),0,10,0,true,'J',true);
	
	$pdf->Ln(10);
	$pdf->writeHTMLCell(0,20,'','',utf8_encode('<span style="line-height:25px">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   Bajo esta carta el trabajador arriba especificado es notificado para cumplir los requisitos exigidos seg�n Articulo 143 de la LOTTT.</span>'),0,10,0,true,'J',true);
	
	$pdf->Ln(10);
	$pdf->Cell(0,7,'Atentamente,',0,0,'C',0);
	
	/////// ENVIAR DOCUMENTO
	$pdf->Output('ConstanciaFideicomiso.pdf', 'I');
?>
