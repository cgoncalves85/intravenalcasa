<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
ini_set('display_errors', '1');
require_once('../../../controller/sessionController.php');
require_once '../../../includes/PHPExcel/PHPExcel.php';
require_once '../../../includes/PHPExcel/PHPExcel/Writer/Excel2007.php';

require_once('../../../model/pedidoDetalleModel.php');
require_once('../../../model/mercadoProductoModel.php');
require_once('../../../model/sedeModel.php');
require_once('../../../model/gerenciaModel.php');

$NU_IdMercado = $_GET['NU_IdMercado'];
$gerencia_NU_IdGerencia = $_GET['gerencia_NU_IdGerencia'];

$objPedidoDetalle = new PedidoDetalle();
$objEmpleado = new PedidoDetalle();
$objMercadoProducto = new MercadoProducto();

////////// CONSULTAS ////////
$RSMercadoProducto = $objMercadoProducto->cantProducXmerc($objConexion,$NU_IdMercado);
$cRSMercadoProducto = $objConexion->cantidadRegistros($RSMercadoProducto);

$RSPedidoDetalle = $objPedidoDetalle->RelacionDescuentoXgerencia($objConexion,$NU_IdMercado,$gerencia_NU_IdGerencia);
$cantRSPedidoDetalle = $objConexion->cantidadRegistros($RSPedidoDetalle);

$empleados = $objEmpleado->Empleados($objConexion,$NU_IdMercado,$gerencia_NU_IdGerencia);
$cantEmpleado = $objConexion->cantidadRegistros($empleados);

$AL_NombreSede = $objConexion->obtenerElemento($RSPedidoDetalle,0,'AL_NombreSede');
$AL_NombreGerencia = $objConexion->obtenerElemento($RSPedidoDetalle,0,'AL_NombreGerencia');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// PROPIEDADES DEL LIBRO
$objPHPExcel->getProperties()->setCreator("Venezolana de Alimentos La Casa, VENALCASA S.A."); //Autor
$objPHPExcel->getProperties()->setLastModifiedBy("Sistema elaborado por la Oficina de Tecnología de la Información"); //ultimo cambio
$objPHPExcel->getProperties()->setTitle("Mercado Corporativo de VENALCASA");
$objPHPExcel->getProperties()->setSubject("Relación de Descuento");
$objPHPExcel->getProperties()->setDescription("Relación de Descuento");

// Establecer la hoja activa, para que cuando se abra el documento se muestre primero
$objPHPExcel->setActiveSheetIndex(0);

///////// ESTILOS DE CELDAS ///////
$BarraHead = array(
  'font' => array(
    'bold'      => true,
  ),
  'alignment' => array(
    'horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    'vertical' 		=> PHPExcel_Style_Alignment::VERTICAL_CENTER,				
  )
);

$BarraProducto = array(
  'font' => array(
    'bold' => true,
    'size' => 8,
    'name' => 'Arial',
  ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
  ),
  'borders' 	=> array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN,
    ),
  ),
  'fill' => array(
    'type' => PHPExcel_Style_Fill::FILL_SOLID,
    'startcolor' => array(
      'argb' => 'CCCCCC'
    )
  )
);

$BarraTitulo = array(
  'font' => array(
    'bold' => true,
    'size' => 8,
    'name' => 'Arial',
  ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,				
  ),
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN,
    ),
  ),
);

$Datos = array(
  'font' => array(
    'bold' => false,
    'size' => 8,
    'name' => 'Arial',
  ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,				
  ),			
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN,
    ),
  )
);

$Borde = array(
  'font' => array(
    'bold' => false,
    'size' => 8,
    'name' => 'Arial',
  ),	
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN,
    ),
  )
);

$Total = array(
  'font' => array(
    'bold' => true,
    'size' => 8,
    'name' => 'Arial',
  ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,				
  )	
);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);	
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);		
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);

$incrementador = 0;
$ColZ = '';
$doble = 0;
for ($z=0; $z<(($cRSMercadoProducto*2)+2); $z++) {
  $ColZ	= chr(68+$incrementador);
  if ($ColZ=='Z') { 
    $incrementador = 0;
    $letra = $ColZ;					
    $doble = 1;			
  } else {
    if ($doble==1) {
      $letra = 'A'.chr(64+$incrementador);						
    } else {
      $letra = chr(68+$incrementador);						
    }
  }
  $objPHPExcel->getActiveSheet()->getColumnDimension($letra)->setWidth(8);
  $incrementador++;
}
/////// FIN DE ESTILOS

//////////// AGREGAR HEAD1
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('../../../images/head1.jpg');
$objDrawing->setHeight(30);
$objDrawing->setCoordinates('A1');
$objDrawing->setOffsetX(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

//////////// AGREGAR HEAD2
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('../../../images/head2.jpg');
$objDrawing->setHeight(30);
$objDrawing->setCoordinates('Q1');
$objDrawing->setOffsetX(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

//////////// LOGO EMPRESA
$objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('../../../images/logo.jpg');
$objDrawing->setHeight(80);
$objDrawing->setCoordinates('A3');
$objDrawing->setOffsetX(0);
$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

//********** ESCRIBIR TITULO *************************************
$UltColumna = chr(68+$cRSMercadoProducto).'4';
$tituloReporte 	= strtoupper("Relación de Descuento de las Ordenes de Compras del Mercado Corporativo de VENALCASA: DGRH-GBS-M0").$NU_IdMercado;
$objPHPExcel->getActiveSheet()->mergeCells('D3:'.$UltColumna);
$objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray($BarraHead);
$objPHPExcel->getActiveSheet()->SetCellValue('D3', $tituloReporte);	

$UltColumna = chr(68+$cRSMercadoProducto).'5';
$objPHPExcel->getActiveSheet()->mergeCells('D5:'.$UltColumna);
$objPHPExcel->getActiveSheet()->getStyle('D5')->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($BarraHead);
$objPHPExcel->getActiveSheet()->SetCellValue('D5', 'UBICACIÓN ADMINISTRATIVA: '.$AL_NombreSede.' - '.$AL_NombreGerencia);	

$UltColumna = chr(68+$cRSMercadoProducto).'6';
$objPHPExcel->getActiveSheet()->mergeCells('D6:'.$UltColumna);
$objPHPExcel->getActiveSheet()->getStyle('D6')->getAlignment()->setWrapText(true);
$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($BarraHead);
$objPHPExcel->getActiveSheet()->SetCellValue('D6', 'NÚMERO DE ORDEN: ');

////// CUERPO DEL ARCHIVO DE EXCEL - CONTENIDO - ////////////

//****** BARRA DE PRODUCTOS ******************************
$objPHPExcel->getActiveSheet()->mergeCells('A8:C8');
$objPHPExcel->getActiveSheet()->getStyle('A8:C8')->applyFromArray($BarraProducto);
$objPHPExcel->getActiveSheet()->SetCellValue('A8', 'Empleados');

$fila = 8;
$incrementador = 0;
$doble = 0;
$ColZ = '';
for ($a=0;$a<$cRSMercadoProducto;$a++) {
  $AF_NombreProducto = $objConexion->obtenerElemento($RSPedidoDetalle,$a,"AF_NombreProducto");

  $ColZ	= chr(68+$incrementador);
  if ($ColZ=='Z') {
    $Columna1 = chr(68+$incrementador).$fila;
    $incrementador = 0;
    $Columna2 = 'A'.chr(65+$incrementador).$fila;		
    $doble = 1;			
  } else {
    if ($doble==1) {
      $Columna1	= 'A'.chr(64+$incrementador).$fila;			
      $Columna2	= 'A'.chr(65+$incrementador).$fila;		
    } else {
      $Columna1	= chr(68+$incrementador).$fila;			
      $Columna2 = chr(69+$incrementador).$fila;		
    }
  }

  $objPHPExcel->getActiveSheet()->mergeCells($Columna1.':'.$Columna2);		
  $objPHPExcel->getActiveSheet()->getStyle($Columna1.':'.$Columna2)->applyFromArray($BarraProducto);
  $objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $AF_NombreProducto);

  $incrementador = $incrementador + 2;
}
$Columna1 = 'A'.chr(64+$incrementador).$fila;			
$Columna2 = 'A'.chr(64+$incrementador).($fila+1);				
$objPHPExcel->getActiveSheet()->mergeCells($Columna1.':'.$Columna2);	
$objPHPExcel->getActiveSheet()->getStyle($Columna1.':'.$Columna2)->applyFromArray($BarraProducto);
$objPHPExcel->getActiveSheet()->getStyle($Columna1)->getAlignment()->setWrapText(true);		
$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, 'Total Rubros');
$ColRubros = 'A'.chr(64+$incrementador);

$Columna1 = 'A'.chr(65+$incrementador).$fila;			
$Columna2 = 'A'.chr(65+$incrementador).($fila+1);	
$objPHPExcel->getActiveSheet()->mergeCells($Columna1.':'.$Columna2);	
$objPHPExcel->getActiveSheet()->getStyle($Columna1.':'.$Columna2)->applyFromArray($BarraProducto);
$objPHPExcel->getActiveSheet()->getStyle($Columna1)->getAlignment()->setWrapText(true);		
$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, 'Total Bs.');	
$ColBs = 'A'.chr(65+$incrementador);	

//***** BARRA DE TITULOS ******************************
$fila++;
$Columna = chr(65).$fila;
$objPHPExcel->getActiveSheet()->getStyle($Columna)->applyFromArray($BarraTitulo);
$objPHPExcel->getActiveSheet()->SetCellValue($Columna, 'Nº');

$Columna = chr(66).$fila;
$objPHPExcel->getActiveSheet()->getStyle($Columna)->applyFromArray($BarraTitulo);
$objPHPExcel->getActiveSheet()->SetCellValue($Columna, 'Nombre y Apellido');

$Columna		= chr(67).$fila;
$objPHPExcel->getActiveSheet()->getStyle($Columna)->applyFromArray($BarraTitulo);
$objPHPExcel->getActiveSheet()->SetCellValue($Columna, 'Cédula');

$incrementador = 0;
$ColZ = '';
$doble = 0;				
for ($b=0;$b<$cRSMercadoProducto;$b++) {
  $BS_PrecioUnitario = $objConexion->obtenerElemento($RSPedidoDetalle,$b,"BS_PrecioUnitario");
  $BS_PrecioUnitario = number_format($BS_PrecioUnitario,2,',','.');		
  $NU_Max = $objConexion->obtenerElemento($RSPedidoDetalle,$b,"NU_Max");
  $AL_Medida = $objConexion->obtenerElemento($RSPedidoDetalle,$b,"AL_Medida");

  $ColZ	= chr(68+$incrementador);
  if ($ColZ=='Z') { 
    $Columna1 = chr(68+$incrementador).$fila;
    $incrementador = 0;
    $Columna2 = 'A'.chr(65+$incrementador).$fila;					
    $doble = 1;			
  } else {
    if ($doble==1) {
      $Columna1	= 'A'.chr(64+$incrementador).$fila;
      $Columna2	= 'A'.chr(65+$incrementador).$fila;
    } else {
      $Columna1	= chr(68+$incrementador).$fila;			
      $Columna2	= chr(69+$incrementador).$fila;						
    }
  }

  $objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($BarraTitulo);
  $objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $NU_Max.' '.$AL_Medida.' Max');

  $objPHPExcel->getActiveSheet()->getStyle($Columna2)->applyFromArray($BarraTitulo);
  $objPHPExcel->getActiveSheet()->SetCellValue($Columna2, 'Bs. '.$BS_PrecioUnitario);
  
  $incrementador = $incrementador + 2;	
}

//***** LISTAR EMPLEADOS CON SUS DATOS ******************************	
$ultimaLetra = substr($Columna2, 0, -1);
$fila++;
$incrementador 	= 0;
$ColZ = '';
$doble = 0;
$contador = 0;
$contador2 = 0;
$totalRubrosT = 0;
$totalBsT = 0;
$totalRubros = 0;
$totalBs = 0;
for ($c=0;$c<$cantEmpleado;$c++) {
  $AL_Nombre = $objConexion->obtenerElemento($empleados,$c,"AL_Nombre");
  $AL_Apellido = $objConexion->obtenerElemento($empleados,$c,"AL_Apellido");
  $NU_Cedula = $objConexion->obtenerElemento($empleados,$c,"NU_Cedula");
  $totalRubrosT	= $totalRubrosT + $NU_Cantidad;
  $totalBsT = $totalBsT + $Costo;
  $totalRubros = $totalRubros + $NU_Cantidad;
  $totalBs = $totalBs + $Costo;
  
  for ($a=0;$a<$cRSMercadoProducto;$a++) {
    $empleadoDetalle = $objEmpleado->EmpleadoDetalle($objConexion,$NU_IdMercado, $objConexion->obtenerElemento($empleados,$c,"NU_IdUsuario"), $objConexion->obtenerElemento($RSMercadoProducto,$a,"NU_IdProducto"));
    $cantEmpleadoDetalle = $objConexion->cantidadRegistros($empleadoDetalle);
  
    $ColZ = chr(68+$incrementador);
    if ($ColZ=='Z') {
      $Columna1 = chr(68+$incrementador).$fila;
      $incrementador = 0;
      $Columna2 = 'A'.chr(65+$incrementador).$fila;		
      $doble = 1;			
    } else {
      if ($doble==1) {
	$Columna1	= 'A'.chr(64+$incrementador).$fila;			
	$Columna2	= 'A'.chr(65+$incrementador).$fila;		
      } else {
	$Columna1	= chr(68+$incrementador).$fila;			
	$Columna2 = chr(69+$incrementador).$fila;		
      }
    }
    if($cantEmpleadoDetalle>0) {
      $cantidad = $objConexion->obtenerElemento($empleadoDetalle,0,"NU_Cantidad");
      $costo = $objConexion->obtenerElemento($empleadoDetalle,0,"BS_PrecioUnitario") * $objConexion->obtenerElemento($empleadoDetalle,0,"NU_Cantidad");
    } else {
      $cantidad = 0;
      $costo = 0;
    }
    $objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $cantidad);
    $objPHPExcel->getActiveSheet()->SetCellValue($Columna2, $costo);	
  
    $incrementador = $incrementador + 2;
  }
  
  $objPHPExcel->getActiveSheet()->getStyle($Columna2)->getNumberFormat()->setFormatCode('#,##0.00');
  $incrementador = 0;
  $doble = 0;
  $contador = 0;
  $contador2++;

  $Columna = 'A'.$fila;
  $objPHPExcel->getActiveSheet()->SetCellValue($Columna, $contador2);		
  $Columna = 'B'.$fila;
  $objPHPExcel->getActiveSheet()->SetCellValue($Columna, $AL_Nombre.' '.$AL_Apellido);		
  $Columna = 'C'.$fila;
  $objPHPExcel->getActiveSheet()->SetCellValue($Columna, $NU_Cedula);				

  $objPHPExcel->getActiveSheet()->getStyle($ColRubros.$fila)->applyFromArray($Datos);
  $objPHPExcel->getActiveSheet()->SetCellValue($ColRubros.$fila, $totalRubros);		
  $objPHPExcel->getActiveSheet()->getStyle($ColBs.$fila)->applyFromArray($Datos);
  $objPHPExcel->getActiveSheet()->getStyle($ColBs.$fila)->getNumberFormat()->setFormatCode('#,##0.00');	
  $objPHPExcel->getActiveSheet()->SetCellValue($ColBs.$fila, $totalBs);		

  $totalRubros = 0;
  $totalBs = 0;
  $fila++;
}
$ultimaLetra = $ultimaLetra.$fila;
$objPHPExcel->getActiveSheet()->getStyle('A10:'.$ultimaLetra)->applyFromArray($Borde);
$objPHPExcel->getActiveSheet()->getStyle('D10:'.$ultimaLetra)->applyFromArray($Datos);

//***** TOTALES ******************************		
$objPHPExcel->getActiveSheet()->mergeCells('A'.$fila.':C'.$fila);
$objPHPExcel->getActiveSheet()->getStyle('A'.$fila)->applyFromArray($Total);
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, 'TOTALES');	

$incrementador = 0;
$ColZ = '';
$doble = 0;				

for ($d=0;$d<$cRSMercadoProducto;$d++) {

  $ColZ	= chr(68+$incrementador);
  if ($ColZ=='Z') { 
    $Columna1 = chr(68+$incrementador).$fila;
    $letraP1 = chr(68+$incrementador);
    $incrementador = 0;
    $Columna2 = 'A'.chr(65+$incrementador).$fila;
    $letraP2 = 'A'.chr(65+$incrementador);
    $doble = 1;
  } else {
    if ($doble==1){
      $Columna1	= 'A'.chr(64+$incrementador).$fila;			
      $letraP1 = 'A'.chr(64+$incrementador);
      $Columna2 = 'A'.chr(65+$incrementador).$fila;						
      $letraP2 = 'A'.chr(65+$incrementador);				
    } else {
      $Columna1 = chr(68+$incrementador).$fila;			
      $letraP1 = chr(68+$incrementador);
      $Columna2 = chr(69+$incrementador).$fila;						
      $letraP2 = chr(69+$incrementador);				
    }
  }

  $objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($Total);
  $objPHPExcel->getActiveSheet()->SetCellValue($Columna1, '=SUM('.$letraP1.'10:'.$letraP1.($fila-1).')');
  
  $objPHPExcel->getActiveSheet()->getStyle($Columna2)->applyFromArray($Total);
  $objPHPExcel->getActiveSheet()->getStyle($Columna2)->getNumberFormat()->setFormatCode('#,##0.00');	
  $objPHPExcel->getActiveSheet()->SetCellValue($Columna2, '=SUM('.$letraP2.'10:'.$letraP2.($fila-1).')');		

  $incrementador = $incrementador + 2;
}	
$incrementador = $incrementador - 1;
$ColRubros = 'A'.chr(65+$incrementador);
$ColBs = 'A'.chr(66+$incrementador);

$objPHPExcel->getActiveSheet()->getStyle($ColRubros.$fila)->applyFromArray($Borde);
$objPHPExcel->getActiveSheet()->getStyle($ColRubros.$fila)->applyFromArray($Total);
$objPHPExcel->getActiveSheet()->SetCellValue($ColRubros.$fila, $totalRubrosT);		

$objPHPExcel->getActiveSheet()->getStyle($ColBs.$fila)->applyFromArray($Borde);
$objPHPExcel->getActiveSheet()->getStyle($ColBs.$fila)->applyFromArray($Total);
$objPHPExcel->getActiveSheet()->getStyle($ColBs.$fila)->getNumberFormat()->setFormatCode('#,##0.00');	
$objPHPExcel->getActiveSheet()->SetCellValue($ColBs.$fila, $totalBsT);		

///////// PIE DE PAGINA

$fila 		= $fila +3;
//$UltColumna = chr(65+$cantRSProducMercado+1).$fila;
$UltColumna = $ColBs.$fila;	
$objPHPExcel->getActiveSheet()->mergeCells('A'.$fila.':'.$UltColumna);
$objPHPExcel->getActiveSheet()->getStyle('A'.$fila)->getFont()->setSize(6);
$objPHPExcel->getActiveSheet()->getStyle('A'.$fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);																				
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, 'Sistema elaborado por la Oficina de Tecnología de la Información de VENALCASA, S.A. G-20008504-5');	
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);

for ($b=0; $b<($cantRSProducMercado+2); $b++) {
  $letra = chr(67+$b);	
  $objPHPExcel->getActiveSheet()->getColumnDimension($letra)->setWidth(8);	
}

/////////////////CREACION DEL DOCUMENTO	
// Orientacion y tamano de la pagina
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

// Cambiar nombre de la Hoja
$objPHPExcel->getActiveSheet()->setTitle('Relación');

// Guardar Archivo en formato Excel 2007
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

// Abrir Documento

header("Location: documento_xls.xlsx");