<?php 
	require_once('../../../controller/sessionController.php');
	require_once('../../../includes/pdf/fpdf.php'); 
	require_once('../../../model/pedidoModel.php');
	require_once('../../../model/mercadoProductoModel.php');	
	require_once('../../../model/pedidoDetalleModel.php');	

	$objPedido 			= new Pedido();
	$objMercadoProducto = new MercadoProducto();
	$objPedidoDetalle 	= new PedidoDetalle();	
	$objMercadoProducto = new MercadoProducto();
			
	$NU_IdMercado 			= $_GET['NU_IdMercado'];
	$gerencia_NU_IdGerencia = $_GET['gerencia_NU_IdGerencia'];
	

	$RSMercadoProducto		= $objMercadoProducto->cantProducXmerc($objConexion,$NU_IdMercado);
	$cRSMercadoProducto		= $objConexion->cantidadRegistros($RSMercadoProducto);


	$_SESSION[cant]			= $cRSMercadoProducto;
	
	for($a=0; $a<$cRSMercadoProducto; $a++){
		$_SESSION[producto][$a] = $objConexion->obtenerElemento($RSMercadoProducto,$a,"AF_NombreProducto");
	}
	
	$RS 		= $objPedido->buscarDescuentoN($objConexion,$NU_IdMercado,$gerencia_NU_IdGerencia);
	$cantRS 	= $objConexion->cantidadRegistros($RS);
	
	$RSPedidoDetalle		= $objPedidoDetalle->RelacionDescuentoXgerencia($objConexion,$NU_IdMercado,$gerencia_NU_IdGerencia);
	$cantRSPedidoDetalle	= $objConexion->cantidadRegistros($RSPedidoDetalle);

if ($cantRSPedidoDetalle=='0'){
	echo 'NO EXISTEN SOLICITUDES PARA ESTE MERCADO';
}else{	
	$_SESSION[cant2] = $cantRSPedidoDetalle;
	
	for($c=0; $c<$cantRSPedidoDetalle; $c++){
		$_SESSION['maxi'][$c] 		= $objConexion->obtenerElemento($RSPedidoDetalle,$c,"NU_Max");
		$_SESSION[medida][$c] 	= $objConexion->obtenerElemento($RSPedidoDetalle,$c,"AL_Medida");
		$_SESSION[precio][$c] 	= $objConexion->obtenerElemento($RSPedidoDetalle,$c,"BS_PrecioUnitario");		
	}	
?>
<?php

		$_SESSION['fecha'] = $objConexion->obtenerElemento($RS,0,"FE_FechaMercado");

		
		$FE_FechaMercado = explode("-",$_SESSION['fecha']);
		$mes1 = $FE_FechaMercado[1];
		
		$meses = array('01' => 'Enero','02' => 'Febrero','03' => 'Marzo','04' => 'Abril','05' => 'Mayo','06' => 'Junio','07' => 'Julio','08' => 'Agosto','09' => 'Septiembre','10' => 'Octubre','11' => 'Noviembre','12' => 'Diciembre');
		
		$_SESSION['$mes'] = $meses[$mes1];
		$_SESSION['$anio'] = $FE_FechaMercado[0];
			
		$_SESSION['mercado_NU_IdMercado'] = $NU_IdMercado;
		$_SESSION['AF_RazonSocial'] = $objConexion->obtenerElemento($RS,0,"AF_RazonSocial");
		
		if ($_GET['gerencia_NU_IdGerencia']!=''){
			$_SESSION['AL_NombreGerencia'] = $objConexion->obtenerElemento($RS,0,"AL_NombreGerencia");		
		}else{
			$_SESSION['AL_NombreGerencia'] = '';		
		}

	///// CONVIERTE FECHA 1980-07-04 A 04/07/1980 (FORMATO ESPANOL)
	function setFechaNOSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	///////////// CONVERTIR DECIMALES A ESPANOL ///////////
	function setDecimalEsp($numero){
		$numero = str_replace(".", ",", $numero);
		return $numero;
	}

?>
<?php
//////////////////////[ CONVERSION A PDF ]///////////////////////////////

class PDF extends FPDF{
	function Header(){
		$this->Image('../../../images/head.jpg',10,5,380,10);
		$this->Ln(10);
		$this->SetFillColor(232,232,232);	
		$this->Cell(0,0,'',1,0,'C');
		$this->Image('../../../images/logo.jpg',200,23,40,0);
		$this->SetLeftMargin(65);
		$this->Ln(3);
		$this->SetFont('Arial','',11);
		$this->Ln(2);
		$this->Cell(0,0,'RELACI�N DE DESCUENTO DE LAS ORDENES DE COMPRAS DEL',0,0,'C');
		$this->Ln(5);
		$this->Cell(0,0,'MERCADO CORPORATIVO DE VENALCASA',0,0,'C');
		$this->Ln(5);
		$this->Cell(0,0,'Direcci�n General de Recursos Humanos',0,0,'C');
		$this->Ln(4);
		$this->Cell(0,0,setFechaNOSQL($_SESSION['fecha']),0,0,'C');
		$this->SetLeftMargin(10);				
		$this->Ln(6);
		$this->Cell(0,0,'',1,0,'C');
		$this->SetFont('Arial','B',11);
		$this->Ln(3);
		$this->Cell(0,0,'Correspondiente al Mercado de: '.$_SESSION['AF_RazonSocial'].', '.$_SESSION['AL_NombreGerencia'].' - Nro.: DGRH-GBS-M0'.$_SESSION['mercado_NU_IdMercado'],0,0,'C');		
		$this->Ln(2);		
		$this->Cell(0,0,'',1,0,'C');
		$this->SetFont('Arial','B',10);
		$this->Ln(1);
		$this->SetLeftMargin(10);
		$this->SetFillColor(232,232,232);		
		
		$this->SetFont('Arial','B',7);
		//ESPACIO 100% DE LA TABLA = 190 EN VERTICAL////////////////////////
		//ESPACIO 100% DE LA TABLA = 277 EN HORIZONTAL////////////////////////
		
		$this->Cell(100,7,'Empleados',1,0,'C',1);
				
		for($b=0; $b<$_SESSION[cant]; $b++){
			$this->Cell(30,7,utf8_decode($_SESSION[producto][$b]),1,0,'C',1);
		}
		
		$this->Cell(17,7,'Total',1,0,'C',1);
		$this->Cell(17,7,'Total',1,0,'C',1);		

		$this->Ln(7);		

		$this->Cell(8,7,'#',1,0,'C',0);
		$this->Cell(77,7,'Nombre y Apellido',1,0,'C',0);
		$this->Cell(15,7,'Cedula',1,0,'C',0);

		for($y=0; $y<$_SESSION[cant]; $y++){
			
			$this->Cell(15,7,$_SESSION['maxi'][$y].' '.$_SESSION[medida][$y].' Max',1,0,'C',0);	
			$this->Cell(15,7,'Bs. '.number_format($_SESSION[precio][$y],2,',','.'),1,0,'C',0);
		}	

		$this->Cell(17,7,'Rubros',1,0,'C',1);
		$this->Cell(17,7,'Bs.',1,0,'C',1);		
		

		$this->Ln(7);			

	}
	function Footer()	{
		$this->SetY(-20);
		$this->Ln(5);
		$this->SetFont('Arial','B',7);
		$this->Cell(0,0,'Sistema elaborado por la Oficina de Tecnolog�a de la Informaci�n.',0,0,'C');
		$this->Ln(5);
		$this->Cell(0,0,'Venezolana de Alimentos La Casa S.A.',0,0,'C');		
		$this->Ln(5);		
		$this->SetFont('Arial','I',7);
		$this->Cell(0,0,'P�gina '.$this->PageNo().' de {nb}',0,0,'C');
//		$this->Cell(0,0,'P�gina �nica',0,0,'C');
		$this->Image('../../../images/footer.jpg',10,191,640,1);
						
	}
}

//Creacin del objeto de la clase heredada

$pdf=new PDF('L','mm',array(210,670));

$pdf->AliasNbPages();
$pdf->AddPage();

$color=0;
$contador =1;
$persona = '';
for($i=0;$i<$cantRSPedidoDetalle;$i++){
/*	
	if ($contador==15){
		$pdf->AddPage('L');
		$contador=0;
	}	
	
	$contador++;
	*/
	$pdf->SetFont('Arial','',7);
	
	$AL_NombreApellido 	= $objConexion->obtenerElemento($RSPedidoDetalle,$i,"AL_Nombre").' '.$objConexion->obtenerElemento($RSPedidoDetalle,$i,"AL_Apellido");
	$NU_Cedula 			= $objConexion->obtenerElemento($RSPedidoDetalle,$i,"NU_Cedula");
	
	$NU_Cantidad 		= $objConexion->obtenerElemento($RSPedidoDetalle,$i,"NU_Cantidad");	
	$Costo 				= $objConexion->obtenerElemento($RSPedidoDetalle,$i,"Costo");	

	/*
	$totalKg 			= $NU_Max*$Solicitantes;
	$totalBs			= $totalKg*$BS_PrecioUnitario;
*/

	$pdf->SetFillColor(248,248,248);

	
	if($color==1){
		//$pdf->SetFillColor(248,248,248);			
		$color=0;
	}else{
		//$pdf->SetFillColor(229,229,229);					
		$color=1;	
	}

	if ($NU_Cedula!=$persona){
		$pdf->Ln(7);		
		$pdf->Cell(8,7,$contador,1,0,'C',1);		
		$pdf->Cell(77,7,ucwords(strtolower(utf8_decode($AL_NombreApellido))),1,0,'L',1);	
		$pdf->Cell(15,7,$NU_Cedula,1,0,'R',1);	
		$persona = $NU_Cedula;
		$contador++;
	}

	$pdf->Cell(15,7,$NU_Cantidad,1,0,'C',1);
	$pdf->Cell(15,7,$Costo,1,0,'C',1);
	/*

	$pdf->Cell(30,7,$CantProducto,1,0,'C',1);
	$pdf->Cell(30,7,number_format($MontoBruto,2,',','.').' BsF.',1,0,'R',1);	
	$pdf->Cell(30,7,number_format($BS_NotaCredito,2,',','.').' BsF.',1,0,'R',1);
	$pdf->Cell(30,7,number_format($MontoDescontar,2,',','.').' BsF.',1,0,'R',1);	
	$pdf->Ln(7);
	/*
	$total_NU_Max 		+= $NU_Max;
	$total_Solicitantes += $Solicitantes ;
	$total_totalBs 		+= $totalBs;
	*/
	
	
}
/*
$pdf->SetFillColor(232,232,232);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(93,7,'TOTAL PRODUCTOS Y MONTO TOTAL DE LA COMPRA',0,0,'R',0);
$pdf->Cell(25,7,$total_NU_Max,1,0,'R',1);
$pdf->Cell(25,7,$total_Solicitantes,1,0,'R',1);	
$pdf->Cell(20,7,$total_NU_Max*$total_Solicitantes,1,0,'R',1);	
$pdf->Cell(27,7,setDecimalEsp($total_totalBs).' BsF.',1,0,'R',1);
$pdf->Ln(5);
$pdf->Ln(30);
$pdf->Cell(0,10,'LDA. YURISMAR MEDINA',0,0,'C');
$pdf->Ln(5);
$pdf->Cell(0,10,'DIRECTORA GENERAL DE RECURSOS HUMANOS',0,0,'C');
*/
$pdf->SetLeftMargin(10);
$pdf->Ln(5);


$pdf->Ln(3);
$pdf->Output();

}
?>