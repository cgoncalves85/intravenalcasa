﻿<?php 
	require_once('../../../controller/sessionController.php'); 
	require_once('../../../model/mercadoModel.php');
	require_once('../../../model/sedeModel.php');
	require_once('../../../model/gerenciaModel.php');

	$objMercado 	= new Mercado();
	$objSede		= new Sede();
	$objGerencia	= new Gerencia();


	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	//////////////////////////////////////////////////////////////
		///////////// CONVERTIR DECIMALES A ESPANOL ///////////
	function setDecimalEsp($numero){
		$numero = str_replace(".", ",", $numero);
		return $numero;
	}

	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <span class="ui-icon ui-icon-circle-check"></span>
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Consultas</h4>
						<form role="form" id="form" method="POST" action="../../../controller/descuentoNController.php" target="_blank">	
							<div class="panel panel-default">
								<div class="panel-heading">
									Consultas &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Relación de Descuentos de Ordenes de Compra.
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-2"></div>
										<div class="col-lg-6">	
											<br>
											<div class="form-group" align="left">
												<div class="col-lg-12">
													<label for="mercado">Nro. Mercado:</label><br>
													<select name="mercado_NU_IdMercado" required id="mercado_NU_IdMercado" class="form-control placeholder">
														<option value="">[ Seleccione ]</option>
														<?php 
														$RSMercado = $objMercado->listarMercado($objConexion);
														$cantRSMercado 	= $objConexion->cantidadRegistros($RSMercado);
														for($i=0;$i<$cantRSMercado;$i++) {
															$value=$objConexion->obtenerElemento($RSMercado,$i,"NU_IdMercado");
															$des = 'DGRH-GBS-M0';
															$des.=$objConexion->obtenerElemento($RSMercado,$i,"NU_IdMercado");
															$des.=" de: ".$objConexion->obtenerElemento($RSMercado,$i,"AF_RazonSocial");
															$selected="";
															echo "<option value=".$value." ".$selected.">".$des."</option>";
														}
														?>
													</select>
												</div>
											</div>
									
											<div class="col-lg-12">&nbsp;</div>
											
											<div class="form-group" align="left">	
										
												<div class="col-lg-12">
													<label for="sede">Sede :</label>
													<select name="sede_NU_IdSede" required id="sede_NU_IdSede" onChange="cargaContenido(this.id)" class="form-control placeholder">
														<option selected="selected">[ Seleccione ]</option>
														<?php 
															$RSSede 		= $objSede->listar($objConexion);
															$cantRSSede 	= $objConexion->cantidadRegistros($RSSede);
															for($i=0;$i<$cantRSSede;$i++){
																$value	= $objConexion->obtenerElemento($RSSede,$i,"NU_IdSede");
																$des		= $objConexion->obtenerElemento($RSSede,$i,"AL_NombreSede");
																$selected	= "";

																echo "<option value=".$value." ".$selected.">".$des."</option>";
															}  
														?>
													</select>
													<br>
												</div>
												
											</div>

											<div class="form-group" align="left">	
										
												<div class="col-lg-12">
													<label for="dependencia">Dependencia :</label>
													<select name="gerencia_NU_IdGerencia" disabled id="gerencia_NU_IdGerencia" class="form-control">
														<option>[ Seleccione ]</option>
													</select>
													<br>
												</div>
												
											</div>

											<div class="form-group" align="left">	
												<div class="col-lg-12">
													<label for="Formato_Archivo">Formato del Archivo:</label><br>
													<select name="formato" required id="formato" class="form-control">
														<option value="">[ Seleccione ]</option>
														<option value="pdf">PDF</option>
														<option value="xls">EXCEL</option>
													</select>
													<br>
												</div>
											</div>											
											
											<div class="col-lg-12" align="right">
												<br>
												<button type="submit" name="submit" id="submit" class="btn btn-primary">Ver Relación de Descuento</button>
												<input name="origen" type="hidden" id="origen" value="descuento_nomina">
												<br><br>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>	
					</div>
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>
	
	<!-- Custom JavaScript -->
	<script type="text/javascript" src="../../../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../../../js/mensajes.js"></script>
	<script type="text/javascript" src="../../../js/select_Sede.js"></script>

</body>

</html>
