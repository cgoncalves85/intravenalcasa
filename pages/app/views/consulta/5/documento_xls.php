<?php
//error_reporting(E_ALL);
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
ini_set('display_errors', '1');
?>
<?php
	require_once('../../../controller/sessionController.php');
	require_once '../../../includes/PHPExcel/PHPExcel.php';
	require_once '../../../includes/PHPExcel/PHPExcel/Writer/Excel2007.php';	

	require_once('../../../model/pedidoDetalleModel.php');
	require_once('../../../model/mercadoProductoModel.php');
	require_once('../../../model/sedeModel.php');
	require_once('../../../model/gerenciaModel.php');	

	$NU_IdMercado 			= $_GET['NU_IdMercado'];
	$gerencia_NU_IdGerencia = $_GET['gerencia_NU_IdGerencia'];
	
	$objPedidoDetalle 	= new PedidoDetalle();
	$objMercadoProducto = new MercadoProducto();

	////////// CONSULTAS ////////
	$RSMercadoProducto		= $objMercadoProducto->cantProducXmerc($objConexion,$NU_IdMercado);
	$cRSMercadoProducto		= $objConexion->cantidadRegistros($RSMercadoProducto);
	
	$RSPedidoDetalle		= $objPedidoDetalle->RelacionDescuentoXgerencia($objConexion,$NU_IdMercado,$gerencia_NU_IdGerencia);
	$cantRSPedidoDetalle	= $objConexion->cantidadRegistros($RSPedidoDetalle);
	
	$AL_NombreSede = $objConexion->obtenerElemento($RSPedidoDetalle,0,'AL_NombreSede');
	$AL_NombreGerencia = $objConexion->obtenerElemento($RSPedidoDetalle,0,'AL_NombreGerencia');
	
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	
	// PROPIEDADES DEL LIBRO
	$objPHPExcel->getProperties()->setCreator("Venezolana de Alimentos La Casa, VENALCASA S.A."); //Autor
	$objPHPExcel->getProperties()->setLastModifiedBy("Sistema elaborado por la Ofic. Asuntos Institucionales e Internacionales"); //ultimo cambio
	$objPHPExcel->getProperties()->setTitle("Mercado Corporativo de VENALCASA");
	$objPHPExcel->getProperties()->setSubject("Relación de Descuento");
	$objPHPExcel->getProperties()->setDescription("Relación de Descuento");
	
	// Establecer la hoja activa, para que cuando se abra el documento se muestre primero
	$objPHPExcel->setActiveSheetIndex(0);

	///////// ESTILOS DE CELDAS ///////
	$BarraHead = array(
		'font'    	=> array(
			'bold'      => true,
		),
		'alignment' => array(
			'horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			'vertical' 		=> PHPExcel_Style_Alignment::VERTICAL_CENTER,				
		)
	);
		
	$BarraProducto = array(
		'font'    	=> array(
			'bold'      => true,
			'size'		=> 8,
			'name'		=> 'Arial',
		),
		'ColumnDimension'	=> array(
			'Width' => 3,
			'height' => 70,
		),
		'alignment' => array(
			'horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical' 		=> PHPExcel_Style_Alignment::VERTICAL_CENTER,				
			'WrapText'		=> true,
		),
		'borders' 	=> array(
			'allborders' 	=> array(
				'style' 		=> PHPExcel_Style_Border::BORDER_THIN,
			),
		),
		'fill' => array(
			'type'       	=> PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' 	=> array(
				'argb' 			=> 'CCCCCC'
			)
		),
	);
	
	$BarraTitulo = array(
		'font'    	=> array(
			'bold'      => true,
			'size'		=> 8,
			'name'		=> 'Arial',
		),
		'alignment' => array(
			'horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical' 		=> PHPExcel_Style_Alignment::VERTICAL_CENTER,				
		),
		'borders' 	=> array(
			'allborders' 	=> array(
				'style' 		=> PHPExcel_Style_Border::BORDER_THIN,
			),
		),
/*		'ColumnDimension' => array(
			'Width'	=> 9,
		),*/
	);	

	$BarraMedidas = array(
		'font'    	=> array(
			'bold'      => true,
			'size'		=> 8,
			'name'		=> 'Arial',
		),
		'alignment' => array(
			'horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical' 		=> PHPExcel_Style_Alignment::VERTICAL_CENTER,				
			'WrapText'		=> true,
		),
		'borders' 	=> array(
			'allborders' 	=> array(
				'style' 		=> PHPExcel_Style_Border::BORDER_THIN,
			),
		),
		'fill' => array(
			'type'       	=> PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' 	=> array(
				'argb' 			=> 'CCCCCC'
			)
		),

	);
	
	$Datos = array(
		'font'    	=> array(
			'bold'      => false,
			'size'		=> 8,
			'name'		=> 'Arial',
		),
		'alignment' => array(
			'horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'vertical' 		=> PHPExcel_Style_Alignment::VERTICAL_CENTER,				
		),			
		'borders' 	=> array(
			'allborders' 	=> array(
				'style' 		=> PHPExcel_Style_Border::BORDER_THIN,
			),
		)
	);
	
	$Borde = array(
		'font'    	=> array(
			'bold'      => false,
			'size'		=> 8,
			'name'		=> 'Arial',
		),	
		'borders' 	=> array(
			'allborders' 	=> array(
				'style' 		=> PHPExcel_Style_Border::BORDER_THIN,
			),
		)
	);

	$Total = array(
		'font'    	=> array(
			'bold'      => true,
			'size'		=> 8,
			'name'		=> 'Arial',
		),
		'alignment' => array(
			'horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'vertical' 		=> PHPExcel_Style_Alignment::VERTICAL_CENTER,				
		)	
	);

	$TotalF = array(
		'font'    	=> array(
			'bold'      => true,
			'size'		=> 8,
			'name'		=> 'Arial',
		),
		'alignment' => array(
			'horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			'vertical' 		=> PHPExcel_Style_Alignment::VERTICAL_CENTER,				
		),
		'borders' 	=> array(
			'allborders' 	=> array(
				'style' 		=> PHPExcel_Style_Border::BORDER_THIN,
			),
		)			
	);
		
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);	
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);		
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(9);
	
	$incrementador 	= 0;
	$ColZ 			= '';
	$doble			= 0;
	for ($z=0; $z<(($cRSMercadoProducto*2)+2); $z++){

		$ColZ				= chr(68+$incrementador);
		if ($ColZ=='Z'){ 
			$incrementador = 0;
			$letra		= $ColZ;					
			$doble			= 1;			
		}else{
			if ($doble==1){
				$letra		= 'A'.chr(64+$incrementador);						
			}else{
				$letra		= chr(68+$incrementador);						
			}
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension($letra)->setWidth(8);
		$incrementador++;
	}
	/////// FIN DE ESTILOS
		
	//////////// AGREGAR HEAD1
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setName('Logo');
	$objDrawing->setDescription('Logo');
	$objDrawing->setPath('../../../images/head1.jpg');
	$objDrawing->setHeight(30);
	$objDrawing->setCoordinates('A1');
	$objDrawing->setOffsetX(0);
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

	//////////// AGREGAR HEAD2
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setName('Logo');
	$objDrawing->setDescription('Logo');
	$objDrawing->setPath('../../../images/head2.jpg');
	$objDrawing->setHeight(30);
	$objDrawing->setCoordinates('Q1');
	$objDrawing->setOffsetX(0);
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

	//////////// LOGO VENALCASA
	$objDrawing = new PHPExcel_Worksheet_Drawing();
	$objDrawing->setName('Logo');
	$objDrawing->setDescription('Logo');
	$objDrawing->setPath('../../../images/logo.jpg');
	$objDrawing->setHeight(80);
	$objDrawing->setCoordinates('A3');
	$objDrawing->setOffsetX(0);
	$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());

	//********** ESCRIBIR TITULO *************************************
	$UltColumna		= chr(68+$cRSMercadoProducto).'4';
	$tituloReporte 	= strtoupper("Relación Ordenes de Compras del Mercado Corporativo de VENALCASA: DGRH-GBS-M0").$NU_IdMercado;
	$objPHPExcel->getActiveSheet()->mergeCells('D3:'.$UltColumna);
	$objPHPExcel->getActiveSheet()->getStyle('D3')->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray($BarraHead);
	$objPHPExcel->getActiveSheet()->SetCellValue('D3', $tituloReporte);	

	$UltColumna		= chr(68+$cRSMercadoProducto).'5';
	$objPHPExcel->getActiveSheet()->mergeCells('D5:'.$UltColumna);
	$objPHPExcel->getActiveSheet()->getStyle('D5')->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($BarraHead);
	$objPHPExcel->getActiveSheet()->SetCellValue('D5', 'UBICACIÓN ADMINISTRATIVA: '.$AL_NombreSede.' - '.$AL_NombreGerencia);	

	$UltColumna		= chr(68+$cRSMercadoProducto).'6';
	$objPHPExcel->getActiveSheet()->mergeCells('D6:'.$UltColumna);
	$objPHPExcel->getActiveSheet()->getStyle('D6')->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($BarraHead);
	$objPHPExcel->getActiveSheet()->SetCellValue('D6', 'NÚMERO DE ORDEN: ');

	////// CUERPO DEL ARCHIVO DE EXCEL - CONTENIDO - ////////////

	//****** BARRA DE PRODUCTOS ******************************
	$objPHPExcel->getActiveSheet()->mergeCells('A8:C8');
	$objPHPExcel->getActiveSheet()->getStyle('A8:C8')->applyFromArray($BarraProducto);
	$objPHPExcel->getActiveSheet()->SetCellValue('A8', 'Empleados');
	
	$fila = 8;
	$incrementador = 0;
	$doble = 0;
	$ColZ = '';
	for ($a=0;$a<$cRSMercadoProducto;$a++){
		$AF_NombreProducto 	= $objConexion->obtenerElemento($RSPedidoDetalle,$a,"AF_NombreProducto");

		$ColZ				= chr(68+$incrementador);
		if ($ColZ=='Z'){ 
			$Columna1		= chr(68+$incrementador).$fila;
			$incrementador 	= 0;
			$doble			= 1;			
		}else{
			if ($doble==1){
				$Columna1		= 'A'.chr(64+$incrementador).$fila;			
			}else{
				$Columna1		= chr(68+$incrementador).$fila;			
			}
		}

		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($BarraProducto);
		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->getAlignment()->setTextRotation(-90);						
		$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $AF_NombreProducto);

		$incrementador++;
	}
	
	$Columna1		= chr(68+$incrementador).$fila;	
	$Columna2		= chr(68+$incrementador).($fila+1);				

	$objPHPExcel->getActiveSheet()->mergeCells($Columna1.':'.$Columna2);	
	$objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($BarraProducto);
	$objPHPExcel->getActiveSheet()->getStyle($Columna1)->getAlignment()->setTextRotation(-90);							
	$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, 'Total Rubros');
	
	$ColRubros = chr(68+$incrementador);
	
	//***** BARRA DE TITULOS ******************************
	$fila++;
	$Columna		= chr(65).$fila;
	$objPHPExcel->getActiveSheet()->getStyle($Columna)->applyFromArray($BarraTitulo);
	$objPHPExcel->getActiveSheet()->SetCellValue($Columna, 'Nº');

	$Columna		= chr(66).$fila;
	$objPHPExcel->getActiveSheet()->getStyle($Columna)->applyFromArray($BarraTitulo);
	$objPHPExcel->getActiveSheet()->SetCellValue($Columna, 'Nombre y Apellido');

	$Columna		= chr(67).$fila;
	$objPHPExcel->getActiveSheet()->getStyle($Columna)->applyFromArray($BarraTitulo);
	$objPHPExcel->getActiveSheet()->SetCellValue($Columna, 'Cédula');

	$incrementador 	= 0;
	$ColZ 			= '';
	$doble			= 0;				
	for ($b=0;$b<$cRSMercadoProducto;$b++){
		
		$NU_Max 			= $objConexion->obtenerElemento($RSPedidoDetalle,$b,"NU_Max");
		$AL_Medida 			= $objConexion->obtenerElemento($RSPedidoDetalle,$b,"AL_Medida");

		$ColZ				= chr(68+$incrementador);
		if ($ColZ=='Z'){ 
			$Columna1		= chr(68+$incrementador).$fila;
			$incrementador = 0;
			$doble			= 1;			
		}else{
			if ($doble==1){
				$Columna1		= 'A'.chr(64+$incrementador).$fila;			
			}else{
				$Columna1		= chr(68+$incrementador).$fila;			
			}
		}

		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($BarraTitulo);
		$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $NU_Max.' '.$AL_Medida.' Max');

		$incrementador++;	
	}

	//***** LISTAR EMPLEADOS CON SUS DATOS ******************************	
	$ultimaLetra = substr($Columna2, 0, -1);
	$fila++;
	$incrementador 	= 0;
	$ColZ 			= '';
	$doble			= 0;
	$contador		= 0;
	$contador2		= 0;
	$totalRubrosT	= 0;
	$totalRubros	= 0;

	for ($c=0;$c<$cantRSPedidoDetalle;$c++){
		$AL_Nombre 		= $objConexion->obtenerElemento($RSPedidoDetalle,$c,"AL_Nombre");
		$AL_Apellido 	= $objConexion->obtenerElemento($RSPedidoDetalle,$c,"AL_Apellido");
		$NU_Cedula 		= $objConexion->obtenerElemento($RSPedidoDetalle,$c,"NU_Cedula");
		$NU_Cantidad 	= $objConexion->obtenerElemento($RSPedidoDetalle,$c,"NU_Cantidad");
		$totalRubrosT	= $totalRubrosT + $NU_Cantidad;
		$totalRubros	= $totalRubros + $NU_Cantidad;

		$ColZ				= chr(68+$incrementador);
		if ($ColZ=='Z'){ 
			$Columna1		= chr(68+$incrementador).$fila;
			$incrementador  = 0;
			$doble			= 1;			
		}else{
			if ($doble==1){
				$Columna1		= 'A'.chr(64+$incrementador).$fila;			
			}else{
				$Columna1		= chr(68+$incrementador).$fila;			
			}
		}

		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($Datos);
		$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $NU_Cantidad);

		$incrementador++;
		$contador++;
		
		if ($contador == $cRSMercadoProducto){
			$incrementador 	= 0;
			$doble			= 0;
			$contador		= 0;
			$contador2++;

			$Columna		= 'A'.$fila;

			$objPHPExcel->getActiveSheet()->SetCellValue($Columna, $contador2);		
			$Columna		= 'B'.$fila;

			$objPHPExcel->getActiveSheet()->SetCellValue($Columna, $AL_Nombre.' '.$AL_Apellido);		
			$Columna		= 'C'.$fila;

			$objPHPExcel->getActiveSheet()->SetCellValue($Columna, $NU_Cedula);				

			$objPHPExcel->getActiveSheet()->SetCellValue($ColRubros.$fila, $totalRubros);		

			$totalRubros	= 0;
			$fila++;
		}
	}
		
	$ultimaLetra = $ultimaLetra.$fila;
	$objPHPExcel->getActiveSheet()->getStyle('A10:'.$ultimaLetra)->applyFromArray($Borde);
	$objPHPExcel->getActiveSheet()->getStyle('D10:'.$ultimaLetra)->applyFromArray($Datos);

	//***** TOTALES ******************************		
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$fila.':C'.$fila);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$fila)->applyFromArray($Total);
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, 'TOTALES');	

	$incrementador 	= 0;
	$ColZ 			= '';
	$doble			= 0;
	$solicitado;				

	for ($d=0;$d<$cRSMercadoProducto;$d++){

		$ColZ				= chr(68+$incrementador);
		if ($ColZ=='Z'){ 
			$Columna1		= chr(68+$incrementador).$fila;
			$letraP1		= chr(68+$incrementador);
			$incrementador  = 0;
			$doble			= 1;			
		}else{
			if ($doble==1){
				$Columna1		= 'A'.chr(64+$incrementador).$fila;			
				$letraP1		= 'A'.chr(64+$incrementador);
			}else{
				$Columna1		= chr(68+$incrementador).$fila;			
				$letraP1		= chr(68+$incrementador);
			}
		}

		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($Total);
		$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, '=SUM('.$letraP1.'10:'.$letraP1.($fila-1).')');
		
		$incrementador++;
	}	
	
	$ColRubros = chr(68+$incrementador);

	$objPHPExcel->getActiveSheet()->getStyle($ColRubros.$fila)->applyFromArray($Borde);
	$objPHPExcel->getActiveSheet()->getStyle($ColRubros.$fila)->applyFromArray($Total);
	$objPHPExcel->getActiveSheet()->SetCellValue($ColRubros.$fila, $totalRubrosT);		

	///////// SECCION DE CALCULO DE BULTOS/CAJA Y UNIDAD
	
	$fila 		= $fila + 2;
	$ColZ 			= '';
	$incrementador 	= 0;
	
	$objPHPExcel->getActiveSheet()->mergeCells('B'.$fila.':C'.($fila+1));
	$objPHPExcel->getActiveSheet()->getStyle('B'.$fila.':C'.($fila+1))->applyFromArray($BarraMedidas);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila, 'MEDIDAS');
	
	// MEDIDA
	for ($c=0;$c<$cRSMercadoProducto;$c++){
		$AF_NombreProducto	= $objConexion->obtenerElemento($RSMercadoProducto,$c,"AF_NombreProducto");
		$NU_BultoCaja		= $objConexion->obtenerElemento($RSMercadoProducto,$c,"NU_BultoCaja");
		$AL_Medida 			= $objConexion->obtenerElemento($RSMercadoProducto,$c,"AL_Medida");

		$ColZ				= chr(68+$incrementador);
		if ($ColZ=='Z'){ 
			$Columna1		= chr(68+$incrementador).$fila;
			$incrementador  = 0;
			$doble			= 1;			
			$Columna2		= 'A'.chr(64+$incrementador).($fila+1);														
		}else{
			if ($doble==1){
				$Columna1		= 'A'.chr(64+$incrementador).$fila;			
				$Columna2		= 'A'.chr(64+$incrementador).($fila+1);											
			}else{
				$Columna1		= chr(68+$incrementador).$fila;			
				$Columna2		= chr(68+$incrementador).($fila+1);							
			}
		}
		
		switch ($AF_NombreProducto) {
			case 'Carne':
				$medida = 'KG';
				break;
			case 'Pollo':
				$medida = $NU_BultoCaja.' KG* '.$AL_Medida;		
				break;
			default:
				$medida = $NU_BultoCaja.' UND* '.$AL_Medida;			
				break;
		}		

		$objPHPExcel->getActiveSheet()->mergeCells($Columna1.':'.$Columna2);
		$objPHPExcel->getActiveSheet()->getStyle($Columna1.':'.$Columna2)->applyFromArray($BarraMedidas);
		$objPHPExcel->getActiveSheet()->getStyle($Columna1.':'.$Columna2)->getAlignment()->setWrapText(true);				
		$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $medida);
		
		$incrementador++;
	}
	
	$fila = $fila + 2;
	
	$ColZ 			= '';
	$incrementador 	= 0;
	
	$objPHPExcel->getActiveSheet()->mergeCells('B'.$fila.':C'.$fila);
	$objPHPExcel->getActiveSheet()->getStyle('B'.$fila.':C'.$fila)->applyFromArray($BarraProducto);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila, 'BULTOS / CAJAS');

	// CALCULO BULTOS
	for ($c=0;$c<$cRSMercadoProducto;$c++){
		$AF_NombreProducto	= $objConexion->obtenerElemento($RSMercadoProducto,$c,"AF_NombreProducto");
		$NU_BultoCaja		= $objConexion->obtenerElemento($RSMercadoProducto,$c,"NU_BultoCaja");
		$AL_Medida 			= $objConexion->obtenerElemento($RSMercadoProducto,$c,"AL_Medida");
		
		$ColZ				= chr(68+$incrementador);
		if ($ColZ=='Z'){ 
			$Columna1		= chr(68+$incrementador).$fila;
			$Letra			= chr(68+$incrementador);						
			$incrementador  = 0;
			$doble			= 1;			
		}else{
			if ($doble==1){
				$Columna1		= 'A'.chr(64+$incrementador).$fila;			
				$Letra			= 'A'.chr(64+$incrementador);							
			}else{
				$Columna1		= chr(68+$incrementador).$fila;			
				$Letra			= chr(68+$incrementador);							
			}
		}

		switch ($AF_NombreProducto) {
			case 'Carne':
				$bulto = '';
				break;
			case 'Pollo':
				$bulto = '';		
				break;
			default:
				$bulto = '=INT(SUM('.$Letra.($fila-4).')/'.$NU_BultoCaja.')';	
				break;
		}		


		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($Datos);
		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->getAlignment()->setWrapText(true);				
		$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $bulto);
		
		$incrementador++;
	}

	$fila++;
	$ColZ 			= '';
	$incrementador 	= 0;
	
	$objPHPExcel->getActiveSheet()->mergeCells('B'.$fila.':C'.$fila);
	$objPHPExcel->getActiveSheet()->getStyle('B'.$fila.':C'.$fila)->applyFromArray($BarraProducto);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila, 'UNIDADES');

	// CALCULO UNIDAD

		
	for ($c=0;$c<$cRSMercadoProducto;$c++){
		$AF_NombreProducto	= $objConexion->obtenerElemento($RSMercadoProducto,$c,"AF_NombreProducto");
		$NU_BultoCaja		= $objConexion->obtenerElemento($RSMercadoProducto,$c,"NU_BultoCaja");
		
		$ColZ				= chr(68+$incrementador);
		if ($ColZ=='Z'){ 
			$Columna1		= chr(68+$incrementador).$fila;
			$Letra			= chr(68+$incrementador);			
			$incrementador  = 0;
			$doble			= 1;			
		}else{
			if ($doble==1){
				$Columna1		= 'A'.chr(64+$incrementador).$fila;			
				$Letra			= 'A'.chr(64+$incrementador);							
			}else{
				$Columna1		= chr(68+$incrementador).$fila;			
				$Letra			= chr(68+$incrementador);											
			}
		}

		switch ($AF_NombreProducto) {
			case 'Carne':
				$bulto = '=('.$Letra.($fila-5).')';
				break;
			case 'Pollo':
				$bulto = '=('.$Letra.($fila-5).')';		
				break;
			default:
				$bulto = '=SUM('.$Letra.($fila-5).'-SUM('.$Letra.($fila-1).')*'.$NU_BultoCaja.')';			
				break;
		}		

		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($Datos);
		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->getAlignment()->setWrapText(true);				
		$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $bulto);
		
		$incrementador++;
	}		
	
	$fila++;
	$ColZ 			= '';
	$incrementador 	= 0;
	
	$objPHPExcel->getActiveSheet()->mergeCells('B'.$fila.':C'.$fila);
	$objPHPExcel->getActiveSheet()->getStyle('B'.$fila.':C'.$fila)->applyFromArray($BarraProducto);
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$fila, 'TOTAL');

	// CALCULO TOTALES
		
	for ($c=0;$c<$cRSMercadoProducto;$c++){
		$AF_NombreProducto	= $objConexion->obtenerElemento($RSMercadoProducto,$c,"AF_NombreProducto");
		$NU_BultoCaja		= $objConexion->obtenerElemento($RSMercadoProducto,$c,"NU_BultoCaja");
		
		$ColZ				= chr(68+$incrementador);
		if ($ColZ=='Z'){ 
			$Columna1		= chr(68+$incrementador).$fila;
			$Letra			= chr(68+$incrementador);			
			$incrementador  = 0;
			$doble			= 1;			
		}else{
			if ($doble==1){
				$Columna1		= 'A'.chr(64+$incrementador).$fila;			
				$Letra			= 'A'.chr(64+$incrementador);							
			}else{
				$Columna1		= chr(68+$incrementador).$fila;			
				$Letra			= chr(68+$incrementador);											
			}
		}

		$total = '=('.$Letra.($fila-6).')';		

		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->applyFromArray($TotalF);
		$objPHPExcel->getActiveSheet()->getStyle($Columna1)->getAlignment()->setWrapText(true);				
		$objPHPExcel->getActiveSheet()->SetCellValue($Columna1, $total);
		
		$incrementador++;
	}		
	///////// PIE DE PAGINA
	
	$fila 		= $fila + 4;
	$UltColumna = $ColRubros.$fila;	
	$objPHPExcel->getActiveSheet()->mergeCells('A'.$fila.':'.$UltColumna);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$fila)->getFont()->setSize(6);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$fila)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);																				
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, 'Sistema elaborado por la Oficina de Asuntos Institucionales e Internacionales de VENALCASA, S.A. G-20008504-5');	
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);

	for ($b=0; $b<($cantRSProducMercado+2); $b++){
		$letra = chr(67+$b);	
		$objPHPExcel->getActiveSheet()->getColumnDimension($letra)->setWidth(8);	
	}
/**/
	/////////////////CREACION DEL DOCUMENTO	
	// Orientacion y tamano de la pagina
	$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

	// Cambiar nombre de la Hoja
	$objPHPExcel->getActiveSheet()->setTitle('Relación');
	
	// Guardar Archivo en formato Excel 2007
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save(str_replace('.php', '.xlsx', __FILE__));

	// Abrir Documento
	
	header("Location: documento_xls.xlsx");
?>