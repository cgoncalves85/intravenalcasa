﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/medidaModel.php'); 	

	$objMedida 	= new Medida();	
	
	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}	
	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Unidades de Medidas</h3>
						<form role="form" id="form" method="POST" action="../../controller/medidaController.php">
							<div class="panel panel-default">
								<div class="panel-heading">
									Configuración &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Unidad de Medida.
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-1"></div>
										<div class="col-lg-11">	
											<br>
											<div class="form-group">
												<div class="col-lg-5">
													<label for="medida">Unidad de Medida :</label><br>
													<input type="text" name="NombreMedida" required id="NombreMedida" class="form-control">
												</div>
												<div class="col-lg-5">
													<label for="abreviatura">Abreviatura</label>(Kg, Lt, Gr.) :<br>
													<input type="text" name="AL_Medida" required id="AL_Medida" class="form-control">
												</div>
												
												<div class="col-lg-5"></div>
												<div class="col-lg-5" align="right">
													<?php 
														$k = 0;
														$rsMedida		= $objMedida->listarMedida($objConexion);
														$cantMedida		= $objConexion->cantidadRegistros($rsMedida);
													?> 
													<br>
													<input name="origen" type="hidden" id="origen" value="agregar_medida">
													<input name="cantMedida" type="hidden" id="cantMedida" value="<?php echo $cantMedida; ?>">
													<input name="Agregar_Medida" type="submit" class="btn btn-primary"  value="Agregar Unidad de Medida" id="Agregar_Medida">
												</div>
												<div class="col-lg-10">&nbsp;</div>
												
												
											</div>
										
										</div>
									</div>
								</div>
							</div>
						</form>

						<div class="panel panel-default">
							<div class="panel-heading">
								Configuración &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Listado de Unidades de Medidas.
							</div>						

							<div class="panel-body">
								<div class="row" align="center">

									<div class="dataTable_wrapper" style="margin:50px; width:70%">
						
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr >
													<td style="vertical-align:middle; width:40px" ><strong>ID</strong></td>
													<td style="vertical-align:middle;" ><strong>Nombre</strong></td>
													<td style="vertical-align:middle;  width:100px" ><strong>Abreviatura</strong></td>
													<td style="vertical-align:middle; width:60px"><strong>Editar</strong></td>
													<td style="vertical-align:middle; width:80px"><strong>Eliminar</strong></td>
												</tr>
											</thead>
     
											<tbody>
												<?php
													for($i=0; $i<$cantMedida; $i++){
														$NU_IdMedida	= $objConexion->obtenerElemento($rsMedida,$i,"NU_IdMedida");
														$NombreMedida	= $objConexion->obtenerElemento($rsMedida,$i,"NombreMedida");
														$AL_Medida		= $objConexion->obtenerElemento($rsMedida,$i,"AL_Medida");
												?>
													<tr align="center">
														<td style="vertical-align:middle;" ><strong><?php echo $NU_IdMedida ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $NombreMedida ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $AL_Medida ?></strong></td>
														<td style="vertical-align:middle;" ><a href="edit.php"><span class="glyphicon glyphicon-edit" title="Editar Unidad de Medida"></span></a></td>
														<td style="vertical-align:middle;" ><a href="delete.php"><span class="glyphicon glyphicon-trash" title="Eliminar Unidad de Medida"></span></a></td>
													</tr>
												<?php
													}
												?>          
											</tbody>
										</table>
									</div>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>	
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
