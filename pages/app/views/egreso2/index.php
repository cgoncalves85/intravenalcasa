﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/ctrabajo_tipoModel.php');	
	
	$objCTrabajo_Tipo 	= new CTrabajo_Tipo();
	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	
	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
		$AL_Adscripcion 		= $objConexion->obtenerElemento($RSUsuario,0,"AL_Adscripcion");	
	}	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <span class="ui-icon ui-icon-circle-check"></span>
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Constancias de Egreso</h4>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								Generar Constancias de Egreso
							</div>
							<div class="panel-body">
								<div class="row">
									<form class="form-horizontal" role="form" name="form1" method="POST" action="Constancia_Egreso.php" target="_blank">
										<div class="col-lg-2"></div>
										<div class="col-lg-7">
											<br>
											<div class="form-group">
												<label for="NU_Cedula" class="col-lg-7">Escoja el Tipo de Constancia que desea :</label>
												<div class="col-lg-5">
													<select name="NU_IdTipo" required id="NU_IdTipo" class="form-control">
														<option selected="selected"></option>
														<?php 
														$RSTipoCTrabajo		= $objCTrabajo_Tipo->listarTipos($objConexion);
														$cantRSTipoCTrabajo = $objConexion->cantidadRegistros($RSTipoCTrabajo);
														for($i=0;$i<$cantRSTipoCTrabajo;$i++){
															$value	= $objConexion->obtenerElemento($RSTipoCTrabajo,$i,"NU_IdTipoCTrabajo");
															$des	= $objConexion->obtenerElemento($RSTipoCTrabajo,$i,"AL_Tipo");
															echo "<option value=".$value.">".$des."</option>";
														}  
														?>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="FE_Egreso" class="col-lg-7">Fecha de Egreso :</label>
												<div class="col-lg-5">
													<input type="text" name="FE_Egreso" required id="FE_FechaNac1" class="form-control" placeholder="Ej. DD/MM/YYYY">
												</div>
											</div>							
											<div align="right">
												<br>
												<button type="submit" name="submit" id="submit" class="btn btn-primary">Generar Constancia</button>
												<br><br>
												<input name="origen" type="hidden" id="origen" value="CTrabajo">
											</div>
										</div>
										<div class="col-lg-3">&nbsp;</div>
									</form>	
								</div>
							</div>
						</div>

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../../js/maximizar.js"></script>
	<script type="text/javascript" src="../../js/desconectar.js"></script>
	<script type="text/javascript" src="../../js/funciones.js"></script>

</body>

</html>
