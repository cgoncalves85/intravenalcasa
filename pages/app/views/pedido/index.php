﻿<?php 
	require_once('../../controller/sessionController.php');
	require_once('../../model/usuarioModel.php'); 
	require_once('../../model/mercadoModel.php'); 	
	require_once('../../model/mercadoProductoModel.php'); 	
	require_once('../../model/pedidoModel.php'); 		
	
	$objUsuario 		= new Usuario();
	$objMercado 		= new Mercado();	
	$objMercadoProducto	= new MercadoProducto();
	$objPedido	 		= new Pedido();	
	
	$RSUsuario 	= $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantRS		= $objConexion->cantidadRegistros($RSUsuario);
	
	if($cantRS > 0){
		$empresa_NU_IdEmpresa = $objConexion->obtenerElemento($RSUsuario,0,"empresa_NU_IdEmpresa");
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
	}
	
	////////////// ACTUALIZAR MERCADOS ACTIVOS Y DESACTIVADOS /////////
	$RS 	= $objMercado->listarMercado($objConexion);
	$cantRS = $objConexion->cantidadRegistros($RS);	
	for($i=0; $i<$cantRS; $i++){
		$NU_IdMercado 	= $objConexion->obtenerElemento($RS,$i,'NU_IdMercado');
		$FE_Inicio 		= $objConexion->obtenerElemento($RS,$i,'FE_Inicio');
		$FE_Fin 		= $objConexion->obtenerElemento($RS,$i,'FE_Fin');			
		$objMercado->actualizarActivo($objConexion,$NU_IdMercado,$FE_Inicio,$FE_Fin);
	}
	///////////////////////////////////////////////////////////////////	
	
	$verificarActivo 			= $objMercado->verificarActivo($objConexion,$empresa_NU_IdEmpresa);
	$cantidadverificarActivo 	= $objConexion->cantidadRegistros($verificarActivo);

	if ($cantidadverificarActivo==0){
		$mensaje='ALERTA: Aun no ha sido activado el Mercado Virtual de su Empresa para la compra.';
		header("Location: ../index.php?mensaje=$mensaje");		
	}else{
		$usuario_NU_IdUsuario 	= $objConexion->obtenerElemento($RSUsuario,0,"NU_IdUsuario");
		$NU_IdMercado 			= $objConexion->obtenerElemento($verificarActivo,0,"NU_IdMercado");

		$RSPedido 		= $objPedido->verificarPedido($objConexion,$usuario_NU_IdUsuario,$NU_IdMercado);
		$cantRSPedido 	= $objConexion->cantidadRegistros($RSPedido);

		if ($cantRSPedido>0){
			$mensaje='ALERTA: Usted ya posee una orden de compra activa para el Mercado Virtual disponible.';
			header("Location: ../index.php?mensaje=$mensaje");		
		}
	}

	///////////// CONVERTIR DECIMALES A ESPANOL ///////////
	function setDecimalEsp($numero){
		$numero = str_replace(".", ",", $numero);
		return $numero;
	}
	///////////// CONVERTIR DECIMALES A AMERICANO ///////////
	function setDecimalAme($numero){
		$numero = str_replace(",", ".", $numero);
		return $numero;
	}
	/////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script>
		$(document).ready(function() {
	
		var default_message_for_dialog='<p align="center"><b>LISTA DE PRODUCTOS </b></p>';
		//var default_message_for_dialog = nombre;
	
		$("#dialog").dialog({
			show: "blind",
			hide: "explode",		
			modal: true,
			bgiframe: true,
			width: 320,
			height: 460,
			autoOpen: false,
			title: 'Confirme su Compra'
		});

		// FORMS
		$('input.confirm').click(function(theINPUT){
			theINPUT.preventDefault();
			var precio = 0;
			var cantidad = 0;
			var valor = 0;
			var indice = 0;
			var total = 0;
			var res2 = '';
			default_message_for_dialog += '<p align="left">';
			<?php 
				$rsMercadoProducto2		= $objMercadoProducto->listarMercadoProducto($objConexion,$NU_IdMercado);
				$cantMercadoProducto2	= $objConexion->cantidadRegistros($rsMercadoProducto2);
	
				for($a=0;$a<$cantMercadoProducto2;$a++){
			?>
			indice = document.form.NU_Cantidad<?php echo $a; ?>.selectedIndex;
			cantidad = document.form.NU_Cantidad<?php echo $a; ?>.options[indice].value;
		
			if (cantidad!=0){
				<?php			
					$AF_NombreProducto2	= $objConexion->obtenerElemento($rsMercadoProducto2,$a,"AF_NombreProducto");	
					$BS_PrecioUnitario2	= $objConexion->obtenerElemento($rsMercadoProducto2,$a,"BS_PrecioUnitario");
			
					//if ($AF_NombreProducto2=='Carne'){
						//	$BS_PrecioUnitario2 = $BS_PrecioUnitario2*2;
					//}
				?>

				precio = cantidad * parseFloat(<?php echo $BS_PrecioUnitario2; ?>);
				precio = ''+precio+'';
				res2 = precio.replace(".",",");			
				default_message_for_dialog += '- '+'(<b>'+cantidad+'</b>) <?php echo $AF_NombreProducto2; ?> = <b>'+res2+' Bsf.</b> </br>';
			
				total = parseFloat(total) + parseFloat(precio);
			}
		<?php	} ?>
		default_message_for_dialog += '</p>';
		
		var total = ''+total+'';
		var res = total.replace(".",",");

		default_message_for_dialog +='<b>TOTAL A PAGAR = '+res+' BsF.</b></br></br>';
		default_message_for_dialog +='<b style="color:#F00">Si confirma esta compra, estará aceptando que el monto total a pagar sea descontado de su próxima quincena.</b>';
				
		var theFORM = $(theINPUT.target).closest("form");
		var theREL = $(this).attr("rel");
		var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
		var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
		
		$('#dialog').html('<P align="left">' + theICON + theMESSAGE + '</P>');
		$("#dialog").dialog('option', 'buttons', {
                "Confirmar" : function() {
					theFORM.submit();
					$(this).dialog("close");
                    },
                "Cancelar" : function() {
					default_message_for_dialog = 'Lista de Productos: </br></br>';
                    $(this).dialog("close");
                    }
                });
		$("#dialog").dialog("open");
		});

		});
	</script>

</head>

<body onLoad="abrir_dialog();">
<div id="dialog"></div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Gestionar Mercado - Comprar</h4>
						

						<div class="panel panel-default">
							<div class="panel-heading">
								Comprar
							</div>						

							<div class="panel-body">
								<div class="row">							

								
									<div class="col-lg-12">

								
								<div class="BlancoGris" align="left">
									Listado de productos Disponibles para la Compra:
								</div>
								
											
								
								<div class="col-md-12 col-sm-12" style = "margin-left:20px">
									<b><u>Productos que Incluye el Combo Higiene:</u></b><br><br>
								</div>
								<i>
								<div class="col-md-4 col-sm-4" style = "margin-left:40px">
									<p>Papel Higiénico Super Blanco. 1 paq. x 4 rollos (Bs. 200.00)</p>
									<p>Toallas Sanitarias Always Prot. Plus 1 paq. x 36 Und. (Bs. 127.68)</p>						
								</div>	
								<div class="col-md-4 col-sm-4" style = "margin-left:40px">
									<p>Toallas Sanitarias Intimas c/alas 1 paq. x 16 Und. (Bs. 50.00)</p>
									<p>Detergente en Polvo Limón 1 Kg. (Bs. 2349.76)</p>
						
								</div>	

								
								</i>
								<div class="col-md-12 col-sm-12"><br><hr><br></div>
								
								
									
								
								<form name="form" id="form" method="post" action="../../controller/pedidoController.php" target="_blank">									
								
									<?php 
									$k=0;
									$rsMercadoProducto		= $objMercadoProducto->listarMercadoProducto($objConexion,$NU_IdMercado);
									$cantMercadoProducto	= $objConexion->cantidadRegistros($rsMercadoProducto);
					
									for($i=0;$i<$cantMercadoProducto;$i++){
										$NU_IdProducto		= $objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_IdProducto");						
										$AF_NombreProducto	= $objConexion->obtenerElemento($rsMercadoProducto,$i,"AF_NombreProducto");
										$AL_Medida			= $objConexion->obtenerElemento($rsMercadoProducto,$i,"AL_Medida");
										$NU_Contenido		= setDecimalEsp($objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_Contenido"));
										$BS_PrecioUnitario	= setDecimalEsp($objConexion->obtenerElemento($rsMercadoProducto,$i,"BS_PrecioUnitario"));
										$NU_Max				= $objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_Max");
										$NU_Min				= $objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_Min");						  
										$NU_Salto			= $objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_Salto");						  
										$AF_Foto			= $objConexion->obtenerElemento($rsMercadoProducto,$i,"AF_Foto");
						  
										if ($k==0){ echo "<div class='col-lg-3'>"; }
										?>
										<img src="../../images/producto/<?php if ($AF_Foto==''){ echo 'sin_imagen.jpg'; }else{ echo $AF_Foto; } ?>" width="124" height="85"  alt="" style="border:solid 1px #CFCFCF"/>
										<br><br>
										<strong><?php echo $AF_NombreProducto; ?></strong></b><br>
										<?php 
										if($NU_IdProducto=='7'){ 
											echo 'Contenido: 1 Emp.'.'<br>';
											echo 'Precio: 133,40 BsF'.'<br>';
										?>
											<select class="form-control" name="<?php echo 'NU_Cantidad'.$i; ?>" id="<?php echo 'NU_Cantidad'.$i; ?>" style="width:60px">
												<!-- <option selected="selected" value="0">0</option> -->
												<?php for ($j=1; $j<=2; $j=$j+1){ ?>
												<option value="<?php echo $j; ?>"><?php echo $j; ?></option>
												<?php } ?>
											</select>
											<hr>									
											<?php
										}else{
											echo 'Contenido: '.$NU_Contenido.' '.$AL_Medida.'<br>';
											echo 'Precio: '.$BS_PrecioUnitario.' BsF'.'<br>';
											?>
											<select class="form-control" name="<?php echo 'NU_Cantidad'.$i; ?>" id="<?php echo 'NU_Cantidad'.$i; ?>" style="width:80px">
												<!-- <option selected="selected" value="0">0</option> -->
												<?php for ($j=$NU_Min; $j<=$NU_Max; $j=$j+$NU_Salto){ ?>
												<option value="<?php echo $j; ?>"><?php echo $j; ?></option>
												<?php } ?>
											</select>
											<hr>
											<?php						
										} ?>

										<input name="<?php echo 'NU_IdProducto'.$i; ?>" type="hidden" id="<?php echo 'NU_IdProducto'.$i; ?>" value="<?php echo $NU_IdProducto; ?>">
										<input type="hidden" name="<?php echo 'BS_PrecioUnitario'.$i; ?>" id="<?php echo 'BS_PrecioUnitario'.$i; ?>" value="<?php echo setDecimalAme($BS_PrecioUnitario); ?>">
										<input name="<?php echo 'NU_Max'.$i; ?>" type="hidden" id="<?php echo 'NU_Max'.$i; ?>" value="<?php echo $NU_Max; ?>">
										<?php $k++; ?>
										<?php 
										if ($k!=0){ echo '</div>'; $k=0; }
									}  ?>  


							
									<div class="col-sm-12 col-lg-12 col-md-12" style="padding:0px">
										
										<div class="BlancoGris" align="left">
											Si usted desea autorizar a otra persona con el fin de retirar su compra, llene el siguiente formulario:
										</div>
										<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
										<div class="form-group" align="left">
											<div class="col-sm-5 col-lg-5 col-md-5">
												<label for="autorizado">Nombre de quién retira:</label><br>
												<input type="text" name="AL_AutorizoNombre" id="AL_AutorizoNombre" class="form-control">
											</div>
											
											<div class="col-sm-5 col-lg-5 col-md-5">
												<label for="cedula_autorizado">N° Ced. de quién retira:</label><br>
												<input type="number" name="AL_AutorizoCedula" id="AL_AutorizoCedula" class="form-control">
												
											</div>	
										</div>
										<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
										
										<div class="col-sm-12 col-lg-12 col-md-12">
											<hr>
											<div class="col-sm-9 col-lg-9 col-md-9" align="right" style="padding:0px">											
												<input name="Comprar" type="submit" class="confirm btn btn-primary"  value="Confirmar Comprar" id="Comprar">
											</div>
											<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
											<div class="col-sm-1 col-lg-1 col-md-1" align="right" style="padding:0px">
												<input name="button2" type="button" id="button2" class="btn btn-primary" value="Cancelar" onClick="javascript:window.location='../index.php'" />
												<br><br>
											</div>
											<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
											<input name="origen" type="hidden" id="origen" value="pedido">
											<input name="cantProducto" type="hidden" id="cantProducto" value="<?php echo $cantMercadoProducto; ?>">
											<input name="NU_IdMercado" type="hidden" id="NU_IdMercado" value="<?php echo $NU_IdMercado; ?>">
										</div>
									</div>
								</form>
									</div>
									
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>	
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	
	<!-- Custom JavaScript -->
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../../js/funciones.js"></script>

</body>

</html>
