﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/pedidoModel.php');
	require_once('../../model/usuarioModel.php');

	$objPedido 			= new Pedido();
	$objUsuario			= new Usuario();

	$mercado_NU_IdMercado = $_REQUEST['mercado_NU_IdMercado'];
	$RSPedido		= $objPedido->listarPedidosXmercado($objConexion,$mercado_NU_IdMercado);
	$cantRSPedido	= $objConexion->cantidadRegistros($RSPedido);

	///////////// CONVERTIR DECIMALES A ESPANOL ///////////
	function setDecimalEsp($numero){
		$numero = str_replace(".", ",", $numero);
		return $numero;
	}
	


	$usuarios = array(); //creamos un array
	for($i=0; $i<$cantRSPedido; $i++){
				
		$NU_Cedula			= $objConexion->obtenerElemento($RSPedido,$i,'NU_Cedula');			
		$MontoPagar 		= $objConexion->obtenerElemento($RSPedido,$i,'MontoPagar');
			
		$usuarios[] = array('cedula'=> $NU_Cedula, 'monto'=> $MontoPagar);
	}

	$json_string = json_encode($usuarios);
		
	$file ='descuentos/DGRH-GBS-M0'.$mercado_NU_IdMercado.'.json';
	file_put_contents($file, $json_string);
	
	
	
	header('Location: index.php?mensaje=El TXT para el Descuento del Mercado Virtual se ha generado con éxito !!');
															
	?>

