﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/mercadoModel.php');

	$objMercado = new Mercado();

	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_FechaNac) {
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	
	///////////// CONVERTIR DECIMALES A ESPANOL ///////////
	function setDecimalEsp($numero) {
		$numero = str_replace(".", ",", $numero);
		return $numero;
	}
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Generar TXT - Mercado Virtual</h3>
					</div>
					<div class="col-lg-5">
						<form role="form" id="form" method="POST" action="jsonphp.php">	
							<div class="panel panel-default">
								<div class="panel-heading">
									Configuración &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Generar TXT - Mercado Virtual.
								</div>
								<div class="panel-body">
									<div class="row">
										
										<div class="col-lg-12">	
											<br>
											<div class="form-group" align="left">
												<div class="col-lg-12">
													<label for="mercado">Nro. Mercado:</label><br>
													<select name="mercado_NU_IdMercado" required id="mercado_NU_IdMercado" class="form-control placeholder">
														<option value="">[ Seleccione ]</option>
														<?php 
														$RSMercado = $objMercado->listarMercado($objConexion);
														$cantRSMercado 	= $objConexion->cantidadRegistros($RSMercado);
														for($i=0;$i<$cantRSMercado;$i++) {
															$value=$objConexion->obtenerElemento($RSMercado,$i,"NU_IdMercado");
															$des = 'DGRH-GBS-M0';
															$des.=$objConexion->obtenerElemento($RSMercado,$i,"NU_IdMercado");
															$des.=" de: ".$objConexion->obtenerElemento($RSMercado,$i,"AF_RazonSocial");
															$selected="";
															echo "<option value=".$value." ".$selected.">".$des."</option>";
														}
														?>
													</select>
													<br><br>
													<div align="right">
														<button type="submit" name="submit" id="submit" class="btn btn-primary">Generar TXT</button>
													</div>
													<br>
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					
					<div class="col-lg-7">
						<div class="panel panel-default">
							<div class="panel-heading">
								Configuración &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Listado de TXT Generados - Mercado Virtual.
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="dataTable_wrapper" align="center" style="margin:50px">
						
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr align="center">
													<td style="vertical-align:middle;" ><strong>Nombre del Archivo TXT Generado</strong></td>
													<td style="vertical-align:middle; width:20%" ><strong>Descargar</strong></td>
												</tr>
											</thead>

											<tbody align="center">
												<?php
													$directorio = opendir("descuentos"); //ruta actual
													while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
													{
														if (is_dir($archivo))//verificamos si es o no un directorio
														{
															//echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
														}
														else
														{
															echo '<tr>';
															echo '<td style="vertical-align:middle;" >'.$archivo . "</td>";
															echo '<td style="vertical-align:middle;" ><a href="download.php?f='.$archivo.'" title="Descargar Archivo"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span></a></td>';
															echo '</tr>';
														}
													}
												?>
											</tbody>
										</table>		
									</div>
								
								</div>
							</div>
						</div>
						
					</div>
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
