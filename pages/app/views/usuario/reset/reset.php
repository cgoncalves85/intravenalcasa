﻿<?php
	require_once('../../../controller/sessionController.php'); 

	$NU_IdUsuario = $_GET['NU_IdUsuario'];
	$rsUsuario = $objUsuario->Resetear($objConexion,$NU_IdUsuario);
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Resetear Usuario</h4>

						
						<div class="panel panel-default">
							<div class="panel-heading">
								Información !!
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-10">
										<div class="col-lg-12">
											<br>
											<p>El usuario ha sido reseteado. <br><br>Debe registrarse de nuevo colocando una nueva Clave y un Correo Electronico Valido.</p>
										</div>
										<div class="col-lg-12">&nbsp;</div>
										<div class="col-lg-12" align="right">
											<input name="button2" type="button" class="btn btn-primary" id="button2" value="Volver al Menú Anterior" onClick="javascript:history.back();" />
											<br><br>
										</div>
										
									</div>
									<div class="col-lg-1"></div>

								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>

</body>

</html>
