<?php 
	require_once('../../../controller/sessionController.php'); 
	$NU_IdUsuario = $_GET['NU_IdUsuario'];
	$rsUsuario = $objUsuario->Resetear($objConexion,$NU_IdUsuario);
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="app/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="app/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="app/dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="app/css/jquery-ui.css" rel="stylesheet" />
	<link rel="shortcut icon" href="app/images/favicon.ico"/>

    <!-- Custom Fonts -->
    <link href="app/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
			$( "#dialog" ).dialog({
				show: "blind",
				hide: "explode",
				modal: true,
				buttons: {
					Aceptar: function() {
					$( this ).dialog( "close" );
					}
				}
			});
			}
		};
	</script>	

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <span class="ui-icon ui-icon-circle-check"></span>
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>

<div class="container">
<div class="row">
	<div class="col-lg-3 col-lg-offset-4">
	<br><br><br>
	<div class="panel panel-default">
    
		<div class="panel-body">
			<div class="row">
					
				<div align="center"><img class="profile-img" src="app/images/intrave.png" class="img-responsive" alt=""> </div>
					
				<hr>
			</div>
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<p>El usuario debe registrarse de nuevo colocando una nueva Clave y un Correo Electronico Valido.</p>
				</div>
			</div>
		</div>
    </div>
	</div>
</div>

    <!-- jQuery -->
    <script src="app/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="app/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="app/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="app/dist/js/sb-admin-2.js"></script>
	
	<script type="text/javascript" src="app/js/jquery-ui.js"></script>
	<script type="text/javascript" src="app/js/maximizar.js"></script>
	<script type="text/javascript" src="app/js/desconectar.js"></script>

</body>

</html>
