<?php 
require_once("../../../includes/constantes.php");
require_once("../../../includes/conexion.class.php");
require_once('../../../model/empresaModel.php');

$objConexion= new conexion(SERVER,USER,PASS,DB);
$objEmpresa = new Empresa();
?>
<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/shop-homepage.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/jquery-ui.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript" src="../js/jquery.ui.datepicker-es.js"></script>

</head>

<body>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../img/head.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
	</div>
	<br>
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../img/head3.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
	</div>

	<br>
    <!-- Page Content -->
    <div class="container">

        <div class="row">
			<div class="col-sm-1 col-lg-1 col-md-1"></div>		
			<div class="col-sm-10 col-lg-10 col-md-10">
				<div class="panel panel-primary">
					<div class="panel-body" style="padding:30px">
						<div class="col-sm-1 col-lg-1 col-md-1"></div>		
						<div class="col-sm-10 col-lg-10 col-md-10">
							<ul class="nav nav-tabs"><li class="active"><a href="#"><strong>ENVIAR SUGERENCIA, DUDA O PROBLEMA</strong></a></li></ul>
							<br><br>
							<p>Para enviar su mensaje conteste el siguiente formulario.</p>
							<br>
							<div class="col-sm-1 col-lg-1 col-md-1"></div>
							<div class="col-sm-5 col-lg-5 col-md-5">
								<form role="form" method="POST" action="../../../controller/usuarioController.php" >
									<div class="form-group">
										<label for="NU_Cedula">Número de Cédula :</label>
										<input type="text" name="NU_Cedula" required id="NU_Cedula" size="32" class="form-control input-sm" placeholder="Introduce tu Cédula">
									</div>
									<div class="form-group">
										<label for="AL_Nombre">Nombre :</label>
										<input type="text" name="AL_Nombre" required id="AL_Nombre" size="32" class="form-control input-sm" placeholder="Introduce tu Nombre">
									</div>
									<div class="form-group">
										<label for="AL_Apellido">Apellido :</label>
										<input type="text" name="AL_Apellido" required id="AL_Apellido" size="32" class="form-control input-sm" placeholder="Introduce tu Apellido">
									</div>							
									<div class="form-group">
										<label for="AF_Telefono">Teléfono :</label>
										<input type="text" name="AF_Telefono" required id="AF_Telefono" size="32" class="form-control input-sm" placeholder="Introduce tu Teléfono">
									</div>
							</div>
							<div class="col-sm-5 col-lg-5 col-md-5">
								<div class="form-group">
									<label for="AF_Correo">Correo Electrónico :</label>
									<input type="email" name="AF_Correo" required id="AF_Correo" size="32" class="form-control input-sm" placeholder="Introduce tu Correo">
								</div>
								<div class="form-group">
									<label for="empresa_NU_IdEmpresa">Empresa :</label>
									<select name="empresa_NU_IdEmpresa" required id="empresa_NU_IdEmpresa" class="form-control input-sm">
										<option selected="selected">[ Seleccione ]</option>
										<?php 
											$rsEmpresa=$objEmpresa->listarEmpresa($objConexion);
											for($i=0;$i<$objConexion->cantidadRegistros($rsEmpresa);$i++){
											$value=$objConexion->obtenerElemento($rsEmpresa,$i,"NU_IdEmpresa");
											$des=$objConexion->obtenerElemento($rsEmpresa,$i,"AF_RazonSocial");
											$selected="";
											echo "<option value=".$value." ".$selected.">".$des."</option>";
											}  
										?>
									</select>
								</div>
								<div class="form-group">
									<label for="AF_Ubicacion">Ubicación :</label>
									<input type="text" name="AF_Ubicacion" required id="AF_Ubicacion" size="32" class="form-control input-sm" placeholder="Sede / Gerencia">
								</div>
								<input name="origen" type="hidden" id="origen" value="Novedad">
								<div class="form-group">
									<label for="AF_Novedad">Sugerencia o Novedad :</label>
									<textarea name="AF_Novedad" id="AF_Novedad" class="form-control input-sm" rows="3"></textarea>
								</div>
								<br>
								<div class="col-sm-12 col-lg-12 col-md-12"></div>
								<div class="col-sm-5 col-lg-5 col-md-5">
									<button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm">Enviar Mensaje</button>
								</div>
								<div class="col-sm-2 col-lg-2 col-md-2">&nbsp;</div>
								<div class="col-sm-5 col-lg-5 col-md-5">
									<button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm" value="Cancelar" onClick="javascript:window.location='../../../../index.php'">Cancelar</button>
								</div>
								
							</div>
							</form>	
						</div>
						<div class="col-sm-1 col-lg-1 col-md-1"></div>	
					</div>
				</div>
			</div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>	
		</div>
	</div>

	
    <div class="container">

        <div class="row">
		<footer> 
            <div class="col-sm-1 col-lg-1 col-md-1"></div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-primary">
					<div class="panel-body">
						<p align="center"><strong>
						Venezolana de Alimentos La Casa - &copy;2015<br>
						Oficina de Tecnología de la Información
						</strong></p>
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1"></div>			
        </footer>
		</div>
	</div>


    <!-- jQuery -->
    <script src="..js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="..js/bootstrap.min.js"></script>

</body>

</html>
