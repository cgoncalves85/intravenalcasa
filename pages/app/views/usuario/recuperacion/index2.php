<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/shop-homepage.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/jquery-ui.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript">
    function abrir_dialog() {
		var mensaje = "<?php echo $_GET['mensaje']; ?>";
		if(mensaje){
		  $( "#dialog" ).dialog({
			  show: "blind",
			  hide: "explode",
			  modal: true,
			  buttons: {
				Aceptar: function() {
				  $( this ).dialog( "close" );
				}
			  }
		  });
		}
    };
</script>	
	
</head>

<body onLoad="abrir_dialog();">
<div id="dialog" title="Mensaje" style="display:none;">
    <span class="ui-icon ui-icon-circle-check"></span>
    <p><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1"></div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../img/head.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1"></div>			
			
		</div>
	</div>
	<br>
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../img/head3.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
	</div>

	<br>
    <!-- Page Content -->
    <div class="container">

        <div class="row">
			<div class="col-sm-1 col-lg-1 col-md-1"></div>		
			<div class="col-sm-10 col-lg-10 col-md-10">
				<div class="panel panel-primary">
					<div class="panel-body" style="padding:30px">
					<div class="col-sm-1 col-lg-1 col-md-1"></div>
						<div class="col-sm-10 col-lg-10 col-md-10">
						<br><br>
						<ul class="nav nav-tabs"><li class="active"><a href="#"><strong>RECUPERACION DE CLAVE</strong></a></li></ul>
						<br><br>
							<p>Para recuperar su clave conteste el siguiente formulario.</p>
						<br><br>
						<div class="col-sm-3 col-lg-3 col-md-3"></div>
						<div class="col-sm-6 col-lg-6 col-md-6">
						<form role="form" method="POST" action="../../../controller/usuarioController.php" >
							<div class="form-group">
								<label for="NU_Cedula">Número de Cédula :</label>
								<input type="text" name="NU_Cedula" required id="NU_Cedula" size="15" class="form-control input-sm" placeholder="Introduce tu Cédula">
							</div><br>
							<div class="form-group">
								<label for="NU_Cedula">Fecha de Nacimiento :</label>
								<input type="text" name="FE_FechaNac1" required id="FE_FechaNac1" size="15" class="form-control input-sm" placeholder="Ej. dd/mm/aaaa">
							</div><br>	

							<div class="col-sm-12 col-lg-12 col-md-12">
							<div class="col-sm-4 col-lg-4 col-md-4">
								<button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm">Registrar</button>
							</div> 
							<div class="col-sm-2 col-lg-2 col-md-2">&nbsp;</div>
							<div class="col-sm-4 col-lg-4 col-md-4">
								<button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm" onClick="javascript:window.location='../../../../index.php'">Cancelar</button>
							</div>
							</div>
							<br>
							<input name="empresa_NU_IdEmpresa" type="hidden" id="empresa_NU_IdEmpresa" value="3">
							<input name="origen" type="hidden" id="origen" value="UserRecuperacion">
						</form>
						<div>
						<div class="col-sm-3 col-lg-3 col-md-3"></div>	
						</div>
						<div class="col-sm-1 col-lg-1 col-md-1"></div>
					</div>
				</div>
			</div>

			<div class="col-sm-1 col-lg-1 col-md-1"></div>

        </div>
    </div></div>
    <!-- /.container -->

        <!-- Footer -->
       <footer>
        <div class="row">
		    <div class="col-sm-12 col-lg-12 col-md-12"></div>
            <div class="col-sm-1 col-lg-1 col-md-1"></div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-primary">
					<div class="panel-body">
						<p align="center"><strong>
						Venezolana de Alimentos La Casa - &copy;2015<br>
						Oficina de Tecnología de la Información
						</strong></p>
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1"></div>			
			
		</div>
        </footer>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
