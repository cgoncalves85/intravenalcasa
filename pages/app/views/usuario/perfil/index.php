﻿<?php
require_once('../../../controller/sessionController.php'); 
require_once('../../../model/usuarioModel.php');

$objUsuario = new Usuario();

$RS = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
if ($objConexion->cantidadRegistros($RS)>0){

	$AL_Nombre 			= $objConexion->obtenerElemento($RS,0,'AL_Nombre');
	$AL_NombreSede 		= $objConexion->obtenerElemento($RS,0,"AL_NombreSede");
	$AL_NombreGerencia	= $objConexion->obtenerElemento($RS,0,"AL_NombreGerencia");	
	$cargo_NU_IdCargo	= $objConexion->obtenerElemento($RS,0,"cargo_NU_IdCargo");	
	$AF_Correo			= $objConexion->obtenerElemento($RS,0,"AF_Correo");	
	$FE_Ingreso			= $objConexion->obtenerElemento($RS,0,"FE_Ingreso");
	$AL_Foto			= $objConexion->obtenerElemento($RS,0,'AL_Foto');	
}

	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_Ingreso)
	{
		$partes = explode("-", $FE_Ingreso);
		$FE_Ingreso = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_Ingreso;
	}

	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
	}

?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Perfil de Usuario</h4>

						
						<div class="panel panel-default">
							<div class="panel-heading">
								Datos del Usuario
							</div>
							<div class="panel-body">
								<div class="row">
									<form role="form" method="POST" action="../../index.php">
										<div class="col-lg-1"></div>
										<div class="col-lg-2" align="center">
											<br>
											<img src="../../<?php echo $AL_Foto; ?>" class="img-thumbnail" />
											<br><br>
											<label>C.I: <?php echo $NU_Cedula; ?></label>
										</div>											
										<div class="col-lg-4">
											<br>
											<div class="form-group ">
												<label>Nombre y Apellidos :</label>
												<input class="form-control" style="text-transform:capitalize" type="text" name="AL_Nombre" value="<?php echo $AL_Nombre; ?>" size="40" readonly>
											</div>										
											<div class="form-group">
												<label>Correo Electrónico :</label>
												<input class="form-control" style="text-transform:lowercase" type="text" name="AF_Correo" value="<?php echo $AF_Correo; ?>" size="40" readonly>
											</div>
											<div class="form-group">
												<label>Fecha de Ingreso :</label>
												<input class="form-control" type="text" name="FE_Ingreso" value="<?php echo setFechaNoSQL($FE_Ingreso); ?>" size="40" readonly>
											</div>											
										</div>
								
										<div class="col-lg-4">
											<br>
											<div class="form-group">
												<label>Ubicación :</label>
												<input class="form-control" style="text-transform:capitalize" type="text" name="sede_NU_IdSede" value="<?php echo $AL_NombreSede; ?>" size="40" readonly>
											</div>											
											<div class="form-group">
												<label>Gerencia :</label>
												<input class="form-control" style="text-transform:capitalize" type="text" name="AL_NombreGerencia" value="<?php echo $AL_NombreGerencia; ?>" size="40" readonly>
											</div>
											<div class="form-group">
												<label>Cargo :</label>
												<input class="form-control" style="text-transform:capitalize" type="text" name="cargo_NU_IdCargo" value="<?php echo $cargo_NU_IdCargo; ?>" size="40" readonly>
											</div>												
										</div>									
										
										<div class="col-lg-12" align="right">
											<hr>
											<div class="col-lg-8"></div>
											<div class="col-lg-2" align="right">											
												<a href="index_clave.php" class="btn btn-primary">Cambiar Clave</a>
											</div>
											<div class="col-lg-2" align="left">
												<button type="submit" name="submit" id="submit" class="btn btn-primary">Cancelar</button>
											</div>
										</div>
										<div class="col-lg-12">&nbsp;</div>
									</form>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>

</body>

</html>
