﻿<?php 
require_once('../../../controller/sessionController.php'); 
require_once("../../../model/usuarioModel.php");

$objUsuario = new Usuario();

$RS = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
if ($objConexion->cantidadRegistros($RS)>0){

	$NU_Cedula 		= $objConexion->obtenerElemento($RS,0,'NU_Cedula');
	$AF_Correo 		= $objConexion->obtenerElemento($RS,0,'AF_Correo');
	$AL_Foto 		= $objConexion->obtenerElemento($RS,0,'AL_Foto');
}

	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
	}

?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Perfil de Usuario</h4>

						
						<div class="panel panel-default">
							<div class="panel-heading">
								Datos del Usuario &nbsp; ǀ &nbsp;Cambiar Clave
							</div>
							<div class="panel-body">
								<div class="row">
									<form role="form" method="POST" action="../../../controller/usuarioController.php" >
										<div class="col-lg-1"></div>
										<div class="col-lg-6">
											<br>
											<div class="form-group">
												<label>Nueva Contraseña :</label>
												<input type="password" name="AF_Clave" required id="AF_Clave" size="32" class="form-control" placeholder="Introduzca su Clave">
											</div>										
											<div class="form-group">
												<label>Correo Electrónico :</label>
												<input type="email" name="AF_Correo" value="<?php echo $AF_Correo; ?>" required id="AF_Correo" size="32" class="form-control">
												<p class="help-block">&nbsp;&nbsp;&nbsp;Su nueva contraseña será enviada a esta dirección !!</p>
											</div>											
										</div>	
										<div class="col-lg-1"></div>
										<div class="col-lg-2" align="center">
											<br>
											<img src="../../<?php echo $AL_Foto; ?>" class="img-thumbnail" />
										</div>
										<div class="col-lg-12" align="right">
											<hr>
											<div class="col-lg-8"></div>
											<div class="col-lg-2" align="right">
												<button type="submit" name="submit" id="submit" class="btn btn-primary">Guardar Cambios</button>
											</div>
											<div class="col-lg-2" align="left">
												<button type="submit" name="submit" id="submit" class="btn btn-primary" value="Cancelar" onClick="javascript:window.location='../../index.php'">Cancelar</button>
											</div>
										</div>
										<div class="col-lg-12">&nbsp;</div>
										<input name="origen" type="hidden" id="origen" value="Perfil">
									</form>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>

</body>

</html>
