<?php 
	session_start();

	require_once("../../../includes/constantes.php");
	require_once("../../../includes/conexion.class.php");
	require_once('../../../model/empresaModel.php'); 
	
	$objConexion= new conexion(SERVER,USER,PASS,DB);
	$objEmpresa = new Empresa();

	$dia			= substr($_GET['FE_FechaNac'],-2);		 
	$mes			= substr($_GET['FE_FechaNac'],4,-2);		 
	$anio			= substr($_GET['FE_FechaNac'],0,-4);		 		
	$FE_FechaNac 	= $dia.'/'.$mes.'/'.$anio;
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../../css/jquery-ui.css" rel="stylesheet" />
	<link rel="shortcut icon" href="../../../images/favicon.ico"/>

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
			$( "#dialog" ).dialog({
				show: "blind",
				hide: "explode",
				modal: true,
				buttons: {
					Aceptar: function() {
					$( this ).dialog( "close" );
					}
				}
			});
			}
		};
	</script>	

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>

<div class="container" style="margin-top:100px">
<div class="row">
	<div class="col-lg-8 col-lg-offset-2">
	
	<div class="panel panel-default">
    
		<div class="panel-body">
			<form role="form" method="POST" action="../../../controller/usuarioController.php" >
			<fieldset>
				<div class="row">
					
					<div align="center"><img class="profile-img" src="../../../images/intrave.png" class="img-responsive" alt=""> </div>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<form role="form" method="POST" action="../../../controller/usuarioController.php">
							<div class="col-lg-6">
								<div class="form-group">
									<label for="NU_Cedula">Número de Cédula :</label>
									<input type="text" name="NU_Cedula" value="<?php echo $_SESSION["NU_Cedula"]; ?>" size="40" readonly class="form-control">
								</div>
								<div class="form-group">
									<label for="AL_Nombre">Nombre :</label>
									<input type="text" name="AL_Nombre" value="<?php echo $_GET['AL_Nombre']; ?>" size="40" readonly class="form-control">
								</div>
								<div class="form-group">
									<label for="FE_FechaNac">Fecha de Nacimiento :</label>
									<input type="text" name="FE_FechaNac" value="<?php echo $FE_FechaNac; ?>" size="40" readonly class="form-control">
								</div>
								<div class="form-group">
									<label for="empresa_NU_IdEmpresa">Empresa :</label>
									<input type="text" name="empresa_NU_IdEmpresa" id="empresa_NU_IdEmpresa" value="<?php if(isset($_GET['AF_RazonSocial'])){ echo $_GET['AF_RazonSocial']; } ?>" size="40" readonly class="form-control">
								</div>							
								<div class="form-group">
									<label for="sede_NU_IdSede">Sede :</label>
									<input type="text" name="sede_NU_IdSede" id="sede_NU_IdSede" value="<?php if(isset($_GET['AL_NombreSede'])){ echo $_GET['AL_NombreSede']; } ?>" size="40" readonly class="form-control">
								</div>	
								<div class="form-group">
									<label for="gerencia_NU_IdGerencia">Gerencia :</label>
									<input type="text" name="gerencia_NU_IdGerencia" id="gerencia_NU_IdGerencia" value="<?php if(isset($_GET['AL_NombreGerencia'])){ echo $_GET['AL_NombreGerencia']; } ?>" size="40" readonly class="form-control">
								</div>	
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<label for="AF_Telefono">Teléfono :</label>
									<input type="text" name="AF_Telefono" autofocus="autofocus" required id="AF_Telefono" autocomplete="off" size="40" class="form-control">
								</div>	
								<div class="form-group">
									<label for="AF_Correo">Correo Electrónico :</label>
									<input type="email" name="AF_Correo" autofocus="autofocus" required id="AF_Correo" autocomplete="off" size="40" class="form-control">
								</div>								
								<div class="form-group">
									<label for="AF_Clave">Clave :</label>
									<input type="password" name="AF_Clave" autofocus="autofocus" required id="AF_Clave" autocomplete="off" size="40" class="form-control">
								</div>
								<div class="form-group">
									<label for="AF_Clave2">Repita Clave :</label>
									<input type="password" name="AF_Clave2" autofocus="autofocus" required id="AF_Clave2" autocomplete="off" size="40" class="form-control">
								</div>
								<br><br>
								<div class="form-group">
									<div class="col-lg-6" align="left">
										<a href="../recuperacion/index.php"  />Recuperar Clave ?<br><br>
										<a href="../../../../login.php"  />Iniciar Sesión<br>
									</div>
									<div align= "right">
										<input name="origen" type="hidden" id="origen" value="UserCrear2">
										<button type="submit" name="submit" id="submit" class="btn btn-default">Registrar Usuario</button>
									</div>													
								</div>
							</div>
						</form>
					</div>
				</div>
			</fieldset>
			</form>
		</div>
    </div>
	</div>
</div>

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>
	
	<script type="text/javascript" src="../../../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../../../js/jquery.validate.js"></script>

</body>

</html>
