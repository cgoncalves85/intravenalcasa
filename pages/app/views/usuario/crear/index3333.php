<!DOCTYPE html>
<html lang="es">
<head>

  <meta charset="utf-10">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Christian Goncalves">

  <title>IntraVenalcasa</title>

  <!-- Bootstrap Core CSS -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="../css/shop-homepage.css" rel="stylesheet">
  <link rel="stylesheet" href="../css/jquery-ui.css" />

  <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.1/respond.min.js"></script>
  <![endif]-->

  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery-ui.js"></script>
  <script type="text/javascript" src="../js/funciones.js"></script>
  <script type="text/javascript" src="../js/jquery.validate.js"></script>

</head>

<body>
<div id="dialog" title="Atención !!" style="display:none;">
    <p>En Construccion!!.</p>
</div>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1"></div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../img/head.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1"></div>			
			
		</div>
	</div>
	<br>
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../img/head3.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
	</div>

	<br>
    <!-- Page Content -->
    <div class="container">

      <div class="row">
			<div class="col-sm-12 col-lg-12 col-md-12"></div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
			<div class="col-sm-10 col-lg-10 col-md-10">
	       <div class="panel panel-primary" >
          
          <div class="panel-body">
            <div class="col-sm-1 col-lg-1 col-md-1"></div>
            <div class="col-sm-10 col-lg-10 col-md-10">
              <br><br>
              <ul class="nav nav-tabs"><li class="active"><a href="#"><strong>CREACIÓN DE USUARIOS</strong></a></li></ul>
              <br><br>
              <p>Coloque los datos para validar su usuario.</p>
              <br>
              <div class="col-sm-4 col-lg-4 col-md-4"></div>
              <div class="col-sm-4 col-lg-4 col-md-4">
                <form role="form" method="POST" action="../../../controller/usuarioController.php">
                  <div class="form-group">
                    <label for="NU_Cedula">Número de Cédula :</label>
                    <input type="text" name="NU_Cedula" required id="NU_Cedula" size="15" class="form-control input-sm" placeholder="Introduce tu Cédula">
                  </div>
                  <input name="origen" type="hidden" id="origen" value="UserCrear1">
                  <br>
                  <div class="col-sm-12 col-lg-12 col-md-12">
                  <div class="col-sm-4 col-lg-4 col-md-4">
                    <button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm">Registrar</button>
                  </div> 
                  <div class="col-sm-2 col-lg-2 col-md-2">&nbsp;</div>
                  <div class="col-sm-4 col-lg-4 col-md-4">
                    <button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm" onClick="javascript:window.location='../../../../index.php'">Cancelar</button>
                  </div>
                  </div>
                  <br><br><br>
				</form>
            </div>

            <div class="col-sm-4 col-lg-4 col-md-4"></div>
            <div>
            <div class="col-sm-1 col-lg-1 col-md-1"></div>  
          </div>
        </div>
			
			</div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
		</div>

	</div></div>
    <!-- /.container -->

        <!-- Footer -->
	
       <footer>
        <div class="row">


      <div class="col-sm-12 col-lg-12 col-md-12"></div>
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-primary">
					<div class="panel-body">
						<p align="center"><strong>
						Venezolana de Alimentos La Casa - &copy;2015<br>
						Oficina de Tecnología de la Información
						</strong></p>
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
        </footer>


    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>

</body>

</html>
