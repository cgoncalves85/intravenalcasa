<?php 
	session_start();

	require_once("../../../includes/constantes.php");
	require_once("../../../includes/conexion.class.php");
	require_once('../../../model/empresaModel.php'); 
	
	$objConexion= new conexion(SERVER,USER,PASS,DB);
	$objEmpresa = new Empresa();

	$dia			= substr($_GET['FE_FechaNac'],-2);		 
	$mes			= substr($_GET['FE_FechaNac'],4,-2);		 
	$anio			= substr($_GET['FE_FechaNac'],0,-4);		 		
	$FE_FechaNac 	= $dia.'/'.$mes.'/'.$anio;
?>

<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/shop-homepage.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/jquery-ui.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery-ui.js"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>


	
</head>

<body>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1"></div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../img/head.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1"></div>			
			
		</div>
	</div>
	<br>
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../img/head3.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
	</div>

	<br>
    <!-- Page Content -->
    <div class="container">

        <div class="row">
			<div class="col-sm-1 col-lg-1 col-md-1">
			
			</div>		
		
			<div class="col-sm-10 col-lg-10 col-md-10">
				<div class="panel panel-primary">

					<div class="panel-body" style="padding:30px">
					<div class="col-sm-1 col-lg-1 col-md-1"></div>
						<div class="col-sm-10 col-lg-10 col-md-10">
						<ul class="nav nav-tabs"><li class="active"><a href="#"><strong>CREACIÓN DE USUARIOS</strong></a></li></ul>
						<br><br>
							<p>Llene el formulario para crear su usuario.</p>
							<br>
						</div>
					<div class="col-sm-1 col-lg-1 col-md-1"></div>
						<div class="col-sm-12 col-lg-12 col-md-12"></div>
						<div class="col-sm-1 col-lg-1 col-md-1"></div>	
						<div class="col-sm-5 col-lg-5 col-md-5">
						<form role="form" method="POST" action="../../../controller/usuarioController.php">
							<div class="form-group">
								<label for="NU_Cedula">Número de Cédula :</label>
								<input type="text" name="NU_Cedula" value="<?php echo $_SESSION["NU_Cedula"]; ?>" size="40" readonly class="form-control input-sm">
							</div>
							<div class="form-group">
								<label for="AL_Nombre">Nombre :</label>
								<input type="text" name="AL_Nombre" value="<?php echo $_GET['AL_Nombre']; ?>" size="40" readonly class="form-control input-sm">
							</div>
							<div class="form-group">
								<label for="FE_FechaNac">Fecha de Nacimiento :</label>
								<input type="text" name="FE_FechaNac" value="<?php echo $FE_FechaNac; ?>" size="40" readonly class="form-control input-sm">
							</div>
							<div class="form-group">
								<label for="empresa_NU_IdEmpresa">Empresa :</label>
								<input type="text" name="empresa_NU_IdEmpresa" id="empresa_NU_IdEmpresa" value="<?php if(isset($_GET['AF_RazonSocial'])){ echo $_GET['AF_RazonSocial']; } ?>" size="40" readonly class="form-control input-sm">
							</div>							
							<div class="form-group">
								<label for="sede_NU_IdSede">Sede :</label>
								<input type="text" name="sede_NU_IdSede" id="sede_NU_IdSede" value="<?php if(isset($_GET['AL_NombreSede'])){ echo $_GET['AL_NombreSede']; } ?>" size="40" readonly class="form-control input-sm">
							</div>	
							<div class="form-group">
								<label for="gerencia_NU_IdGerencia">Gerencia :</label>
								<input type="text" name="gerencia_NU_IdGerencia" id="gerencia_NU_IdGerencia" value="<?php if(isset($_GET['AL_NombreGerencia'])){ echo $_GET['AL_NombreGerencia']; } ?>" size="40" readonly class="form-control input-sm">
							</div>	
							</div>
							<div class="col-sm-5 col-lg-5 col-md-5">
							<div class="form-group">
								<label for="AF_Telefono">Teléfono :</label>
								<input type="text" name="AF_Telefono" autofocus="autofocus" required id="AF_Telefono" autocomplete="off" size="40" class="form-control input-sm">
							</div>	
							<div class="form-group">
								<label for="AF_Correo">Correo Electrónico :</label>
								<input type="email" name="AF_Correo" autofocus="autofocus" required id="AF_Correo" autocomplete="off" size="40" class="form-control input-sm">
							</div>								
							<div class="form-group">
								<label for="AF_Clave">Clave :</label>
								<input type="password" name="AF_Clave" autofocus="autofocus" required id="AF_Clave" autocomplete="off" size="40" class="form-control input-sm">
							</div>
							<div class="form-group">
								<label for="AF_Clave2">Repita Clave :</label>
								<input type="password" name="AF_Clave2" autofocus="autofocus" required id="AF_Clave2" autocomplete="off" size="40" class="form-control input-sm">
							</div>
							<br>
							<div class="col-sm-2 col-lg-2 col-md-2"></div>
							<input name="origen" type="hidden" id="origen" value="UserCrear2">
							<div class="col-sm-4 col-lg-4 col-md-4">
								<button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm">Registrar</button>
							</div>
							<div class="col-sm-2 col-lg-2 col-md-2">&nbsp;</div>
							<div class="col-sm-4 col-lg-4 col-md-4">
								<button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm" onClick="javascript:window.location='../../../../index.php'">Cancelar</button>
							</div>
							</div>
						</form>
						<div>
					</div>
				</div>
			</div>

			<div class="col-sm-1 col-lg-1 col-md-1"></div>

        </div>
    </div>
    <!-- /.container -->

        <!-- Footer -->
       <footer>
        <div class="row">

      <div class="col-sm-12 col-lg-12 col-md-12"></div>
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-primary">
					<div class="panel-body">
						<p align="center"><strong>
						Venezolana de Alimentos La Casa - &copy;2015<br>
						Oficina de Tecnología de la Información
						</strong></p>
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
        </footer>
</div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
