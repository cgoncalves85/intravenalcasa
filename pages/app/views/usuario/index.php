﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/empresaModel.php'); 
	require_once('../../model/productoModel.php'); 	

	$objEmpresa 	= new Empresa();
	$objProducto 	= new Producto();	
	
	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}	
	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <span class="ui-icon ui-icon-circle-check"></span>
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Productos</h3>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								Nuevo Producto
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-2"></div>
									<div class="col-lg-8">								
										<br>
										<p>Para agregar un nuevo Producto haz click en el siguiente botón:</p>
										<a href="apertura/index.php" align="right" type="submit" name="submit" id="submit" class="btn btn-primary">Agregar Producto</a>
										<br><br>
									</div>
									<div class="col-lg-2"></div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								Listado de Productos
							</div>						

							<div class="panel-body">
								<div class="row">							
									
									<div class="col-lg-1"></div>
									<div class="col-lg-10">
										<?php 
											$url = "index.php";
											$k = 0;
											$rsProducto		= $objProducto->listarProducto($objConexion);
											$cantProducto	= $objConexion->cantidadRegistros($rsProducto);
											$rsProducto2	= $objProducto->listarProductos($objConexion);
											$cantProducto2	= $objConexion->cantidadRegistros($rsProducto2);
										?> 
										<br>
									
										<div class="dataTable_wrapper">
											<?php 
											
											if ($cantProducto>0){ 
											
												$TAMANO_PAGINA = 5;
												$pagina = false;
												//examino la pagina a mostrar y el inicio del registro a mostrar
												if (isset($_GET["pagina"]))
													$pagina = $_GET["pagina"];  
													if (!$pagina) {
														$inicio = 0;
														$pagina = 1;
													}
												else {
													$inicio = ($pagina - 1) * $TAMANO_PAGINA;
												}
												
												$total_paginas = ceil($cantProducto / $TAMANO_PAGINA);
											?>
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr align="center">
														<td style="vertical-align:middle;" ><strong>ID</strong></td>
														<td style="vertical-align:middle;" ><strong>Nombre</strong></td>
														<td style="vertical-align:middle;" ><strong>Precio</strong></td>
														<td style="vertical-align:middle;" ><strong>N° Max</strong></td>	
														<td style="vertical-align:middle;" ><strong>N° Min</strong></td>
														<td style="vertical-align:middle;" ><strong>N° Salto</strong></td>
														<td style="vertical-align:middle;" ><strong>Contenido</strong></td>
														<td style="vertical-align:middle;" colspan="2"><strong>Opciones</strong></td>	
													</tr>
												</thead>
												<tbody>
													<?php
														for($i=0; $i<$cantProducto2; $i++){
														$NU_IdProducto		= $objConexion->obtenerElemento($rsProducto2,$i,"NU_IdProducto");
														$AF_NombreProducto	= $objConexion->obtenerElemento($rsProducto2,$i,"AF_NombreProducto");
														$NU_Max				= $objConexion->obtenerElemento($rsProducto2,$i,"NU_Max");
														$NU_Min				= $objConexion->obtenerElemento($rsProducto2,$i,"NU_Min");
														$NU_Salto				= $objConexion->obtenerElemento($rsProducto2,$i,"NU_Salto");
														$AL_Medida			= $objConexion->obtenerElemento($rsProducto2,$i,"AL_Medida");
														$NU_Contenido		= $objConexion->obtenerElemento($rsProducto2,$i,"NU_Contenido");
														$BS_PrecioUnitario	= $objConexion->obtenerElemento($rsProducto2,$i,"BS_PrecioUnitario");	
													?>
													<tr align="center">
														<td style="vertical-align:middle;" ><strong><?php echo $NU_IdProducto ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $AF_NombreProducto ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $BS_PrecioUnitario ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Max ?></strong></td>	
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Min ?></strong></td>	
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Salto ?></strong></td>	
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Contenido.' '.$AL_Medida ?></strong></td>	
														<td style="vertical-align:middle;" ><a href="edit.php"><span class="glyphicon glyphicon-edit" title="Editar Producto"></span></a></td>
														<td style="vertical-align:middle;" ><a href="Constancia_Fideicomiso<?php echo $NU_IdTipoCFideicomiso; ?>.php?NU_IdCFideicomiso=<?php echo $NU_IdCFideicomiso; ?>"><span class="glyphicon glyphicon-trash" title="Eliminar Notificación"></span></a></td>
													</tr>
													<?php
														}
													?>          
												</tbody>
											</table>
											<?php
							
												echo "<div align='center' class='col-lg-5'>";
												echo "Mostrando la pagina $pagina de $total_paginas<br>";
												echo "Hay $cantProducto Productos registrados.";	
												echo "</div>";
												echo "<div align='right' class='col-lg-7'>";
												echo "<ul class='pagination'>";
												
												if ($total_paginas > 1) {
													if ($pagina != 1)
														echo '<li><a href="'.$url.'?pagina='.($pagina-1).'">&laquo;</li></a>';
														for ($i=1;$i<=$total_paginas;$i++) {
															if ($pagina == $i)
																//si muestro el �ndice de la p�gina actual, no coloco enlace
																//echo "$pagina";
																echo '<li class="active"><a href="'.$url.'?pagina='.$pagina.'">'.$pagina.'</a></li>';
																
											
															else
																//si el �ndice no corresponde con la p�gina mostrada actualmente,
																//coloco el enlace para ir a esa p�gina
																echo '<li><a href="'.$url.'?pagina='.$i.'">'.$i.'</a></li>';
														}
													if ($pagina != $total_paginas)
													echo '<li><a href="'.$url.'?pagina='.($pagina+1).'">&raquo;</li></a>';
												}
											
											}else{ echo '<br><tr align="center"><td><strong class="rojo">No Hay Productos Registrados.</strong></td></tr><br>'; }
												echo "</ul>";

												
												echo "</div>";												
												
												
												
												
																									
											?>
											
											
											
									
										</div>										
									</div>
									<div class="col-lg-1"></div>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>	
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

</body>

</html>
