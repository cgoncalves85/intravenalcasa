<?php
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/rolModel.php');
	require_once('../../model/usuarioModel.php');
	
	$objRol = new Rol();
	$objUsuario = new Usuario();
	
	if (isset($_POST['rol'])){
		$objUsuario->cambiarRol($objConexion,$_SESSION['NU_IdUsuario'],$_POST['rol']);
	}
	
	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 		= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_User 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_User");	
										
	} 
	
	$combo_categorias="";

	$sql = mysql_query("SELECT * FROM categorias");
    while($sql_p = mysql_fetch_row($sql))
    {
     $combo_categorias.= "<option value='".$sql_p[0]."'>".$sql_p[1]."</option>";
    } 

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema de Soporte</title>
	
	<!-- Importamos el Favicon -->
	<link rel="shortcut icon" href="../../img/favicon.ico"/>

    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/shop-homepage.css" rel="stylesheet">
	
	<script type="text/javascript" src="../../js/jquery.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>
	
	<script type="text/javascript" charset="utf-8">
	  $(document).ready(function() {
	  // Parametros para el combo
	   $("#categorias").change(function () {
	      $("#categorias option:selected").each(function () {
	        elegido=$(this).val();
	        $.post("subcategorias.php", { elegido: elegido }, function(data){
	        $("#subcategorias").html(data);
	      });     
	     });
	   });    
	});
	</script>

    <style>
        textarea {text-transform: uppercase;}
    </style>

<body>
    <div class="container">
        <div class="row">
            <div class="panel panel-primary">
				<div class="panel-body" style="padding:0px">
					<img src="../../img/head.jpg" alt="" class="img-responsive">
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-body" style="padding:0px; border: 1px solid #FFF">
					<img src="../../img/soporte.jpg" alt="" class="img-responsive">
				</div>
			</div>
				
	
		<!-- Navigation -->
		<nav class="navbar navbar-default" role="navigation">
  			<!-- El logotipo y el icono que despliega el menú se agrupan
       		para mostrarlos mejor en los dispositivos móviles -->
  			<div class="navbar-header">
    			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      				<span class="sr-only">Desplegar navegación</span>
      				<span class="icon-bar"></span>
      				<span class="icon-bar"></span>
      				<span class="icon-bar"></span>
    			</button>
    			<a class="navbar-brand" style="padding:7px" href="#"><img class="img-responsive" src="../../img/inicio.png" alt=""></a>
  			</div>
 			<!-- Agrupar los enlaces de navegación, los formularios y cualquier
       		otro elemento que se pueda ocultar al minimizar la barra -->
  			<div class="collapse navbar-collapse navbar-ex1-collapse">
    			<ul class="nav navbar-nav">
					<li><a href="../index.php">INICIO</a></li>
					<?php if ($_SESSION['BI_Admin']==1){ ?>
      				<li><a href="../usuario/listado/index.php">GESTIONAR USUARIOS</a></li>
					<?php } ?>	
					<?php if ($BI_User==1){  ?>
      				<li><a href="index.php">ASIGNACIONES</a></li>
					<?php } ?>					
      				<li><a href="listado/index.php">SOLICITUDES</a></li>
      				<li class="dropdown">
        				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
          					REPORTES <b class="caret"></b>
        				</a>
        				<ul class="dropdown-menu">
          					<li><a href="../perfil/index.php">Usuarios</a></li>
          					<li class="divider"></li>
          					<li><a href="../salirView.php">Solicitudes</a></li>
        				</ul>
      				</li>
    			</ul>
				<p class="navbar-text pull-right">
					<a href="../perfil.php?id=<?=$_SESSION['NU_IdUsuario']?>" class="navbar-link"><strong><?=$_SESSION['AL_NombreApellido']?></strong></a>
					<a href="../logout.php" style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-off"></span></a>
				</p>				
  			</div>
		</nav>
		</div>
	</div>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
			<div class="panel panel-primary">
				<div class="panel-body">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#solicitudes" aria-controls="solicitudes" role="tab" data-toggle="tab">Generar Solicitudes</a></li>
				</ul>
						
				<div class="tab-content">					
					<div role="tabpanel" class="tab-pane active" id="solicitudes">
					<br>
					<div class="BlancoGris" style="margin:0px">
						Llene el siguiente formulario para generar una nueva solicitud :
					</div>
					<br>
					<form role="form" method="POST" action="guardar_solicitud.php" enctype="multipart/form-data">

						<div class="col-lg-6">
						<div class="form-group">
							<label>Usuario:</label>
							<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-user"></i> </span>
								<input class="form-control" name="AL_Nombre" value="<?=$_SESSION['AL_NombreApellido']?>" id="AL_Nombre" readonly size="15" type="text">
							</div>
						</div>
						</div>
						<div class="col-lg-6">
						<div class="form-group">
							<label>Ubicación:</label>
							<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-map-marker"></i> </span>
								<input class="form-control" name="AL_Adscripcion" value="<?=$_SESSION['AL_Adscripcion']?>"  id="AL_Adscripcion" readonly size="15" type="text">
							</div>
						</div>	
						</div>						
						<div class="col-lg-12">&nbsp;</div>
						<div class="col-lg-6">
							<label>Categoria:</label>
							<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-th"></i> </span>
							    <select class="form-control"  name="categorias" id="categorias" required>
									<option value="0">SELECCIONE...</option>
									<?php  echo $combo_categorias;?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<label>Subcategoria:</label>
							<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-th-list"></i> </span>
								<select class="form-control"  name="subcategorias" id="subcategorias" required></select>
							</div>
						</div>					
						<div class="col-lg-12">&nbsp;</div>
						<div class="col-lg-6">
							<label>Descripción:</label>
							<textarea class="form-control" name="descripcion" required id="subcategorias" rows="3" placeholder="Máximo (250 caracteres)"></textarea>
						</div>							

						<div class="col-lg-6">
							<label>Adjuntar un archivo:</label>						
							<input type="file" class="btn btn-default" id="archivo" name="imagen">
							<p class="help-block">Imágen referencial a la solicitud (No es Obigatorio).</p>
						</div>
						<div class="col-lg-12">&nbsp;</div>						
						<div class="col-lg-12" align="right">
							<input class="form-control" name="id_user" value="<?=$_SESSION['NU_IdUsuario']?>"  id="id_user" type="hidden">
							<button type="submit" name="guardar" class="btn btn-success"><span class="glyphicon glyphicon-share-alt"></span>&nbsp;&nbsp;&nbsp;&nbsp;Generar Solicitud</button>
						</div>
						<div class="col-lg-12">&nbsp;</div>	
					</form>
					</div>
				</div>
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="col-md-4">
						<p style="margin-top:10px" align="center">Venezolana de Alimentos La Casa S.A. 2015<br>RIF: G-20008504-5</p>
					</div>
					<div class="col-md-6">
						<p style="margin-top:10px" align="center">Oficina de Tecnología de la Información<br>Copyright &copy; 2015 - SGS Venalcasa.</p>
					</div>
					<div class="col-md-2" align="center">
						<img src="../../img/venalcasa.jpg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
		
	</div>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>

</body>

</html>
