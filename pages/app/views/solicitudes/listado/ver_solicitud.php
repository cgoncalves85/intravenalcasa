<?php
	require_once('../../../controller/sessionController.php'); 
	require_once('../../../model/rolModel.php');
	require_once('../../../model/usuarioModel.php');
	
	$objRol = new Rol();
	$objUsuario = new Usuario();
	
	if (isset($_POST['rol'])){
		$objUsuario->cambiarRol($objConexion,$_SESSION['NU_IdUsuario'],$_POST['rol']);
	}
	
	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 		= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_User 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_User");	
										
	}
	

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema de Soporte</title>
	
	<!-- Importamos el Favicon -->
	<link rel="shortcut icon" href="../../../img/favicon.ico"/>

    <!-- Bootstrap Core CSS -->
    <link href="../../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../css/shop-homepage.css" rel="stylesheet">
	
	<script type="text/javascript" src="../../../js/jquery.js"></script>
	<script type="text/javascript" src="../../../js/jquery-ui.js"></script>	

	
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="panel panel-primary">
				<div class="panel-body" style="padding:0px">
					<img src="../../../img/head.jpg" alt="" class="img-responsive">
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-body" style="padding:0px; border: 1px solid #FFF">
					<img src="../../../img/soporte.jpg" alt="" class="img-responsive">
				</div>
			</div>
				
	
		<!-- Navigation -->
		<nav class="navbar navbar-default" role="navigation">
  			<!-- El logotipo y el icono que despliega el menú se agrupan
       		para mostrarlos mejor en los dispositivos móviles -->
  			<div class="navbar-header">
    			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      				<span class="sr-only">Desplegar navegación</span>
      				<span class="icon-bar"></span>
      				<span class="icon-bar"></span>
      				<span class="icon-bar"></span>
    			</button>
    			<a class="navbar-brand" style="padding:7px" href="#"><img class="img-responsive" src="../../../img/inicio.png" alt=""></a>
  			</div>
 			<!-- Agrupar los enlaces de navegación, los formularios y cualquier
       		otro elemento que se pueda ocultar al minimizar la barra -->
  			<div class="collapse navbar-collapse navbar-ex1-collapse">
    			<ul class="nav navbar-nav">
					<li><a href="../../index.php">INICIO</a></li>
					<?php if ($_SESSION['BI_Admin']==1){ ?>
      				<li><a href="../../usuario/listado/index.php">GESTIONAR USUARIOS</a></li>
					<?php } ?>	
					<?php if ($BI_User==1){  ?>
      				<li><a href="../index.php">ASIGNACIONES</a></li>
					<?php } ?>					
      				<li><a href="../listado/index.php">SOLICITUDES</a></li>
      				<li class="dropdown">
        				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
          					REPORTES <b class="caret"></b>
        				</a>
        				<ul class="dropdown-menu">
          					<li><a href="../perfil/index.php">Usuarios</a></li>
          					<li class="divider"></li>
          					<li><a href="../salirView.php">Solicitudes</a></li>
        				</ul>
      				</li>
    			</ul>
				<p class="navbar-text pull-right">
					<a href="../../perfil.php?id=<?=$_SESSION['NU_IdUsuario']?>" class="navbar-link"><strong><?=$_SESSION['AL_NombreApellido']?></strong></a>
					<a href="../../logout.php" style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-off"></span></a>
				</p>				
  			</div>
		</nav>
		</div>
	</div>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
			<div class="panel panel-primary">
				<div class="panel-body">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#solicitudes" aria-controls="solicitudes" role="tab" data-toggle="tab">Solicitudes</a></li>
				</ul>
						
				<div class="tab-content">					
					<div role="tabpanel" class="tab-pane active" id="solicitudes">
					<br>
					<div class="BlancoGris" style="margin:0px">
						A continuación se presenta la solicitud consultada :
					</div>
					<br>
								<?php 
								
									$id_solicitud=$_POST["id"];
									
									mysql_query("SET NAMES 'utf8'");
									$consulta = "SELECT * FROM solicitudes where id = $id_solicitud";
									$rs = mysql_query($consulta);
									while ($row = mysql_fetch_array($rs)) {
										$date = date_create($row['fecha']);
										$date = date_format($date, 'd/m/Y');
										
										$cat = $row['categoria'];
										$subcat = $row['subcategoria'];
										$status = $row['estatus'];
										
										mysql_query("SET NAMES 'utf8'");
										$consulta1 = "SELECT * FROM categorias where id_categoria = $cat";
										$rs1 = mysql_query($consulta1);										
										while ($row1 = mysql_fetch_array($rs1)) {
										$categ = $row1['categoria'];
										}
										
										mysql_query("SET NAMES 'utf8'");
										$consulta1 = "SELECT * FROM subcategorias where id_subcategoria = $subcat";
										$rs1 = mysql_query($consulta1);										
										while ($row1 = mysql_fetch_array($rs1)) {
										$subcateg = $row1['subcategoria'];
										}

										mysql_query("SET NAMES 'utf8'");
										$consulta1 = "SELECT * FROM estatus where NU_IdEstatus = $status";
										$rs1 = mysql_query($consulta1);										
										while ($row1 = mysql_fetch_array($rs1)) {
										$estatus = $row1['AF_Estatus'];
										}
										
										$tec = $row['tecnico'];
										mysql_query("SET NAMES 'utf8'");
										$consulta1 = "SELECT * FROM usuario where NU_IdUsuario = $tec";
										$rs1 = mysql_query($consulta1);										
										while ($row1 = mysql_fetch_array($rs1)) {
										$nombre = $row1['AL_Nombre'];
										}											
										
										echo "<div class='col-md-2'><img src='../".$row['imagen']."'class='img-thumbnail' style='width:auto; height:auto'></div>";
										
										echo "<div class='col-md-10'>";
										echo "<blockquote style='color:#790D0D'>Categoria de la Solicitud: $categ - $subcateg";
										echo "<small>Solicitud N° OTI-S000$row[id]</small></blockquote>";
										
										echo "<u>Descripción de la Solicitud:</u> ";
										echo "<br><br><div align='justify' style='text-transform:uppercase; padding-left:15px'>$row[descripcion] </div>";
										
										
										echo "<hr>";
										echo "<div class='col-md-4'>";
										echo "Solicitante: $row[usuario]";
										echo "</div>";
										
										echo "<div class='col-md-4'>";
										echo "Fecha de Solicitud: $date";
										echo "</div>";
										
										echo "<div class='col-md-4'>";
										echo "Asignado A: ".$nombre."</div>";
										echo "<div class='col-md-12'>&nbsp;</div>";
										
										if ($_SESSION['BI_Admin']==1){
										echo "<div class='col-md-6'>";
										echo '<label>Reasignar A:</label>';
										echo '<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-user green"></i> </span>';
										
										$RSTec 		= $objRol->listarTecnicos($objConexion);
										$cantRSTec 	= $objConexion->CantidadRegistros($RSTec);
										
										echo "<form action='asignar.php' method='post' class='form-horizontal' role='form'>";
										
										echo '<select class="form-control" name="tecnico" id="tecnico">';
										
										if ($cantRSTec>0){
											for($i=0; $i<$cantRSTec; $i++){	
												$valor = $objConexion->obtenerElemento($RSTec,$i,"NU_IdUsuario");
												$desc = $objConexion->obtenerElemento($RSTec,$i,"AL_Nombre");						
												$selected="";
												if($tec==$valor){
													$selected="selected='selected'";
												}
												echo "<option value=".$valor." ".$selected.">".$desc."</option>";
											}
										}
										echo "</select>";
										echo "</div></div>";
										echo "<input name='id_solicitud' type='hidden' value='$row[id]'>";
										echo "<div class='col-md-6'>
										<label>&nbsp;</label>
										<div align='right'>
											<button name='btnLogin' type='submit' id='btnLogin' class='btn btn-primary'><span class='glyphicon glyphicon-tags'></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Asignar Solicitud</button>
										</div><br><br></div></form>";
										} else
										{
											echo "<div class='col-md-12'>";
											echo "<label>Estátus de la Solicitud: <b>$estatus</b></label>";
											echo "</div>";
											echo "<div class='col-md-12'>";
											echo "<br><u>Observaciones:</u> ";
											echo "</div>";
											echo "<div class='col-md-12'>";
											echo "<p align='justify' style='text-transform:uppercase'>$row[observacion]</p>";
											echo "</div>";											
											
										}
										
										echo "</div>";
									}
								?>
					</div>
				</div>
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="col-md-4">
						<p style="margin-top:10px" align="center">Venezolana de Alimentos La Casa S.A. 2015<br>RIF: G-20008504-5</p>
					</div>
					<div class="col-md-6">
						<p style="margin-top:10px" align="center">Oficina de Tecnología de la Información<br>Copyright &copy; 2015 - SGS Venalcasa.</p>
					</div>
					<div class="col-md-2" align="center">
						<img src="../../../img/venalcasa.jpg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
		
	</div>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>

</body>

</html>
