﻿<?php 
	require_once('../../../controller/sessionController.php'); 
	require_once('../../../model/solicitudesModel.php');

	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
	}	

	$objSolicitudes = new Solicitudes();

	$RS 	= $objSolicitudes->listarSolicitudesIndv($objConexion,$_SESSION["NU_IdUsuario"]);
	$cantRS = $objConexion->cantidadRegistros($RS);

		///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
		function setFechaNoSQL($FE_FechaNac)
		{
			$partes = explode("-", $FE_FechaNac);
			$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
			return $FE_FechaNac;
		}
		//////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body>
	<div id="dialog" title="Atención !!" style="display:none;">
		<p>En Construccion!!.</p>
	</div>
    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Mis Solicitudes de Soporte</h4>
						<div class="panel panel-default">
							<div class="panel-heading">
								Listados de Solicitudes que ha realizado.
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-10">	
										<br>
										
										<?php 
											$cont = 1;
											if ($cantRS>0){ 
										?>
										<div>
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead align="center">
													<tr>
														<td style="vertical-align:middle;" ><strong>N°</strong></td>
														<!--<td style="vertical-align:middle;" ><strong>Solicitante</strong></td>-->
														<td style="vertical-align:middle;" ><strong>Ubicación</strong></td>
														<td style="vertical-align:middle;" ><strong>Descripcion</strong></td>

													</tr>
												</thead>
												<tbody align="center">
													<?php
														for($i=0; $i<$cantRS; $i++){	
														//$Solicitante 		= $objConexion->obtenerElemento($RS,$i,'usuario');
														$Adscripcion 		= $objConexion->obtenerElemento($RS,$i,'adscripcion');
														$Descripcion 		= $objConexion->obtenerElemento($RS,$i,'descripcion');

													?>
													<tr>
														<td><?php echo $cont; $cont = $cont+1;?></td>
														<!--<td><?php echo $Solicitante; ?></td>-->
														<td><?php echo $Adscripcion; ?></td>
														<td><?php echo $Descripcion; ?></td>

													</tr>
													<?php
													
													}
													?>          
												</tbody>
											</table>
										</div>
										<?php
											
											}else{ echo '<tr align="center"><td>No se encontraron registros.</td></tr>'; 
											}
										?>	
									</div>
									<div class="col-lg-1"></div>
								</div>
							</div>
						</div>

					</div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
