<?php 
	require_once('../../../controller/sessionController.php'); 
	require_once('../../../model/rolModel.php');
	require_once('../../../model/usuarioModel.php');
	
	$objRol = new Rol();
	$objUsuario = new Usuario();
	
	if (isset($_POST['rol'])){
		$objUsuario->cambiarRol($objConexion,$_SESSION['NU_IdUsuario'],$_POST['rol']);
	}
	
	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 		= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_User 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_User");	
										
	} 
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema de Soporte</title>
	
	<!-- Importamos el Favicon -->
	<link rel="shortcut icon" href="../../../img/favicon.ico"/>

    <!-- Bootstrap Core CSS -->
    <link href="../../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../css/shop-homepage.css" rel="stylesheet">
    <link href="../../../css/jquery-ui.css" rel="stylesheet">
	
	<script type="text/javascript" src="../../../js/jquery.js"></script>
	<script type="text/javascript" src="../../../js/jquery-ui.js"></script>
	
	<script type="text/javascript">
		function abrir_dialog() {
			$( "#dialog" ).dialog({
				show: "blind",
				hide: "explode",
				modal: true,
				buttons: {
					Aceptar: function() {
					$( this ).dialog( "close" );
					}
				}
			});
		};
	</script>
		
	
</head>

<body onLoad="abrir_dialog();">
<?php if (isset($_GET['mensaje'])) { ?>
	<div id="dialog" title="Mensaje" style="display:none;">
		<span class="ui-icon ui-icon-circle-check"></span>
		<p><?php echo $_GET['mensaje']; ?></p>
	</div>
<?php } ?>
    <div class="container">
        <div class="row">
            <div class="panel panel-primary">
				<div class="panel-body" style="padding:0px">
					<img src="../../../img/head.jpg" alt="" class="img-responsive">
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-body" style="padding:0px; border: 1px solid #FFF">
					<img src="../../../img/soporte.jpg" alt="" class="img-responsive">
				</div>
			</div>
				
	
		<!-- Navigation -->
		<nav class="navbar navbar-default" role="navigation">
  			<!-- El logotipo y el icono que despliega el menú se agrupan
       		para mostrarlos mejor en los dispositivos móviles -->
  			<div class="navbar-header">
    			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      				<span class="sr-only">Desplegar navegación</span>
      				<span class="icon-bar"></span>
      				<span class="icon-bar"></span>
      				<span class="icon-bar"></span>
    			</button>
    			<a class="navbar-brand" style="padding:7px" href="../../index.php"><img class="img-responsive" src="../../../img/inicio.png" alt=""></a>
  			</div>
 			<!-- Agrupar los enlaces de navegación, los formularios y cualquier
       		otro elemento que se pueda ocultar al minimizar la barra -->
  			<div class="collapse navbar-collapse navbar-ex1-collapse">
    			<ul class="nav navbar-nav">
					<li><a href="../../index.php">INICIO</a>
					<?php if ($_SESSION['BI_Admin']==1){ ?>
      				<li><a href="../../usuario/listado/index.php">GESTIONAR USUARIOS</a></li>
					<?php } ?>
					<?php if ($BI_User==1){  ?>
      				<li><a href="../index.php">ASIGNACIONES</a></li>
					<?php } ?>					
      				<li class="active"><a href="#">SOLICITUDES</a></li>
      				<li class="dropdown">
        				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
          					REPORTES <b class="caret"></b>
        				</a>
        				<ul class="dropdown-menu">
          					<li><a href="perfil/index.php">Usuarios</a></li>
          					<li class="divider"></li>
          					<li><a href="salirView.php">Solicitudes</a></li>
        				</ul>
      				</li>
    			</ul>
				<p class="navbar-text pull-right">
					<a href="../../perfil.php?id=<?=$_SESSION['NU_IdUsuario']?>" class="navbar-link"><strong><?=$_SESSION['AL_NombreApellido']?></strong></a>
					<a href="../../logout.php" style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-off"></span></a>
				</p>					
  			</div>
		</nav>
		</div>
	</div>
    <!-- Page Content -->
    <div class="container">

        <div class="row">
			<div class="panel panel-primary">
				<div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#solicitudes" aria-controls="solicitudes" role="tab" data-toggle="tab">Mis Solicitudes</a></li>
						</ul>
						
						<div class="tab-content">					
							<div role="tabpanel" class="tab-pane active" id="solicitudes">
								<br>
								<div class="BlancoGris" style="margin:0px">
									Listado de Mis Solicitudes
								</div>
								<br>
								<?php

								//Sentencia sql (sin limit) 
								if ($_SESSION['BI_Admin']==1) {
								$_pagi_sql = "SELECT * FROM solicitudes ORDER BY ID DESC";
								} else
								{
								$_pagi_sql = "SELECT * FROM solicitudes WHERE id_usuario = '$_SESSION[NU_IdUsuario]'  ORDER BY ID DESC"; 
								}

								//cantidad de resultados por página (opcional, por defecto 20) 
								$_pagi_cuantos = 10; 

								//Incluimos el script de paginación. Éste ya ejecuta la consulta automáticamente 
								include("paginator.inc.php"); 
								
								//Leemos y escribimos los registros de la página actual 
								$cont_not = 1;
								echo "<div class= 'table table-responsive'>";
								echo "<table class='table table-hover'>";
								echo "<thead>";
								echo "<tr  align='center'  style='color: #000; font-weight:700; text-transform:uppercase'><td style='vertical-align:middle'>Nº</td><td style='vertical-align:middle'>Solicitante</td><td style='vertical-align:middle'>Categoria</td><td style='vertical-align:middle'>Fecha de Solicitud</td><td style='vertical-align:middle'>Estátus</td><td style='vertical-align:middle' colspan='3'>Acciones</td>";
								echo "</tr>";
								echo "</thead>";								
								while($row=mysql_fetch_array($_pagi_result))
								{ 
									$date = date_create($row['fecha']);
									$date=date_format($date, 'd/m/Y');
									
									$cat = $row['categoria'];
									
									mysql_query("SET NAMES 'utf8'");
									$consulta1 = "SELECT * FROM categorias where id_categoria = $cat";
									$rs1 = mysql_query($consulta1);										
									while ($row1 = mysql_fetch_array($rs1)) {
									$categ = $row1['categoria'];
									}	
									
									echo "<tbody style='text-transform:capitalize'>";
									echo "<tr align='center'>";
									echo "<td style='vertical-align:middle'>$cont_not</td>";
									echo "<td style='vertical-align:middle'>".$row["usuario"]."</td>";
									echo "<td style='vertical-align:middle'>".$categ."</td>";
									echo "<td style='vertical-align:middle'>$date</td>";
									echo "<td style='vertical-align:middle'>";
									if($row['estatus']==1)
									{
									echo "<span title='EN PROCESO' class='glyphicon glyphicon-eye-open orange'></span>";	
									}else
									if($row['estatus']==2)
									{
									echo "<span title='FINALIZADA' class='glyphicon glyphicon-check green'></span>";	
									}else
									{
									echo "<span title='PENDIENTE' class='glyphicon glyphicon-exclamation-sign red'></span>";		
									}
									echo"</td>";
									echo "<td style='vertical-align:middle' align='center'>";
									echo "<form action='ver_solicitud.php' method='post' class='form-horizontal' role='form'>";
									echo "<input type='hidden' name='id' value='".$row['id']."'>";
									echo "<div align='center'><button name='btnLogin' type='submit' id='btnLogin' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-search'></span> Ver</button></div>";
									echo "</td></form>";
									if ($BI_User==1){ 
									echo "<td style='vertical-align:middle' align='center'>";
									echo "<form action='editar_solicitud.php' method='post' class='form-horizontal' role='form'>";
									echo "<input type='hidden' name='id_solicitud' value='".$row['id']."'>";
									echo "<div align='center'><button name='btnLogin' type='submit' id='btnLogin' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-edit'></span> Editar</button></div>";
									echo "</td></form>";
									}
									if ($_SESSION['BI_Admin']==1) {
									echo "<td style='vertical-align:middle' align='center'>";
									echo "<form action='eliminar_solicitud.php' method='post' class='form-horizontal' role='form'>";
									echo "<input type='hidden' name='id_solicitud' value='".$row['id']."'>";
									echo "<div align='center'><button name='btnLogin' type='submit' id='btnLogin' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-trash'></span> Eliminar</button></div>";
									echo "</form>";
									echo "</td>";
									}
									echo "</tr>";
									echo "</tbody>";
									$cont_not = $cont_not + 1;
								}								
								echo "</table></div>";
								//Incluimos la barra de navegación 
								echo"<div align='center'><p>".$_pagi_navegacion."</p></div>";
								?>
							</div>
						</div>
				</div>
			</div>
			
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="col-md-4">
						<p style="margin-top:10px" align="center">Venezolana de Alimentos La Casa S.A. 2015<br>RIF: G-20008504-5</p>
					</div>
					<div class="col-md-6">
						<p style="margin-top:10px" align="center">Oficina de Tecnología de la Información<br>Copyright &copy; 2015 - SGS Venalcasa.</p>
					</div>
					<div class="col-md-2" align="center">
						<img src="../../../img/venalcasa.jpg" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
		
	</div>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../js/bootstrap.min.js"></script>

</body>

</html>
