﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/rolModel.php');
	require_once('../../model/usuarioModel.php');
	
	
	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
		$AL_Adscripcion 		= $objConexion->obtenerElemento($RSUsuario,0,"AL_Adscripcion");	
	}	

	$combo_categorias="";

	$sql = mysql_query("SELECT * FROM categorias");
    while($sql_p = mysql_fetch_row($sql))
    {
     $combo_categorias.= "<option value='".$sql_p[0]."'>".$sql_p[1]."</option>";
    } 	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Solicitudes de Soporte Técnico</h4>
						<form role="form" method="POST" action="guardar_solicitud.php" enctype="multipart/form-data">
							<div class="panel panel-default">
								<div class="panel-heading">
									Soporte Técnico &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Llene el siguiente formulario para generar una nueva solicitud.
								</div>
								<div class="panel-body">
									<div class="row">
									
										<div class="col-lg-12">	
											<br>
											
											<div class="col-lg-6">
												<div class="form-group">
													<label>Usuario:</label>
													<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-user"></i> </span>
														<input class="form-control" name="AL_Nombre" value="<?=$_SESSION['AL_NombreApellido']?>" id="AL_Nombre" readonly size="15" type="text">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Ubicación:</label>
													<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-map-marker"></i> </span>
														<input class="form-control" name="AL_Adscripcion" value="<?=$AL_Adscripcion?>"  id="AL_Adscripcion" readonly size="15" type="text">
													</div>
												</div>	
											</div>						
											<div class="col-lg-12">&nbsp;</div>
											<div class="col-lg-6">
												<label>Categoria:</label>
												<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-th"></i> </span>
													<select class="form-control"  name="categorias" id="categorias" required>
														<option value="0">Seleccione...</option>
														<?php  echo $combo_categorias;?>
													</select>
												</div>
											</div>
											<div class="col-lg-6">
												<label>Subcategoria:</label>
												<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-th-list"></i> </span>
													<select class="form-control"  name="subcategorias" id="subcategorias" required></select>
												</div>
											</div>					
											<div class="col-lg-12">&nbsp;</div>
											<div class="col-lg-6">
												<label>Descripción:</label>
												<textarea class="form-control" name="descripcion" required id="subcategorias" rows="5" placeholder="Máximo (250 caractéres)"></textarea>
											</div>							

											<div class="col-lg-6">
												<label>Adjuntar un archivo:</label>						
												<input type="file" class="btn btn-default" id="archivo" name="imagen">
												<p class="help-block">Imágen referencial a la solicitud (No es Obligatoria).</p>
												<br>
												<div align="right">
													<button type="submit" name="guardar" class="btn btn-primary"><span class="glyphicon glyphicon-share-alt"></span>&nbsp;&nbsp;&nbsp;&nbsp;Generar Solicitud</button>
												</div>
											</div>
											<div class="col-lg-12">&nbsp;</div>						
											<div class="col-lg-12" align="right">
												<input class="form-control" name="id_user" value="<?=$_SESSION['NU_IdUsuario']?>"  id="id_user" type="hidden">
											</div>
											<div class="col-lg-12">&nbsp;</div>	
														
										</div>
									</div>
								</div>
							</div>
						</form>
						<br><br>
						<div class="panel panel-default">
							<div class="panel-heading">
								Configuración &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Listado de Productos.
							</div>						

							<div class="panel-body">
								<div class="row">	

									<div class="dataTable_wrapper" align="center" style="margin:50px">
						
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<td style="vertical-align:middle;" ><strong>ID</strong></td>
													<td style="vertical-align:middle;" ><strong>Nombre</strong></td>
													<td style="vertical-align:middle;" ><strong>Precio</strong></td>
													<td style="vertical-align:middle;" ><strong>N° Max</strong></td>	
													<td style="vertical-align:middle;" ><strong>N° Min</strong></td>
													<td style="vertical-align:middle;" ><strong>N° Salto</strong></td>
													<td style="vertical-align:middle;" ><strong>Contenido</strong></td>
													<td style="vertical-align:middle;" ><strong>Editar</strong></td>
													<td style="vertical-align:middle;" ><strong>Eliminar</strong></td>
												</tr>
											</thead>
     
											<tbody>
												<?php
													for($i=0; $i<$cantProducto; $i++){
													$NU_IdProducto		= $objConexion->obtenerElemento($rsProducto,$i,"NU_IdProducto");
													$AF_NombreProducto	= $objConexion->obtenerElemento($rsProducto,$i,"AF_NombreProducto");
													$NU_Max				= $objConexion->obtenerElemento($rsProducto,$i,"NU_Max");
													$NU_Min				= $objConexion->obtenerElemento($rsProducto,$i,"NU_Min");
													$NU_Salto			= $objConexion->obtenerElemento($rsProducto,$i,"NU_Salto");
													$AL_Medida			= $objConexion->obtenerElemento($rsProducto,$i,"AL_Medida");
													$NU_Contenido		= $objConexion->obtenerElemento($rsProducto,$i,"NU_Contenido");
													$BS_PrecioUnitario	= $objConexion->obtenerElemento($rsProducto,$i,"BS_PrecioUnitario");
												?>	
												
													<tr align="center">
														<td style="vertical-align:middle;" ><strong><?php echo $NU_IdProducto ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $AF_NombreProducto ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $BS_PrecioUnitario ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Max ?></strong></td>	
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Min ?></strong></td>	
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Salto ?></strong></td>	
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Contenido.' '.$AL_Medida ?></strong></td>	
														<td style="vertical-align:middle;" ><a href="edit.php"><span class="glyphicon glyphicon-edit" title="Editar Unidad de Medida"></span></a></td>
														<td style="vertical-align:middle;" ><a href="delete.php"><span class="glyphicon glyphicon-trash" title="Eliminar Unidad de Medida"></span></a></td>
													</tr>
												<?php
													}
												?>          
											</tbody>
										</table>
									</div>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>	
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

	<script type="text/javascript" charset="utf-8">
	  $(document).ready(function() {
	  // Parametros para el combo
	   $("#categorias").change(function () {
	      $("#categorias option:selected").each(function () {
	        elegido=$(this).val();
	        $.post("subcategorias.php", { elegido: elegido }, function(data){
	        $("#subcategorias").html(data);
	      });     
	     });
	   });    
	});
	</script>

    <style>
        textarea{text-transform: capitalize;}
    </style>		

</body>

</html>
