﻿<?php
error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
ini_set('display_errors', '1');
?>
<?php
/*
$path = getcwd();
echo "La ruta absoluta es: ";
echo $path;
die();
*/
?>
<?php 
	require_once('../controller/sessionController.php'); 
	require_once('../model/rolModel.php');
	require_once('../model/usuarioModel.php');
	require_once('../model/mercadoModel.php');
	require_once('../model/pedidoModel.php');
	require_once('../model/ctrabajoModel.php');
	require_once('../model/cfideicomisoModel.php');
	
	$objRol 			= new Rol();
	$objUsuario 		= new Usuario();
	$objMercado 		= new Mercado();
	$objPedido 			= new Pedido();
	$objCTrabajo 		= new CTrabajo();
	$objCFideicomiso 	= new CFideicomiso();
	
	if (isset($_POST['rol'])){
		$objUsuario->cambiarRol($objConexion,$_SESSION['NU_IdUsuario'],$_POST['rol']);
	}
	
	$RSMercado 		= $objMercado->listarMercado($objConexion);
	$cantMercado 	= $objConexion->CantidadRegistros($RSMercado);	
	
	$RSPedido 		= $objPedido->listarPedidoIndiv($objConexion,$_SESSION['NU_IdUsuario']);
	$cantPedido 	= $objConexion->CantidadRegistros($RSPedido);	
	
	$RSCTrabajo 	= $objCTrabajo->listarConstancias($objConexion,$_SESSION['NU_IdUsuario']);
	$cantCT 		= $objConexion->CantidadRegistros($RSCTrabajo);
	
	$RSFideicomiso 	= $objCFideicomiso->listarConstancias($objConexion,$_SESSION['NU_IdUsuario']);
	$cantCF 		= $objConexion->CantidadRegistros($RSFideicomiso);	
	
	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
	}
	
	
	$RS = $objUsuario->listarActivos($objConexion);
	$cantU = $objConexion->CantidadRegistros($RS);

	$FechaActual 	= Date("Y-m-d");
	//echo "La Fecha Actual es: ".$FechaActual;
	$FechaHoy 		= explode ('-',$FechaActual);
	$AyoHoy = $FechaHoy[0];
	$MesHoy = $FechaHoy[1];
	$DiaHoy = $FechaHoy[2];
	

	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../css/jquery-ui.css" rel="stylesheet">
	
	<!-- Clippy CSS -->
	<link rel="stylesheet" type="text/css" href="../clippy/build/clippy.css" media="all">
	<script src="../js/jquery-1.11.3.min.js"></script>
	<script src="../clippy/build/clippy.min.js"></script>
	

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	
	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
			$( "#dialog" ).dialog({
				show: "blind",
				hide: "explode",
				modal: true,
				buttons: {
					Aceptar: function() {
					$( this ).dialog( "close" );
					}
				}
			});
			}
		};
	</script>	
	
	<!-- Este Script es para activar el Ayudante Office, si descomenta este
		código aparecera Merlin(Ayudante de Office)
		
	<script type="text/javascript">
		   
		clippy.load('F1', function(agent){    
			agent.show();
			agent.speak('Bienvenidos');
			
			agent.gestureAt(400, 400);
			agent.play ('SendMail');
			agent.play ('GetAttention');
			agent.play ('Searching');
			agent.animations("MoveLeft", "Congratulate", "Hide", "Pleased", "Acknowledge");
		});
	</script>
	
-->
</head>

<body onLoad="abrir_dialog();">
	
	<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
		<p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
	</div>

    <div id="wrapper">

        <!-- Navigation -->
		
		<?php include "nav.php"; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Menú Principal</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
				<?php if ($BI_ASGestionMercado==1){  ?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $cantU; ?></div>
								</div>
								<div class="col-xs-12">
									<br>
                                    <div> Total de <br>Usuarios</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $cantMercado; ?></div>
								</div>
								<div class="col-xs-12">
									<br>
                                    <div>Total de <br>Mercados</div>
                                </div>
                            </div>
                        </div>
                        <a href="apertura_mercado/index.php">
                            <div class="panel-footer">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<?php } ?>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-credit-card fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $cantPedido; ?></div>
								</div>
								<div class="col-xs-12">
									<br>
                                    <div>Mis <br>Compras</div>
                                </div>
                            </div>
                        </div>
                        <a href="historial/index.php">
                            <div class="panel-footer">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $cantCT; ?></div>
								</div>
								<div class="col-xs-12">	
									<br>
                                    <div>Constancias de<br> Trabajo</div>
                                </div>
                            </div>
                        </div>
                        <a href="ctrabajo/index.php">
                            <div class="panel-footer">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $cantCF; ?></div>
								</div>
								<div class="col-xs-12">
									<br>
                                    <div> Notificación de<br> Fideicomiso</div>
                                </div>
                            </div>
                        </div>
                        <a href="cfideicomiso/index.php">
                            <div class="panel-footer">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">4</div>
								</div>
								<div class="col-xs-12">
									<br>
                                    <div>Mis Tickets de <br>Soporte</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Ver Detalles</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>				
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Cumpleaños</h3>
					<?php
						echo "<h4>Felicidades a : </h4><br>";
						
						for($i=0; $i<$cantU; $i++){
							$Nombre	 		= $objConexion->obtenerElemento($RS,$i,"AL_Nombre");
							$FE_FechaNac2	= $objConexion->obtenerElemento($RS,$i,"FE_FechaNac");
							$Foto	 		= $objConexion->obtenerElemento($RS,$i,"AL_Foto");
							//echo "La Fecha de Nacimiento es: ".$FE_FechaNac2. "<br>";
							$Fecha 		= explode ('-',$FE_FechaNac2);
							$MesNac = $Fecha[1];
							$DiaNac = $Fecha[2];
							//echo $MesNac."<br>";
							
							if (($MesHoy === $MesNac) AND ($DiaHoy === $DiaNac)) {
								echo "<div align='center' class='col-lg-2 col-md-6'>";
								echo "<img src='".$Foto."' class='img-thumbnail'>";	
								echo "<br><br><div align='center'>".$Nombre."</div><br><br><br>";
								echo "</div>";				
							} 		
						}
					?>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Custom JavaScript -->
   <script type="text/javascript" src="../js/jquery-ui.js"></script>
   <script type="text/javascript" src="../js/maximizar.js"></script>
   <script type="text/javascript" src="../js/desconectar.js"></script>	


</body>

</html>
