﻿<?php 
error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
ini_set('display_errors', '1');
?>
<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/mercadoModel.php');
	require_once('../../model/usuarioModel.php');

	$objMercado = new Mercado();

	$RS 	= $objMercado->listarMercado($objConexion);
	$cantRS = $objConexion->cantidadRegistros($RS);

	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	//////////////////////////////////////////////////////////////

	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}	
	
	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="center"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Gestionar Mercado</h4>
						
						<div class="panel panel-default">
							<div class="panel-heading">
								Aperturar Nuevo Mercado
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-2"></div>
									<div class="col-lg-8">								
										<br>
										<p>Para aperturar un nuevo Mercado Corporativo haz click en el siguiente botón:</p><br>
										<a href="apertura/index.php"  type="submit" name="submit" id="submit" class="btn btn-primary">Aperturar Mercado</a>
										<br><br>
									</div>
									<div class="col-lg-2"></div>
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								Listado de Mercados Corporativos
							</div>						

							<div class="panel-body">
								<div class="row">							
									<br>
									<div class="col-lg-1"></div>
									<div class="col-lg-10">
										<div class="dataTable_wrapper">
											<?php if ($cantRS>0){ ?>
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead class="panel-default">
													<tr align="center">
														<td style="vertical-align:middle;">ID</th>
														<td style="vertical-align:middle;">Inicio<br>Solicitud</th>
														<td style="vertical-align:middle;">Fin<br>Solicitud</th>
														<td style="vertical-align:middle;">Día del<br>Mercado</th>
														<!--<td style="vertical-align:middle;">Empresa</th>-->
														<td style="vertical-align:middle;">Cant.<br> Prod.</th>
														<td style="vertical-align:middle;">Activo</th>
														<td style="vertical-align:middle;">Ver</th>
														<td style="vertical-align:middle;">Eliminar</th>														
														<!--<td>BORRAR</th>-->
													</tr>
												</thead>
												<tbody>
													<?php
														for($i=0; $i<$cantRS; $i++){
														$NU_IdMercado 		= $objConexion->obtenerElemento($RS,$i,'NU_IdMercado');
														$AF_RazonSocial		= $objConexion->obtenerElemento($RS,$i,'AF_RazonSocial');			
														$FE_FechaMercado 	= $objConexion->obtenerElemento($RS,$i,'FE_FechaMercado');			
														$FE_Inicio 			= $objConexion->obtenerElemento($RS,$i,'FE_Inicio');
														$FE_Fin 			= $objConexion->obtenerElemento($RS,$i,'FE_Fin');			
														$producto 			= $objConexion->obtenerElemento($RS,$i,'producto');
														$NU_Activo 			= $objConexion->obtenerElemento($RS,$i,'NU_Activo');
														////////////// ACTUALIZAR MERCADOS ACTIVOS Y DESACTIVADOS /////////
														$objMercado->actualizarActivo($objConexion,$NU_IdMercado,$FE_Inicio,$FE_Fin);	
													?>
													<tr align="center">
														<td style="vertical-align:middle;"><?php echo $NU_IdMercado; ?></td>
														<td style="vertical-align:middle;"><?php echo setFechaNoSQL($FE_Inicio); ?></td>
														<td style="vertical-align:middle;"><?php echo setFechaNoSQL($FE_Fin); ?></td>
														<td style="vertical-align:middle;"><?php echo setFechaNoSQL($FE_FechaMercado); ?></td>
														<!--<td style="vertical-align:middle;"><?php echo $AF_RazonSocial; ?></td>-->
														<td style="vertical-align:middle;"><?php echo $producto; ?></td>
														<td style="vertical-align:middle;"><?php if ($NU_Activo==1){ echo 'Si'; }else{ echo 'No'; } ?></td>
														<td style="vertical-align:middle;"><a href="apertura/edit.php?NU_IdMercado=<?php echo $NU_IdMercado; ?>" target="_blank"><span class="glyphicon glyphicon-edit"></span></a></td>
														<td style="vertical-align:middle;">
															<a href="index.php?mensaje=No tiene Autorización para eliminar el Mercado !!">
															<span class="glyphicon glyphicon-trash"></span></a>
														</td>
													</tr>
													<?php
														}
													?>          
												</tbody>
												<?php
													}else{ echo '<br><tr align="center"><td><strong class="rojo">No se encontraron Registros.</strong></td></tr><br>'; }
												?>
											</table>
										</div>	
										<br>
									</div>
									<div class="col-lg-1"></div>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>	
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
