<?php 
	require_once('../../../controller/sessionController.php'); 
	require_once('../../../model/empresaModel.php'); 
	require_once('../../../model/productoModel.php'); 	

	$objEmpresa 	= new Empresa();
	$objProducto 	= new Producto();	
	
	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}	
	
?>

<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Christian Goncalves">

    <title>Mercado Corporativo</title>
    <!-- Favicon ICO -->
	<link rel="shortcut icon" href="../../../images/favicon.ico"/>
	
	<!-- Bootstrap Core CSS -->
    <link href="../../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../css/shop-homepage1.css" rel="stylesheet">
    <link href="../../../css/jquery-ui.css" rel="stylesheet">
	

    <!-- Custom JavaScript -->
	<script type="text/javascript" src="../../../js/jquery.js"></script>
	<script type="text/javascript" src="../../../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../../../js/funciones.js"></script>

<body>
<div id="dialog"></div>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1"></div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../../../img/head.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1"></div>			
			
		</div>
	</div>
	<br>
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../../../img/head3.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
	</div>

	<br>
    <!-- Page Content -->
    <div class="container">

        <div class="row">
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
			<div class="col-sm-10 col-lg-10 col-md-10">			
			
				<nav class="navbar navbar-default" role="navigation" style="text-transform:uppercase; border-color:#E7E7E7">
					<!-- El logotipo y el icono que despliega el menú se agrupan
					para mostrarlos mejor en los dispositivos móviles -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Desplegar navegación</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" style="padding:7px" href="../../index.php"><img class="img-responsive" src="../../../images/inicio.jpg" alt=""></a>
					</div>
					<!-- Agrupar los enlaces de navegación, los formularios y cualquier
					otro elemento que se pueda ocultar al minimizar la barra -->
  				<div class="collapse navbar-collapse navbar-ex1-collapse">
    				<ul class="nav navbar-nav">
						<li><a href="../../index.php">INICIO</a></li>
					<?php if ($BI_ASGestionMercado==1){  ?>
						<li class="dropdown">
							<li class="active">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									GESTIONAR MERCADOS <b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li><a href="index.php">APERTURAR MERCADO</a></li>
									<li class="divider"></li>
									<li><a href="../index.php">CONSULTAR MERCADOS</a></li>
								</ul>
							</li>
						</li>
					<?php } ?>
      					<li><a href="../../index.php?mensaje=En Construcción!!">GESTIONAR PERSONAL</a></li>
					<?php if ($BI_ASParametrosSis==1){  ?>
      					<li class="dropdown">
        					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
          						PARAMETROS DEL SISTEMA <b class="caret"></b>
        					</a>
        					<ul class="dropdown-menu">
          						<li><a href="#">Agregar Usuario</a></li>
								<li class="divider"></li>
								<li><a href="../../configuracion/usuario/index.php">Consultar Usuario</a></li>
        					</ul>
      					</li>					
						
					<?php } ?> 
      					<li class="dropdown">
        					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
          						PERFIL DE USUARIO <b class="caret"></b>
        					</a>
        					<ul class="dropdown-menu">
          						<li><a href="../../perfil/index.php">Ver Perfil</a></li>
          						<li class="divider"></li>
          						<li><a href="../../salirView.php">Cerrar Sesión</a></li>
        					</ul>
      					</li>
    				</ul>
  				</div>
				</nav>		
			</div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>				
		</div>
		
		<div class="row">
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
			<div class="col-sm-5 col-lg-5 col-md-5">
				
				<p style="margin-top:17px">
  					Bienvenido(a), <a href="usuario/perfil/index.php" class="navbar-link"><?php echo $_SESSION['AL_NombreApellido']; ?></a>
				</p>
			</div>
			<div class="col-sm-5 col-lg-5 col-md-5"></div>
		
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
		</div>
		
		<div class="row">
			
			<div class="col-sm-1 col-lg-1 col-md-1"></div>		
			<div class="col-sm-10 col-lg-10 col-md-10">
				<div class="panel panel-primary">
					<div class="panel-body" style="padding:0px">
					<div class="col-sm-12 col-lg-12 col-md-12">
					<br>
					<div class="panel panel-primary">
					<div class="panel-body" style="padding:3px" align="center">

					  <!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#comprar" aria-controls="comprar" role="tab" data-toggle="tab">Apertura de Mercado</a></li>
						</ul>
						
						<div class="col-sm-12 col-lg-12 col-md-12">
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="comprar">
								<br><br>
								<div class="BlancoGris" align="left">
									Llene el formulario para Aperturar un Mercado Virtual para una Empresa
								</div>
								<form role="form" id="form" method="POST" action="../../../controller/mercadoController.php">									
								
									<div class="form-group" align="left">
										<div class="col-sm-1 col-lg-1 col-md-1"></div>
										<div class="col-sm-4 col-lg-4 col-md-4">
											<label for="empresa">Destinado a la Empresa:</label><br>
											<select class="form-control input-sm"  name="empresa_NU_IdEmpresa" required id="empresa_NU_IdEmpresa">
												<option selected="selected">[ Seleccione ]</option>
												<?php 
												$rsEmpresa=$objEmpresa->listarEmpresa($objConexion);
												for($i=0;$i<$objConexion->cantidadRegistros($rsEmpresa);$i++){
													$value=$objConexion->obtenerElemento($rsEmpresa,$i,"NU_IdEmpresa");
													$des=$objConexion->obtenerElemento($rsEmpresa,$i,"AF_RazonSocial");
													$selected="";
													echo "<option value=".$value." ".$selected.">".$des."</option>";
												}  
												?>
											</select>
										</div>
										<div class="col-sm-4 col-lg-4 col-md-4">
											<label for="FE_Inicio">Solicitudes del Mercado Inician:</label><br>
											<input type="text" name="FE_Inicio" id="FE_Inicio" class="form-control input-sm">
										</div>
										<div class="col-sm-1 col-lg-1 col-md-1"></div>
									</div>
									
									<div class="col-sm-12 col-lg-12 col-md-12">&nbsp;</div>
									
									<div class="form-group" align="left">	
										<div class="col-sm-1 col-lg-1 col-md-1"></div>
										<div class="col-sm-4 col-lg-4 col-md-4">
											<label for="FE_Inicio">Realización del Mercado:</label><br>
											<input type="text" name="FE_FechaNac1" id="FE_FechaNac1" class="form-control input-sm">
										</div>
										<div class="col-sm-4 col-lg-4 col-md-4">
											<label for="FE_Inicio">Solicitudes del Mercado Finalizan:</label><br>
											<input type="text" name="FE_Fin" id="FE_Fin" class="form-control input-sm">
										</div>
										<div class="col-sm-1 col-lg-1 col-md-1"></div>
										
									</div>
									
									<div class="col-sm-12 col-lg-12 col-md-12" style="padding:0px">
										<br><br>
										<div class="BlancoGris" align="left">
											Seleccione los productos que estaran disponibles en este Mercado Virtual :
										</div>
								
										<?php 
										$k=0;
										$rsProducto		= $objProducto->listarProducto($objConexion);
										$cantProducto	= $objConexion->cantidadRegistros($rsProducto);
										for($i=0;$i<$cantProducto;$i++){
										$NU_IdProducto		= $objConexion->obtenerElemento($rsProducto,$i,"NU_IdProducto");
										$AF_Foto			= $objConexion->obtenerElemento($rsProducto,$i,"AF_Foto");
										$AF_NombreProducto	= $objConexion->obtenerElemento($rsProducto,$i,"AF_NombreProducto");
										$AL_Medida			= $objConexion->obtenerElemento($rsProducto,$i,"AL_Medida");
										$NU_Contenido		= $objConexion->obtenerElemento($rsProducto,$i,"NU_Contenido");
										$BS_PrecioUnitario	= $objConexion->obtenerElemento($rsProducto,$i,"BS_PrecioUnitario");
										if ($k==0){ echo "<div class='col-sm-3 col-lg-3 col-md-3'>"; }
										?>

										<br><br>
										<img src="../../../images/producto/<?php if ($AF_Foto==''){ echo 'sin_imagen.jpg'; }else{ echo $AF_Foto; } ?>" width="124" height="85"  alt="" style="border:solid 1px #CFCFCF"/><br>
										<input type="checkbox" name="<?php echo 'chk'.$i; ?>" id="<?php echo 'chk'.$i; ?>" value="<?php echo $NU_IdProducto; ?>"><br>
								
										<?php echo $AF_NombreProducto; ?></b><br><?php echo $NU_Contenido.' '.$AL_Medida.' = '.number_format($BS_PrecioUnitario,2,',','.').' BsF.';?>
										<?php $k++; ?>
										<?php 
										if ($k!=0){ echo '</div>'; $k=0; }
										}  ?> 
									</div>
									<div class="col-sm-12 col-lg-12 col-md-12">
										<hr>
										<div class="col-sm-9 col-lg-9 col-md-9" align="right" style="padding:0px">						
											<input name="Comprar" type="submit" class="btn btn-primary btn-sm"  value="Aperturar Mercado" id="Comprar">
										</div>
										<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
										<div class="col-sm-1 col-lg-1 col-md-1" align="right" style="padding:0px">
											<input name="button2" type="button" class="btn btn-primary btn-sm" id="button2" value="Cancelar" onClick="javascript:window.location='../../index.php'" />
											<br><br><br>
										</div>
										<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
										<input name="origen" type="hidden" id="origen" value="apertura_mercado">
										<input name="cantProducto" type="hidden" id="cantProducto" value="<?php echo $cantProducto; ?>">		
									</div>
								</form>
							</div>
						</div>
						</div>
					</div>
					</div>
					</div>
					</div>
				</div>
			</div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
		</div>
		
		<!-- /.container -->

        <!-- Footer -->
		<div class="row">
			<footer>
				<div class="col-sm-1 col-lg-1 col-md-1"></div>
				<div class="col-sm-10 col-lg-10 col-md-10">
					<div class="panel panel-primary">
						<div class="panel-body">
							<p align="center">
								<strong>
									Venezolana de Alimentos La Casa - &copy;2015<br>
									Oficina de Tecnología de la Información
								</strong>
							</p>
						</div>
					</div>
				</div>			
				<div class="col-sm-1 col-lg-1 col-md-1"></div>			
			</footer>
		</div>
	</div>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../js/bootstrap.min.js"></script>

</body>
</html>