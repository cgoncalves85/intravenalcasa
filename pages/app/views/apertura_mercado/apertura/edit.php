﻿<?php
error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
ini_set('display_errors', '1');
?>
<?php 
	require_once('../../../controller/sessionController.php'); 
	require_once('../../../model/empresaModel.php'); 
	require_once('../../../model/productoModel.php'); 	
	require_once('../../../model/mercadoModel.php'); 	
	
	$objMercado 	= new Mercado();
	$objEmpresa 	= new Empresa();
	$objProducto 	= new Producto();	

	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}	
	
	$RSMercado = $objMercado->buscar($objConexion,$_GET['NU_IdMercado']);
	$cRSMercado = mysql_num_rows($RSMercado);
	
	if($cRSMercado>0){
			$FE_Inicio = mysql_result($RSMercado,0,'FE_Inicio');
	
	}
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Apertura de Mercado</h3>
						<form role="form" id="form" method="POST" action="../../../controller/mercadoController.php">	
							<div class="panel panel-default">
								<div class="panel-heading">
									Datos Generales &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Llene el formulario para Aperturar un Mercado Virtual para una Empresa.
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-1"></div>
										<div class="col-lg-11">	
											<br>
											<div class="form-group" align="left">
												<div class="col-lg-5">
													<label for="empresa">Destinado a la Empresa:</label><br>
													<select class="form-control"  name="empresa_NU_IdEmpresa" required id="empresa_NU_IdEmpresa">
														<option selected="selected">[ Seleccione ]</option>
														<?php 
														$rsEmpresa=$objEmpresa->listarEmpresa($objConexion);
														for($i=0;$i<$objConexion->cantidadRegistros($rsEmpresa);$i++){
															$value=$objConexion->obtenerElemento($rsEmpresa,$i,"NU_IdEmpresa");
															$des=$objConexion->obtenerElemento($rsEmpresa,$i,"AF_RazonSocial");
															$selected="";
															echo "<option value=".$value." ".$selected.">".$des."</option>";
														}  
														?>
													</select>
												</div>
												<div class="col-lg-4">
													<label for="FE_Inicio">Solicitudes del Mercado Inician:</label><br>
													<input type="text" name="FE_Inicio" id="FE_Inicio" class="form-control" value="<?php echo $FE_Inicio; ?>">
												</div>
												<div class="col-lg-3"></div>
											</div>
									
											<div class="col-lg-12">&nbsp;</div>
									
											<div class="form-group" align="left">	
										
												<div class="col-lg-5">
													<label for="FE_Inicio">Realización del Mercado:</label><br>
													<input type="text" name="FE_FechaNac1" id="FE_FechaNac1" class="form-control">
												</div>
												<div class="col-lg-4">
													<label for="FE_Inicio">Solicitudes del Mercado Finalizan:</label><br>
													<input type="text" name="FE_Fin" id="FE_Fin" class="form-control">
													<br>
												</div>
												<div class="col-lg-3"></div>
												
											</div>
											
										</div>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									Productos Disponibles &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Seleccione los productos que estarán disponibles en este Mercado Virtual.
								</div>
								<div class="panel-body">
									<div class="row">
									
										<div class="col-lg-12" align="center">								
											<br>

											<?php 
											$k=0;
											$rsProducto		= $objProducto->listarProducto($objConexion);
											$cantProducto	= $objConexion->cantidadRegistros($rsProducto);
											for($i=0;$i<$cantProducto;$i++){
												  $NU_IdProducto		= $objConexion->obtenerElemento($rsProducto,$i,"NU_IdProducto");						
												  $AF_NombreProducto	= $objConexion->obtenerElemento($rsProducto,$i,"AF_NombreProducto");
												  $AL_Medida			= $objConexion->obtenerElemento($rsProducto,$i,"AL_Medida");
												  $AF_Foto				= $objConexion->obtenerElemento($rsProducto,$i,"AF_Foto");
												  $NU_Contenido			= $objConexion->obtenerElemento($rsProducto,$i,"NU_Contenido");
												  $BS_PrecioUnitario	= $objConexion->obtenerElemento($rsProducto,$i,"BS_PrecioUnitario");
											if ($k==0){ echo "<div class='col-lg-3'>"; }
											?>
											<br><br>
											<img src="../../../images/producto/<?php if ($AF_Foto==''){ echo 'sin_imagen.jpg'; }else{ echo $AF_Foto; } ?>"   width="104" height="85"  alt="" style="border:solid 1px #AF312F"/><br>
											<input type="checkbox" name="<?php echo 'chk'.$i; ?>" id="<?php echo 'chk'.$i; ?>" value="<?php echo 'chk'.$i; ?>"><br>
								
											<?php echo $AF_NombreProducto; ?></b><br><?php echo $NU_Contenido.' '.$AL_Medida.' = '.number_format($BS_PrecioUnitario,2,',','.').' BsF.';?>
											<?php $k++; ?>
											<?php 
											if ($k!=0){ echo '</div>'; $k=0; }
											}  ?> 
										</div>
										<input name="origen" type="hidden" id="origen" value="editar">
										<input name="cantProducto" type="hidden" id="cantProducto" value="<?php echo $cantProducto; ?>">
										<div class="col-lg-12">
											<br><hr><br>
											<div class="col-lg-9" align="right" style="padding:0px">						
												<input name="Comprar" type="submit" class="btn btn-primary"  value="Guardar Cambios" id="Comprar">
											</div>
											<div class="col-lg-1">&nbsp;</div>
											<div class="col-lg-1" align="right" style="padding:0px">
												<input name="button2" type="button" class="btn btn-primary" id="button2" value="Cancelar" onClick="javascript:window.location='../../index.php'" />
												<br><br>
											</div>
											<div class="col-lg-1">&nbsp;</div>		
										</div>						
									
									</div>

								</div>
							</div>
						</form>	
					</div>
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../dist/js/sb-admin-2.js"></script>
	
	<!-- Custom JavaScript -->
	<script type="text/javascript" src="../../../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../../../js/funciones.js"></script>

</body>

</html>
