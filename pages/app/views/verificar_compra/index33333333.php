﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/pedidoModel.php');
	require_once('../../model/usuarioModel.php');

	$objPedido 			= new Pedido();
	$objUsuario			= new Usuario();

	$mercado_NU_IdMercado	= $_REQUEST['mercado_NU_IdMercado'];
	$RSPedido				= $objPedido->listarPedidosXmercado($objConexion,$mercado_NU_IdMercado);
	$cantRSPedido			= $objConexion->cantidadRegistros($RSPedido);

		///////////// CONVERTIR DECIMALES A ESPANOL ///////////
		function setDecimalEsp($numero){
			$numero = str_replace(".", ",", $numero);
			return $numero;
		}

	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>
	
    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	

	
</head>

<body onLoad="abrir_dialog();">
<div id="dialog" title="Mensaje" style="display:none;">
    <p><?php echo $_GET['mensaje']; ?></p>
</div>

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Verificar Compras</h4>
						<div class="panel panel-default">
							<div class="panel-heading">
								Verificación de Ordenes de Compra &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Usuarios que SI compraron.
							</div>
							<div class="panel-body" style="padding:0px">
								<div class="row">
								
									<div class="dataTable_wrapper" align="center" style="margin:50px">
						
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr >
												  <td style="vertical-align:middle;" >N°</td>
												  <td style="vertical-align:middle;" >O.C</th>
												  <td style="vertical-align:middle;" >Ubicación</th>
												  <td style="vertical-align:middle;" >C.I</th>
												  <td style="vertical-align:middle;" >Nombres y Apellidos</th>
												  <td style="vertical-align:middle;" >N° Prod.</th>
												  <td style="vertical-align:middle;" >Monto a Pagar</th>
												  <td style="vertical-align:middle;" >Ver O/C</th>
												</tr>
											</thead>
     
											<tbody>
												<?php
													for($i=0; $i<$cantRSPedido; $i++){
														$NU_IdPedido 		= $objConexion->obtenerElemento($RSPedido,$i,'NU_IdPedido');
														$AF_CodPedido 		= $objConexion->obtenerElemento($RSPedido,$i,'AF_CodPedido');
														$AL_NombreSede		= $objConexion->obtenerElemento($RSPedido,$i,'AL_NombreSede');			
														$AL_NombreGerencia	= $objConexion->obtenerElemento($RSPedido,$i,'AL_NombreGerencia');						
														$NU_Cedula			= $objConexion->obtenerElemento($RSPedido,$i,'NU_Cedula');			
														$AL_Nombre 			= $objConexion->obtenerElemento($RSPedido,$i,'AL_Nombre');
														$AL_Apellido 		= $objConexion->obtenerElemento($RSPedido,$i,'AL_Apellido');			
														$CantProductos 		= $objConexion->obtenerElemento($RSPedido,$i,'CantProductos');
														$MontoPagar 		= $objConexion->obtenerElemento($RSPedido,$i,'MontoPagar');
														$AF_RazonSocial		= $objConexion->obtenerElemento($RSPedido,$i,'AF_RazonSocial');	
												?>
													<tr align="center">
													  <td style="vertical-align:middle;" ><?php echo $i+1; ?></td>
													  <td style="vertical-align:middle;" ><?php echo $AF_CodPedido; ?></td>

													  <td style="vertical-align:middle;" ><?php echo $AL_NombreSede.", ".$AL_NombreGerencia; ?></td>
													  <td style="vertical-align:middle;" ><?php echo $NU_Cedula; ?></td>
													  <td style="vertical-align:middle;" ><?php echo $AL_Nombre.' '.$AL_Apellido; ?></td>
													  <td style="vertical-align:middle;" ><?php echo $CantProductos; ?></td>
													  <td style="vertical-align:middle;" ><?php echo number_format($MontoPagar,2,',','.').' BsF.'; ?></td>
													  <td style="vertical-align:middle;" >
														<a href="../pedido/orden_compra.php?pedido_NU_IdPedido=<?php echo $NU_IdPedido; ?>" target="_blank">
														  <span class="glyphicon glyphicon-search" title="Ver Orden de Compra"></span></a>
													  </td>
													</tr>
												<?php
													}
												?>          
											</tbody>
										</table>
									</div>
									
									<br>
								</div>
							</div>
						</div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->

	<script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    

    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
	
</body>

</html>
