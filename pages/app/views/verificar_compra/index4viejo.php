<?php 
require_once('../../controller/sessionController.php'); 
require_once('../../model/pedidoModel.php');
require_once('../../model/usuarioModel.php');

$objPedido 			= new Pedido();
$objUsuario			= new Usuario();

$mercado_NU_IdMercado = $_REQUEST['mercado_NU_IdMercado'];
$RSPedido		= $objPedido->listarPedidosXNOmercado($objConexion,$mercado_NU_IdMercado);
$cantRSPedido	= $objConexion->cantidadRegistros($RSPedido);

	///////////// CONVERTIR DECIMALES A ESPANOL ///////////
	function setDecimalEsp($numero){
		$numero = str_replace(".", ",", $numero);
		return $numero;
	}
	

?>
<!doctype html>
<html>
<link rel="stylesheet" type="text/css" href="../../css/estilo.css">
<head>
<title>Mercado Corporativo de VENALCASA</title>

<script src="../../js/jquery.js"></script>
<script src="../../js/jquery.dataTables.js"></script>
<script src="../../js/dataTables.tableTools.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="../../css/dataTables.tableTools.css">
<link rel="stylesheet" href="../../css/jquery-ui.css" />
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "../../video/copy_csv_xls_pdf.swf",			
            "aButtons": [
                "copy",
                "print",
                {
                    "sExtends":    "collection",
                    "sButtonText": "Guardar Como",
                    "aButtons":    [ "csv", "xls", "pdf" ]
                }
            ]
        }
    } );
} );


</script>
</head>

<body>
  <table class="Textonegro" width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td height="25" align="left" bgcolor="#CCCCCC" class="Negrita">&nbsp;&nbsp;&nbsp; ORDENES DE COMPRA (Empleados que NO Compraron)</td>
    </tr>
    <tr>
      <td><img src="../../images/blank.gif" width="20" height="5"></td>
    </tr>

    <tr>
      <td height="25" align="center"><table width="95%" border="0" cellspacing="0" cellpadding="2">
        <tr>
          <td class="BlancoGris">&nbsp; Verifique la información de las ordenes de compra</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td height="25">&nbsp;</td>
    </tr>
    <tr>
      <td height="25">
      
      
      <table width="95%" align="center" id="example" class="display">
      <thead>
        <tr>
          <th width="5" align="center">NRO</th>
		  <th align="center">EMPRESA</th>
          <th align="center">UBICACIÓN</th>
          <th width="75" align="center">CÉDULA</th>
          <th align="center">NOMBRE Y APELLIDO</th>
          <th align="center">TELÉFONO</th>
          </tr>
	  </thead>
      <tfoot>
        <tr>
          <th align="center">NRO</th>
		  <th align="center">EMPRESA</th>
          <th align="center">UBICACIÓN</th>
          <th align="center">CÉDULA</th>
          <th align="center">NOMBRE Y APELLIDO</th>
          <th align="center">TELÉFONO</th>
          </tr>
	  </tfoot>      
      <tbody>
	<?php
    	for($i=0; $i<$cantRSPedido; $i++){
			$AL_NombreSede		= $objConexion->obtenerElemento($RSPedido,$i,'AL_NombreSede');			
			$AL_NombreGerencia	= $objConexion->obtenerElemento($RSPedido,$i,'AL_NombreGerencia');						
			$NU_Cedula			= $objConexion->obtenerElemento($RSPedido,$i,'NU_Cedula');			
			$AL_Nombre 			= $objConexion->obtenerElemento($RSPedido,$i,'AL_Nombre');
			$AL_Apellido 		= $objConexion->obtenerElemento($RSPedido,$i,'AL_Apellido');
			$AF_Telefono 		= $objConexion->obtenerElemento($RSPedido,$i,'AF_Telefono');
			$AF_RazonSocial		= $objConexion->obtenerElemento($RSPedido,$i,'AF_RazonSocial');
    ?>
        <tr>
          <td align="center"><?php echo $i+1; ?></td>
		  <td align="left"  ><?php echo $AF_RazonSocial; ?></td>
          <td align="center"><?php echo $AL_NombreSede.", ".$AL_NombreGerencia; ?></td>
          <td align="center"><?php echo $NU_Cedula; ?></td>
          <td align="left"  ><?php echo $AL_Nombre.' '.$AL_Apellido; ?></td>
          <td align="left"  ><?php echo $AF_Telefono; ?></td>
          </tr>
	<?php
	    }
    ?>          
        </tbody>
      </table>
    
      </td>
    </tr>
    <tr>
      <td height="25" align="center">&nbsp;</td>
    </tr>
    <tr>
      <td height="25" align="center">
      <input name="button" type="button" class="BotonRojo" id="button" value="[ Atrás ]" onClick="javascript:window.location='index2.php?mercado_NU_IdMercado=<?php echo $mercado_NU_IdMercado; ?>'" /></td>
    </tr>
  </table>

</body>
</html>