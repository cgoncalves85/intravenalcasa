﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/empresaModel.php'); 
	require_once('../../model/productoModel.php'); 	
	require_once('../../model/medidaModel.php'); 	

	$objEmpresa 	= new Empresa();
	$objProducto 	= new Producto();	
	$objMedida 	= new Medida();
	
	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}	

	$RS	 = $objMedida->listarMedida($objConexion);
	$cant = $objConexion->CantidadRegistros($RS);

	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Productos</h3>
						<form class="form-horizontal" role="form" id="form" method="POST" action="../../controller/productoController.php">
							<div class="panel panel-default">
								<div class="panel-heading">
									Configuración &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Producto.
								</div>
								<div class="panel-body">
									<div class="row">
									
										<div class="col-lg-12">	
											<br>
											
											<div class="form-group">
												<div class="col-lg-4">
													<div class="col-lg-3" style="padding:0px">
														<label class="control-label" for="AF_NombreProducto">Producto :</label>
													</div>
													<div class="col-lg-9" style="padding:0px">
														<input type="text" name="AF_NombreProducto" required id="AF_NombreProducto" class="form-control">
													</div>
												</div>
												<div class="col-lg-2" style="padding:0px">
													<div class="col-lg-4" style="padding:0px">
														<label class="control-label" for="medida_NU_IdMedida">U.Med:</label>
													</div>
													<div class="col-lg-6" style="padding:0px">
														<select name="medida_NU_IdMedida" required id="medida_NU_IdMedida" class="form-control placeholder">
															<option value=""></option>
															<?php
																$RS	 = $objMedida->listarMedida($objConexion);
																$cant = $objConexion->CantidadRegistros($RS);																								
																for($i=0; $i<$cant; $i++){
																	$value	= $objConexion->obtenerElemento($RS,$i,"NU_IdMedida");
																	$des	= $objConexion->obtenerElemento($RS,$i,"AL_Medida");
																	$selected	= "";
																	
																	echo "<option value=".$value." ".$selected.">".$des."</option>";
																}																																	
															?>				
														</select>
													</div>
												</div>
												<div class="col-lg-2" style="padding:0px">
													<div class="col-lg-3" style="padding:0px">
														<label class="control-label" for="NU_Max">Max:</label>
													</div>
													<div class="col-lg-5" style="padding:0px">
														<select name="NU_Max" required id="NU_Max" class="form-control placeholder">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
														</select>
													</div>
										
												</div>	
												<div class="col-lg-2" style="padding:0px">
													<div class="col-lg-3" style="padding:0px">
														<label class="control-label" for="NU_Min">Min:</label>
													</div>
													<div class="col-lg-5" style="padding:0px">
														<select name="NU_Min" required id="NU_Min" class="form-control placeholder">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
														</select>
													</div>
												</div>	
												<div class="col-lg-2" style="padding:0px">
													<div class="col-lg-4" style="padding:0px">
														<label class="control-label" for="NU_Salto">Salto:</label>
													</div>
													<div class="col-lg-6" style="padding:0px">
														<select name="NU_Salto" required id="NU_Salto" class="form-control placeholder">
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
														</select>
													</div>
												</div>
												
												<div class="col-lg-12"><br><br></div>
												
												<div class="col-lg-4">
													<div class="col-lg-3" style="padding:0px">
														<label class="control-label" for="BS_PrecioUnitario">Precio :</label>
													</div>
													<div class="col-lg-9" style="padding:0px">
														<input type="text" name="BS_PrecioUnitario" required id="BS_PrecioUnitario" class="form-control">
													</div>
												</div>
												<div class="col-lg-2" style="padding:0px">
													<div class="col-lg-5" style="padding:0px">
														<label class="control-label" for="NU_Contenido">Cont. :</label>
													</div>
													<div class="col-lg-5" style="padding:0px">
														<input type="text" name="NU_Contenido" required id="NU_Contenido" class="form-control">
													</div>
												</div>
												<div class="col-lg-4" style="padding:0px">
													<div class="col-lg-6" style="padding:0px">
														<label class="control-label" for="NU_BultoCaja">Cant. x Bulto/Caja :</label>
													</div>
													<div class="col-lg-4" style="padding:0px">
														<input type="text" name="NU_BultoCaja" required id="NU_BultoCaja" class="form-control">
													</div>
												</div>													
												<div class="col-lg-2" style="padding:0px">
													<div class="col-lg-4" style="padding:0px">
														<label class="control-label" for="NU_IdMedidaBulto">U.Med:</label>
													</div>
													<div class="col-lg-6" style="padding:0px">
														<select name="NU_IdMedidaBulto" required id="NU_IdMedidaBulto" class="form-control placeholder">
															<option value=""></option>
															<?php
																$RS	 = $objMedida->listarMedida($objConexion);
																$cant = $objConexion->CantidadRegistros($RS);																								
																for($i=0; $i<$cant; $i++){
																	$value	= $objConexion->obtenerElemento($RS,$i,"NU_IdMedida");
																	$des	= $objConexion->obtenerElemento($RS,$i,"AL_Medida");
																	$selected	= "";
																	
																	echo "<option value=".$value." ".$selected.">".$des."</option>";
																}																																	
															?>				
														</select>
													</div>
												</div>

												<div class="col-lg-12"><br><br></div>

												<div class="col-lg-8">
													<div class="col-lg-3" style="padding:0px">
														<label class="control-label" for="AF_Foto">Imágen del Producto :</label>
													</div>
													<div class="col-lg-7" style="padding:0px">
														<input type="file" name="AF_Foto" id="AF_Foto" class="form-control">
														<p class="help-block">Adjunte una Imágen referencial del producto.</p>
													</div>
												</div>													
												
												
												<div class="col-lg-4" align="right" style="padding-right:30px">
													<?php 
														$k = 0;
														$rsProducto		= $objProducto->listarProductos($objConexion);
														$cantProducto	= $objConexion->cantidadRegistros($rsProducto);
													?> 
													<br>
													<input name="origen" type="hidden" id="origen" value="agregar_producto">
													<input name="cantProducto" type="hidden" id="cantProducto" value="<?php echo $cantProducto; ?>">
													<input name="Agregar_Producto" type="submit" class="btn btn-primary"  value="Agregar Producto" id="Agregar_Producto">
												</div>
								
												
												
											</div>
										
										</div>
									</div>
								</div>
							</div>
						</form>

						<div class="panel panel-default">
							<div class="panel-heading">
								Configuración &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; Listado de Productos.
							</div>						

							<div class="panel-body">
								<div class="row">	

									<div class="dataTable_wrapper" align="center" style="margin:50px">
						
										<table class="table table-striped table-bordered table-hover" id="dataTables-example">
											<thead>
												<tr>
													<td style="vertical-align:middle;" ><strong>ID</strong></td>
													<td style="vertical-align:middle;" ><strong>Nombre</strong></td>
													<td style="vertical-align:middle;" ><strong>Precio</strong></td>
													<td style="vertical-align:middle;" ><strong>N° Max</strong></td>	
													<td style="vertical-align:middle;" ><strong>N° Min</strong></td>
													<td style="vertical-align:middle;" ><strong>N° Salto</strong></td>
													<td style="vertical-align:middle;" ><strong>Contenido</strong></td>
													<td style="vertical-align:middle;" ><strong>Editar</strong></td>
													<td style="vertical-align:middle;" ><strong>Eliminar</strong></td>
												</tr>
											</thead>
     
											<tbody>
												<?php
													for($i=0; $i<$cantProducto; $i++){
													$NU_IdProducto		= $objConexion->obtenerElemento($rsProducto,$i,"NU_IdProducto");
													$AF_NombreProducto	= $objConexion->obtenerElemento($rsProducto,$i,"AF_NombreProducto");
													$NU_Max				= $objConexion->obtenerElemento($rsProducto,$i,"NU_Max");
													$NU_Min				= $objConexion->obtenerElemento($rsProducto,$i,"NU_Min");
													$NU_Salto			= $objConexion->obtenerElemento($rsProducto,$i,"NU_Salto");
													$AL_Medida			= $objConexion->obtenerElemento($rsProducto,$i,"AL_Medida");
													$NU_Contenido		= $objConexion->obtenerElemento($rsProducto,$i,"NU_Contenido");
													$BS_PrecioUnitario	= $objConexion->obtenerElemento($rsProducto,$i,"BS_PrecioUnitario");
												?>	
												
													<tr align="center">
														<td style="vertical-align:middle;" ><strong><?php echo $NU_IdProducto ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $AF_NombreProducto ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $BS_PrecioUnitario ?></strong></td>
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Max ?></strong></td>	
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Min ?></strong></td>	
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Salto ?></strong></td>	
														<td style="vertical-align:middle;" ><strong><?php echo $NU_Contenido.' '.$AL_Medida ?></strong></td>	
														<td style="vertical-align:middle;" ><a href="edit.php"><span class="glyphicon glyphicon-edit" title="Editar Unidad de Medida"></span></a></td>
														<td style="vertical-align:middle;" ><a href="delete.php"><span class="glyphicon glyphicon-trash" title="Eliminar Unidad de Medida"></span></a></td>
													</tr>
												<?php
													}
												?>          
											</tbody>
										</table>
									</div>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>	
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
