﻿<?php
error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
ini_set('display_errors', '1');
?>
<?php
/*
$path = getcwd();
echo "La ruta absoluta es: ";
echo $path;
die();
*/
?>
<?php 
	require_once('../controller/sessionController.php'); 

	
	$RSUsuario = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
	}
	
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../css/jquery-ui.css" rel="stylesheet">
	
	<!-- Clippy CSS -->
	<link rel="stylesheet" type="text/css" href="../clippy/build/clippy.css" media="all">
	<script src="../js/jquery-1.11.3.min.js"></script>
	<script src="../clippy/build/clippy.min.js"></script>
	

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	
	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
			$( "#dialog" ).dialog({
				show: "blind",
				hide: "explode",
				modal: true,
				buttons: {
					Aceptar: function() {
					$( this ).dialog( "close" );
					}
				}
			});
			}
		};
	</script>	
	
	<!-- Este Script es para activar el Ayudante Office, si descomenta este
		código aparecera Merlin(Ayudante de Office)
		
	<script type="text/javascript">
		   
		clippy.load('F1', function(agent){    
			agent.show();
			agent.speak('Hola a todos, Bienvenidos a IntraVenalcasa');
			
			agent.gestureAt(400, 400);
			agent.play ('SendMail');
			agent.play ('GetAttention');
			agent.play ('Searching');
			agent.animations("MoveLeft", "Congratulate", "Hide", "Pleased", "Acknowledge");
		});
	</script>
	-->

</head>

<body onLoad="abrir_dialog();">
	
	<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
		<p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
	</div>

    <div id="wrapper">

        <!-- Navigation -->
		
		<?php include "nav.php"; ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Página en Blanco</h3>
                </div>
                <div class="col-lg-12">
                    <h5>La Página no está disponible en este momento !!</h5>
                </div>				
				
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Custom JavaScript -->
   <script type="text/javascript" src="../js/jquery-ui.js"></script>
   <script type="text/javascript" src="../js/maximizar.js"></script>
   <script type="text/javascript" src="../js/desconectar.js"></script>	


</body>

</html>
