﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/ctrabajo_tipoModel.php');
	require_once('../../model/ctrabajoModel.php');	
	
	$objCTrabajo_Tipo 	= new CTrabajo_Tipo();
	$objCTrabajo 		= new CTrabajo();
	
	$RS 	= $objCTrabajo->listarConstancias($objConexion,$_SESSION["NU_IdUsuario"]);
	$cantRS = $objConexion->cantidadRegistros($RS);

	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	
	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");	
		$AL_Adscripcion 		= $objConexion->obtenerElemento($RSUsuario,0,"AL_Adscripcion");	
	}		
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <span class="ui-icon ui-icon-circle-check"></span>
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Constancias de Trabajo</h4>

						
						<div class="panel panel-default">
							<div class="panel-heading">
								Generar Constancias de Trabajo
							</div>
							<div class="panel-body">
								<div class="row">
									<form role="form" method="POST" action="../../controller/ctrabajoController.php" target="_blank">
										<div class="col-lg-4"></div>
										<div class="col-lg-4">
											<br>
											<div class="form-group">
												<label for="NU_Cedula">Tipo de Constancia que desea :</label>
												<select name="NU_IdTipoCTrabajo" required id="NU_IdTipoCTrabajo" class="form-control">
													<option selected="selected"></option>
													<?php 
														$RSTipoCTrabajo		= $objCTrabajo_Tipo->listarTipos($objConexion);
														$cantRSTipoCTrabajo = $objConexion->cantidadRegistros($RSTipoCTrabajo);
														for($i=0;$i<$cantRSTipoCTrabajo;$i++){
															$value	= $objConexion->obtenerElemento($RSTipoCTrabajo,$i,"NU_IdTipoCTrabajo");
															$des	= $objConexion->obtenerElemento($RSTipoCTrabajo,$i,"AL_Tipo");
															echo "<option value=".$value.">".$des."</option>";
														}  
													?>
												</select>
											</div>
											<div align="right">
												<button type="submit" name="submit" id="submit" class="btn btn-primary">Generar Constancia</button>
												<input name="origen" type="hidden" id="origen" value="CTrabajo">
											</div>
											<br>
										</div>
										<div class="col-lg-4"></div>
									</form>	
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
								Mis Constancias de Trabajo
							</div>						

							<div class="panel-body">
								<div class="row">							
									<br>
									<div class="col-lg-1"></div>
									<div class="col-lg-10">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr align="center">
														<td style="vertical-align:middle;" ><strong>N°</strong></td>
														<td style="vertical-align:middle;" ><strong>Fecha de Solicitud</strong></td>
														<td style="vertical-align:middle;" ><strong>Tipo de Constancia</strong></td>
														<td style="vertical-align:middle;" ><strong>Ver</strong></td>
														<td style="vertical-align:middle;" ><strong>Eliminar</strong></td>
													</tr>
												</thead>
												<tbody>
													<?php
													$cont = 1;
													if ($cantRS>0){ 
														for($i=0; $i<$cantRS; $i++){
														
														$NU_IdCTrabajo 		= $objConexion->obtenerElemento($RS,$i,'NU_IdCTrabajo');
														$FE_Solicitud 		= $objConexion->obtenerElemento($RS,$i,'FE_Solicitud');
														$NU_IdTipoCTrabajo 	= $objConexion->obtenerElemento($RS,$i,'NU_IdTipoCTrabajo');
														$AL_Tipo			= $objConexion->obtenerElemento($RS,$i,'AL_Tipo');	
														$cont= $cont++;
													?>
													<tr>
														<td align="center"><?php echo $cont++; ?></td>
														<td align="center"><?php echo setFechaNoSQL($FE_Solicitud); ?></td>
														<td align="center"><?php echo $AL_Tipo; ?></td>
														<td align="center"><a href="Constancia_Trabajo<?php echo $NU_IdTipoCTrabajo; ?>.php?NU_IdCTrabajo=<?php echo $NU_IdCTrabajo; ?>" target="_blank"><span class="glyphicon glyphicon-search" title="Ver Constancia"></span></a></td>
														<td align="center"><a href="Constancia_Trabajo<?php echo $NU_IdTipoCTrabajo; ?>.php?NU_IdCTrabajo=<?php echo $NU_IdCTrabajo; ?>"><span class="glyphicon glyphicon-trash" title="Eliminar Constancia"></span></a></td>	
													</tr>
													<?php
														}
													?>          
												</tbody>
												<?php
													}else{ echo '<br><tr align="center"><td><strong class="rojo">No se encontraron Constancias de Trabajo.</strong></td></tr><br>'; }
												?>
											</table>
										</div>	
										<br>
									</div>
									<div class="col-lg-1">&nbsp;</div>
								</div>
								
								<!-- /.row (nested) -->
							</div>
							<!-- /.panel-body -->
						</div>	

                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>	

</body>

</html>
