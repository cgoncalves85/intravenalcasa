﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/ctrabajo_tipoModel.php');
	require_once('../../model/ctrabajoModel.php');	
	
	$objCTrabajo_Tipo 	= new CTrabajo_Tipo();
	$objCTrabajo 		= new CTrabajo();
	
	$RS 	= $objCTrabajo->listarConstancias($objConexion,$_SESSION["NU_IdUsuario"]);
	$cantRS = $objConexion->cantidadRegistros($RS);

	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	//////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <span class="ui-icon ui-icon-circle-check"></span>
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>	

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header">Constancias de Trabajo</h3>
						<div class="col-lg-4 col-md-4"><img src="../../img/nodisponible.jpg"></img></div>
						<div class="col-lg-7 col-md-7">
							<div class="alert alert-warning" style="margin-top:30px; font-size:16px">
								<strong>Disculpe, en estos momentos esta página no se encuentra disponible !!.</strong>
							</div>
						</div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>	

</body>

</html>
