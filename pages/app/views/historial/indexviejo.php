<?php 
require_once('../../controller/sessionController.php'); 

$objUsuario = new Usuario();

$RS 	= $objUsuario->listar($objConexion);
$cantRS = $objConexion->cantidadRegistros($RS);

	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	//////////////////////////////////////////////////////////////
	
?>
<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Christian Goncalves">

    <title>Mercado Corporativo</title>

    <!-- Favicon ICO -->
	<link rel="shortcut icon" href="../../images/favicon.ico"/>
	
	<!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/shop-homepage1.css" rel="stylesheet">
    <link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom JavaScript -->
   <script type="text/javascript" src="../../js/jquery.js"></script>
   <script type="text/javascript" src="../../js/jquery-ui.js"></script>
   <script type="text/javascript" src="../../js/maximizar.js"></script>
   <script type="text/javascript" src="../../js/desconectar.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.1/respond.min.js"></script>
    <![endif]-->


<script type="text/javascript">
    function abrir_dialog() {
		  $( "#dialog" ).dialog({
			  show: "blind",
			  hide: "explode",
			  modal: true,
			  buttons: {
				Aceptar: function() {
				  $( this ).dialog( "close" );
				}
			  }
		  });
    };
</script>
	
</head>

<body>
<div id="dialog" title="Atención !!" style="display:none;">
    <p>En Construccion!!.</p>
</div>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1"></div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../../img/head.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1"></div>			
			
		</div>
	</div>
	<br>
    <div class="container">
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-default" style="margin:0px">
					<div class="panel-body" style="padding:0px">
						<img class="img-responsive" src="../../img/head3.jpg" alt="">
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
	</div>

	<br>
    <!-- Page Content -->
    <div class="container">

        <div class="row">
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
			<div class="col-sm-10 col-lg-10 col-md-10">
			
				<nav class="navbar navbar-default" role="navigation" style="text-transform:uppercase; border-color:#E7E7E7; background-color: #9C0707">
					<!-- El logotipo y el icono que despliega el menú se agrupan
					para mostrarlos mejor en los dispositivos móviles -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Desplegar navegación</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" style="padding:7px" href="../index.php"><img class="img-responsive" src="../../images/inicio.jpg" alt=""></a>
					</div>
					<!-- Agrupar los enlaces de navegación, los formularios y cualquier
					otro elemento que se pueda ocultar al minimizar la barra -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav">
							<li><a href="../index.php">INICIO</a></li>
							<li><a href="../pedido/index.php">COMPRAR</a></li>
							<li class="active"><a href="../historial/index.php">MIS COMPRAS</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									PERFIL DE USUARIO <b class="caret"></b>
								</a>
								<ul class="dropdown-menu">
									<li><a href="../perfil/index.php">Ver Perfil</a></li>
									<li class="divider"></li>
									<li><a href="../salirView.php">Cerrar Sesión</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>

			</div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>				


			<div class="col-sm-12 col-lg-12 col-md-12"></div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
			<div class="col-sm-5 col-lg-5 col-md-5">
				
				<p style="margin-top:17px">
  					Bienvenido(a), <a href="usuario/perfil/index.php" class="navbar-link"><?php echo $_SESSION['AL_NombreApellido']; ?></a>
				</p>
			</div>
			<div class="col-sm-5 col-lg-5 col-md-5"></div>
		
			<div class="col-sm-1 col-lg-1 col-md-1"></div>

			<div class="col-sm-12 col-lg-12 col-md-12"></div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>		
			<div class="col-sm-10 col-lg-10 col-md-10">
			<div class="panel panel-primary">
				<div class="panel-body">
					<div class="BlancoGris" align="left" style="margin-left:0px; margin-right:0px">
						Listados de Mercados que ha realizado:
					</div>
					<?php if ($cantRS>0){ ?>
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr align="center" style="background-color: #9C0707; color: #FFF">
									<td style="vertical-align:middle;">Fecha del<br> Mercado</th>
									<td style="vertical-align:middle;">Mercado</th>
									<td style="vertical-align:middle;">Orden de Compra</th>
									<td style="vertical-align:middle;">Cant.<br>Prod.</th>
									<td style="vertical-align:middle;">Monto Bruto</th>
									<td style="vertical-align:middle;">Nota de<br> Crédito</th>
									<td style="vertical-align:middle;">Monto a Pagar</th>
									<td style="vertical-align:middle;" colspan="2">Opciones</th>
									
									<!--<td>BORRAR</th>-->
								</tr>
							</thead>
							<tbody align="center">
								<?php
								for($i=0; $i<$cantRS; $i++){
									$FE_FechaMercado 		= $objConexion->obtenerElemento($RS,$i,'NU_IdUsuario');
									$FE_Fin 				= $objConexion->obtenerElemento($RS,$i,'FE_Fin');
									$mercado_NU_IdMercado	= $objConexion->obtenerElemento($RS,$i,'mercado_NU_IdMercado');			
									$NU_IdPedido 			= $objConexion->obtenerElemento($RS,$i,'NU_IdPedido');			
									$CantProductos 			= $objConexion->obtenerElemento($RS,$i,'CantProductos');
									$MontoBruto 			= $objConexion->obtenerElemento($RS,$i,'MontoBruto');			
									$BS_NotaCredito			= $objConexion->obtenerElemento($RS,$i,'BS_NotaCredito');
									$MontoPagar 			= $MontoBruto - $BS_NotaCredito;
									$AF_CodPedido 			= $objConexion->obtenerElemento($RS,$i,'AF_CodPedido');
								?>
								<tr>
									<td style="vertical-align:middle;"><?php echo setFechaNoSQL($FE_FechaMercado); ?></td>
									<td style="vertical-align:middle;"><?php echo 'DGRH-GBS-M0'.$mercado_NU_IdMercado; ?></td>
									<td style="vertical-align:middle;"><?php echo $AF_CodPedido; ?></td>
									<td style="vertical-align:middle;"><?php echo $CantProductos; ?></td>
									<td style="vertical-align:middle;"><?php echo number_format($MontoBruto,2,',','.').' BsF.'; ?></td>
									<td style="vertical-align:middle;"><?php echo number_format($BS_NotaCredito,2,',','.').' BsF.'; ?></td>
									<td style="vertical-align:middle;"><?php echo number_format($MontoPagar,2,',','.').' BsF.'; ?></td>
									<td style="vertical-align:middle;"><a href="../pedido/orden_compra.php?pedido_NU_IdPedido=<?php echo $NU_IdPedido; ?>" target="_blank"><img src="../../images/ver.png" alt=""/></a></td>
									<td style="vertical-align:middle;">
										<?php if (date("Y-m-d") <= $FE_Fin){ ?>
										<a href="editar.php?NU_IdPedido=<?php echo $NU_IdPedido; ?>">
										<img src="../../images/edit.png"></a>
										<?php } else { ?>
										<a href="download.php?f=Mercado-DGRH-GBS-M0<?php echo $mercado_NU_IdMercado; ?>.pdf">
										<img src="../../images/descarga.jpg"></a>
										<?php } ?>
									</td>
									<!--<td align="center" class="TablaRojaGridTD">
										<a href="index.php?mensaje='En Construccion'">
										<img src="../../images/bton_del.gif" width="35" height="31"></a>
									</td>-->
								</tr>
								<?php
								}
								?>          
							</tbody>
						</table>
					</div>
					<?php
					}else{ echo '<tr align="center"><td>No se encontraron registros.</td></tr>'; 
					}
					?>
				</div>
			</div>		
			</div>
			<div class="col-sm-1 col-lg-1 col-md-1"></div>
		</div>

	
    <!-- /.container -->

        <!-- Footer -->
	
       <footer>
        <div class="row">
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>
            <div class="col-sm-10 col-lg-10 col-md-10">
            	<div class="panel panel-primary">
					<div class="panel-body">
						<p align="center"><strong>
						Venezolana de Alimentos La Casa - &copy;2015<br>
						Oficina de Tecnología de la Información
						</strong></p>
					</div>
				</div>
            </div>			
            <div class="col-sm-1 col-lg-1 col-md-1">

            </div>			
			
		</div>
        </footer>


    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>

</body>

</html>
