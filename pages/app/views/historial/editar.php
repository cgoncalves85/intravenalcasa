﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/mercadoProductoModel.php'); 	
	require_once('../../model/pedidoModel.php'); 		
	require_once('../../model/pedidoDetalleModel.php'); 
	
	$objPedido = new Pedido();

	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}

	$objMercadoProducto	= new MercadoProducto();
	$objPedido	 		= new Pedido();	
	$objpedidoDetalle	= new PedidoDetalle();
	
	$NU_IdPedido = $_GET['NU_IdPedido'];

	$RSPedido 		= $objPedido->buscar($objConexion,$NU_IdPedido);
	
	$NU_IdMercado		= $objConexion->obtenerElemento($RSPedido,0,"mercado_NU_IdMercado");
	$AL_AutorizoNombre	= $objConexion->obtenerElemento($RSPedido,0,"AL_AutorizoNombre");	
	$AL_AutorizoCedula	= $objConexion->obtenerElemento($RSPedido,0,"AL_AutorizoCedula");
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>
	
	<script>
		$(document).ready(function() {
			
			var default_message_for_dialog='<p align="center"><b>LISTA DE PRODUCTOS </b></p>';
			//var default_message_for_dialog = nombre;
			
			$("#dialog").dialog({
				show: "blind",
				hide: "explode",		
				modal: true,
				bgiframe: true,
				width: 320,
				height: 460,
				autoOpen: false,
				title: 'Confirme su Compra'
				});

			// FORMS
			$('input.confirm').click(function(theINPUT){
				theINPUT.preventDefault();
				var precio = 0;
				var cantidad = 0;
				var valor = 0;
				var indice = 0;
				var total = 0;
				var res2 = '';
				default_message_for_dialog += '<p align="left">';
		<?php 
			$rsMercadoProducto2		= $objMercadoProducto->listarMercadoProducto($objConexion,$NU_IdMercado);
			$cantMercadoProducto2	= $objConexion->cantidadRegistros($rsMercadoProducto2);
			
			for($a=0;$a<$cantMercadoProducto2;$a++){
		?>
				indice = document.form.NU_Cantidad<?php echo $a; ?>.selectedIndex;
				cantidad = document.form.NU_Cantidad<?php echo $a; ?>.options[indice].value;
				
				if (cantidad!=0){
		<?php			
					$AF_NombreProducto2	= $objConexion->obtenerElemento($rsMercadoProducto2,$a,"AF_NombreProducto");	
					$BS_PrecioUnitario2	= $objConexion->obtenerElemento($rsMercadoProducto2,$a,"BS_PrecioUnitario");
		?>

					precio = cantidad * parseFloat(<?php echo $BS_PrecioUnitario2; ?>);
					precio = ''+precio+'';
					res2 = precio.replace(".",",");			
					default_message_for_dialog += '- '+'(<b>'+cantidad+'</b>) <?php echo $AF_NombreProducto2; ?> = <b>'+res2+' Bsf.</b> </br>';
					
					total = parseFloat(total) + parseFloat(precio);
				}
		<?php
				
			} 
		?>
				default_message_for_dialog += '</p>';
				
				var total = ''+total+'';
				//var totalres = total.toFixed(3);
				var res = total.replace(".",",");
				

				default_message_for_dialog +='<b>TOTAL A PAGAR = '+res+' BsF.</b></br></br>';
				default_message_for_dialog +='<b style="color:#F00">Si confirma esta compra, estará aceptando que el monto total a pagar sea descontado de su próxima quincena.</b>';
						
				var theFORM = $(theINPUT.target).closest("form");
				var theREL = $(this).attr("rel");
				var theMESSAGE = (theREL == undefined || theREL == '') ? default_message_for_dialog : theREL;
				var theICON = '<span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 0 0;"></span>';
				
				$('#dialog').html('<P align="left">' + theICON + theMESSAGE + '</P>');
				$("#dialog").dialog('option', 'buttons', {
						"Confirmar" : function() {
							theFORM.submit();
							$(this).dialog("close");
							},
						"Cancelar" : function() {
							default_message_for_dialog = 'Lista de Productos: </br></br>';
							$(this).dialog("close");
							}
						});
				$("#dialog").dialog("open");
				});

		});
	</script>	

</head>

<body>
	<div id="dialog" title="Atención !!" style="display:none;">
		<p>En Construccion!!.</p>
	</div>
    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Modificar Compras</h4>
						<div class="panel panel-default">
							<div class="panel-heading">
								Modifique las cantidades que desea comprar.
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-10">	
										<br>
										<form name="form" id="form" method="post" action="../../controller/pedidoController.php" target="_blank">								
										
											<?php 
											$k=0;
											$rsMercadoProducto		= $objMercadoProducto->listarMercadoProducto($objConexion,$NU_IdMercado);
											$cantMercadoProducto	= $objConexion->cantidadRegistros($rsMercadoProducto);
							
											for($i=0;$i<$cantMercadoProducto;$i++){
												$NU_IdProducto		= $objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_IdProducto");						
												$AF_NombreProducto	= $objConexion->obtenerElemento($rsMercadoProducto,$i,"AF_NombreProducto");
												$AL_Medida			= $objConexion->obtenerElemento($rsMercadoProducto,$i,"AL_Medida");
												$NU_Contenido		= number_format($objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_Contenido"),3,',','.');
												$BS_PrecioUnitario	= $objConexion->obtenerElemento($rsMercadoProducto,$i,"BS_PrecioUnitario");
												$NU_Max				= $objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_Max");
												$NU_Min				= $objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_Min");						  
												$NU_Salto			= $objConexion->obtenerElemento($rsMercadoProducto,$i,"NU_Salto");						  
												$AF_Foto			= $objConexion->obtenerElemento($rsMercadoProducto,$i,"AF_Foto");
								  
												if ($k==0){ echo "<div class='col-sm-3 col-lg-3 col-md-3'>"; }
												?>
												
												<?php
													$RSPedidoDetalle	= $objpedidoDetalle->buscarProducPedido($objConexion,$NU_IdPedido,$NU_IdProducto);
													$cantPedidoDetalle	= $objConexion->CantidadRegistros($RSPedidoDetalle);
								
													if ($cantPedidoDetalle>0){
														$NU_Cantidad 		= $objConexion->obtenerElemento($RSPedidoDetalle,0,"NU_Cantidad");
														$NU_IdPedidoDetalle = $objConexion->obtenerElemento($RSPedidoDetalle,0,"NU_IdPedidoDetalle");			
													}else{
														$NU_Cantidad 		= '';
														$NU_IdPedidoDetalle = '';
													}
												?>
												
												<img src="../../images/producto/<?php if ($AF_Foto==''){ echo 'sin_imagen.jpg'; }else{ echo $AF_Foto; } ?>" width="124" height="85"  alt="" style="border:solid 1px #CFCFCF"/>
												<br><br>
												<strong><?php echo $AF_NombreProducto; ?></strong></b><br>
												<?php 
													if($NU_IdProducto=='7'){ 
														echo 'Contenido: 1 Pedazo.'.'<br>';
														echo 'Precio: 29,09 BsF'.'<br>';
												?>
												<select name="<?php echo 'NU_Cantidad'.$i; ?>" id="<?php echo 'NU_Cantidad'.$i; ?>" style="width:50px">
													<!-- <option selected="selected" value="0">0</option> -->
													<?php for ($j=1; $j<=2; $j=$j+1){ 
													$carne = $NU_Cantidad / 2;
												?>
									
													<option <?php if ($carne==$j){ echo 'selected="selected"'; } ?> value="<?php echo $j; ?>"><?php echo $j; ?></option>
												<?php } ?>
												</select>						
												<hr>
												<?php
													}else{
														echo 'Contenido: '.$NU_Contenido.' '.$AL_Medida.'<br>';
														echo 'Precio: '.number_format($BS_PrecioUnitario,2,',','.').' BsF'.'<br>';
												?>
												<select name="<?php echo 'NU_Cantidad'.$i; ?>" id="<?php echo 'NU_Cantidad'.$i; ?>" style="width:50px">
													<!-- <option selected="selected" value="0">0</option> -->
													<?php for ($j=$NU_Min; $j<=$NU_Max; $j=$j+$NU_Salto){ ?>
													<option <?php if ($NU_Cantidad==$j){ echo 'selected="selected"'; } ?> value="<?php echo $j; ?>"><?php echo $j; ?></option>
													<?php } ?>
												</select>
												<hr>
												<?php						
												} ?>

												<input name="<?php echo 'NU_IdProducto'.$i; ?>" type="hidden" id="<?php echo 'NU_IdProducto'.$i; ?>" value="<?php echo $NU_IdProducto; ?>">
												<input  name="<?php echo 'BS_PrecioUnitario'.$i; ?>" type="hidden" id="<?php echo 'BS_PrecioUnitario'.$i; ?>" value="<?php echo number_format($BS_PrecioUnitario,2,'.',','); ?>">
												<input name="<?php echo 'NU_Max'.$i; ?>" type="hidden" id="<?php echo 'NU_Max'.$i; ?>" value="<?php echo $NU_Max; ?>">
												<input name="<?php echo 'NU_IdPedidoDetalle'.$i; ?>" type="hidden" id="<?php echo 'NU_IdPedidoDetalle'.$i; ?>" value="<?php echo $NU_IdPedidoDetalle; ?>">
												<?php $k++; ?>
												<?php 
												if ($k!=0){ echo '</div>'; $k=0; }
											}  ?>              
									
											<div class="col-sm-12 col-lg-12 col-md-12" style="padding:0px">
												
												<div class="BlancoGris" align="left">
													Si usted desea autorizar a otra persona con el fin de retirar su compra, llene el siguiente formulario:
												</div>
												<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
												<div class="form-group" align="left">
													<div class="col-sm-5 col-lg-5 col-md-5">
														<label for="autorizado">Nombre de quién retira:</label><br>
														<input type="text" name="AL_AutorizoNombre" id="AL_AutorizoNombre" value="<?php echo $AL_AutorizoNombre; ?>" class="form-control input-sm">
													</div>
													
													<div class="col-sm-5 col-lg-5 col-md-5">
														<label for="cedula_autorizado">N° Ced. de quién retira:</label><br>
														<input type="number" name="AL_AutorizoCedula" id="AL_AutorizoCedula" value="<?php echo $AL_AutorizoCedula; ?>" class="form-control input-sm">
														
													</div>	
												</div>
												<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
												
												<div class="col-sm-12 col-lg-12 col-md-12">
													<hr>
													<div class="col-sm-9 col-lg-9 col-md-9" align="right" style="padding:0px">											
														<input name="Comprar" type="submit" class="confirm"  value="Guardar Cambios" id="Comprar">
													</div>
													<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
													<div class="col-sm-1 col-lg-1 col-md-1" align="right" style="padding:0px">
														<input name="button2" type="button"  id="button2" value="Cancelar" onClick="javascript:window.location='../index.php'" />
														<br><br>
													</div>
													<div class="col-sm-1 col-lg-1 col-md-1">&nbsp;</div>
													<input name="origen" type="hidden" id="origen" value="pedido_edit">
													<input name="cantProducto" type="hidden" id="cantProducto" value="<?php echo $cantMercadoProducto; ?>">
													<input name="NU_IdPedido" type="hidden" id="NU_IdPedido" value="<?php echo $NU_IdPedido; ?>">
												</div>
											</div>
										</form>
									</div>
									<div class="col-lg-1"></div>
								</div>
							</div>
						</div>

					</div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
