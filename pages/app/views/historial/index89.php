<?php 
require_once('../../controller/sessionController.php'); 
require_once('../../model/pedidoModel.php');

$objPedido = new Pedido();

$RS 	= $objPedido->listarPedidoIndiv($objConexion,$_SESSION["NU_IdUsuario"]);
$cantRS = $objConexion->cantidadRegistros($RS);

	///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
	function setFechaNoSQL($FE_FechaNac)
	{
		$partes = explode("-", $FE_FechaNac);
		$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
		return $FE_FechaNac;
	}
	//////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Christian Goncalves">

    <title>Mercado Corporativo</title>

    <!-- Favicon ICO -->
	<link rel="shortcut icon" href="../images/favicon.ico"/>
	
	<!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/shop-homepage1.css" rel="stylesheet">
    <link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom JavaScript -->
   <script type="text/javascript" src="../js/jquery.js"></script>
   <script type="text/javascript" src="../js/jquery-ui.js"></script>
   <script type="text/javascript" src="../js/maximizar.js"></script>
   <script type="text/javascript" src="../js/desconectar.js"></script>
<script type="text/javascript">
    function abrir_dialog() {
		var mensaje = "<?php echo $_GET['mensaje']; ?>";
		if(mensaje){
		  $( "#dialog" ).dialog({
			  show: "blind",
			  hide: "explode",
			  modal: true,
			  buttons: {
				Aceptar: function() {
				  $( this ).dialog( "close" );
				}
			  }
		  });
		}
    };
</script>
</head>
<body onLoad="abrir_dialog();">
<div id="dialog" title="Mensaje" style="display:none;">
    <p><?php echo $_GET['mensaje']; ?></p>
</div>
<div class="container">
	<div class="row" >
		<div class="col-sm-12 col-lg-12 col-md-12">

			<div class="panel-body">
			<?php if ($cantRS>0){ ?>
				<div>
					<table class="table table-bordered">
						<thead align="center" style="background-color: #9C0707; color: #FFF">
							<tr>
								<td>DIA DEL<br>MERCADO</th>
								<td>NRO.<br>MERCADO</th>
								<td>ORDEN DE<br>COMPRA</th>
								<td>CANTIDAD<br>PRODUCTOS</th>
								<td>MONTO<br>BRUTO</th>
								<td>NOTA DE<br>CREDITO</th>
								<td>MONTO A<br>PAGAR</th>
								<td>VER</th>
								<td>EDITAR</th>
								<!--<td>BORRAR</th>-->
							</tr>
						</thead>
						<tbody align="center">
							<?php
							for($i=0; $i<$cantRS; $i++){
								$FE_FechaMercado 		= $objConexion->obtenerElemento($RS,$i,'FE_FechaMercado');
								$FE_Fin 				= $objConexion->obtenerElemento($RS,$i,'FE_Fin');
								$mercado_NU_IdMercado	= $objConexion->obtenerElemento($RS,$i,'mercado_NU_IdMercado');			
								$NU_IdPedido 			= $objConexion->obtenerElemento($RS,$i,'NU_IdPedido');			
								$CantProductos 			= $objConexion->obtenerElemento($RS,$i,'CantProductos');
								$MontoBruto 			= $objConexion->obtenerElemento($RS,$i,'MontoBruto');			
								$BS_NotaCredito			= $objConexion->obtenerElemento($RS,$i,'BS_NotaCredito');
								$MontoPagar 			= $MontoBruto - $BS_NotaCredito;
								$AF_CodPedido 			= $objConexion->obtenerElemento($RS,$i,'AF_CodPedido');
							?>
							<tr>
								<td><?php echo setFechaNoSQL($FE_FechaMercado); ?></td>
								<td><?php echo 'DGRH-GBS-M0'.$mercado_NU_IdMercado; ?></td>
								<td><?php echo $AF_CodPedido; ?></td>
								<td><?php echo $CantProductos; ?></td>
								<td><?php echo number_format($MontoBruto,2,',','.').' BsF.'; ?></td>
								<td><?php echo number_format($BS_NotaCredito,2,',','.').' BsF.'; ?></td>
								<td><?php echo number_format($MontoPagar,2,',','.').' BsF.'; ?></td>
								<td><a href="../pedido/orden_compra.php?pedido_NU_IdPedido=<?php echo $NU_IdPedido; ?>" target="_blank"><img src="../../images/bton_ver.gif" width="31" height="31"  alt=""/></a></td>
								<td>
								<?php if (date("Y-m-d") <= $FE_Fin){ ?>
									<a href="editar.php?NU_IdPedido=<?php echo $NU_IdPedido; ?>">
									<img src="../../images/bton_edit.gif" width="35" height="31"></a>
								<?php } ?>   
								</td>
								<!--<td align="center" class="TablaRojaGridTD">
									<a href="index.php?mensaje='En Construccion'">
									<img src="../../images/bton_del.gif" width="35" height="31"></a>
								</td>-->
							</tr>
							<?php
							}
							?>          
						</tbody>
					</table>
				</div>
			<?php
				}else{ echo '<tr align="center"><td>No se encontraron registros.</td></tr>'; 
				}
			?>
			<div>		
		</div>
	</div>
</div>
</body>
</html>