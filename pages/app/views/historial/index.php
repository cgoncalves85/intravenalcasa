﻿<?php 
	require_once('../../controller/sessionController.php'); 
	require_once('../../model/pedidoModel.php');

	$objPedido = new Pedido();

	$RSUsuario	 = $objUsuario->buscarUsuario($objConexion,$_SESSION["NU_Cedula"]);
	$cantUsuario = $objConexion->CantidadRegistros($RSUsuario);
	if($cantUsuario>0){
		$rol 					= $objConexion->obtenerElemento($RSUsuario,0,"rol_NU_IdRol");	
		$BI_ASGestionMercado 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASGestionMercado");	
		$BI_ASParametrosSis 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_ASParametrosSis");	
		$BI_GPVerificarCompra 	= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPVerificarCompra");	
		$BI_GPNotaCredito 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPNotaCredito");
		$BI_GPConsultas 		= $objConexion->obtenerElemento($RSUsuario,0,"BI_GPConsultas");											
	}

	$RS 	= $objPedido->listarPedidoIndiv($objConexion,$_SESSION["NU_IdUsuario"]);
	$cantRS = $objConexion->cantidadRegistros($RS);

		///// CONVIERTE FECHA 04/07/1980 A 1980-07-04 (FORMATO MYSQL)
		function setFechaNoSQL($FE_FechaNac)
		{
			$partes = explode("-", $FE_FechaNac);
			$FE_FechaNac = $partes[2].'/'.$partes[1].'/'.$partes[0];
			return $FE_FechaNac;
		}
		//////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="../../css/jquery-ui.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
				$( "#dialog" ).dialog({
					show: "blind",
					hide: "explode",
					modal: true,
					buttons: {
						Aceptar: function() {
							$( this ).dialog( "close" );
						}
					}
				});
			}
		};
	</script>

</head>

<body>
	<div id="dialog" title="Atención !!" style="display:none;">
		<p>En Construccion!!.</p>
	</div>
    <div id="wrapper">

        <!-- Navigation -->
		<?php include "nav.php"; ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header">Mis Compras</h4>
						<div class="panel panel-default">
							<div class="panel-heading">
								Listados de Mercados que ha realizado.
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-1"></div>
									<div class="col-lg-10">	
										<br>
										<?php if ($cantRS>0){ ?>
										<div>
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead align="center">
													<tr>
										
														<td style="vertical-align:middle;" ><strong>Día del<br> Mercado</strong></td>
														<td style="vertical-align:middle;" ><strong>N° de Mercado</strong></td>
														<td style="vertical-align:middle;" ><strong>Orden de<br>Compra</strong></td>
														<td style="vertical-align:middle;" ><strong>Cant.<br>Prod.</strong></td>
														<td style="vertical-align:middle;" ><strong>Monto<br>Bruto</strong></td>
														<!-- <td style="vertical-align:middle;" ><strong>Nota de<br>Crédito</strong></td> -->
														<td style="vertical-align:middle;" ><strong>Monto a<br>Pagar</strong></td>
														<td style="vertical-align:middle;" ><strong>Ver</strong></td>	
														<td style="vertical-align:middle;" ><strong></strong></td>	
													</tr>
												</thead>
												<tbody align="center">
													<?php
														for($i=0; $i<$cantRS; $i++){
														$FE_FechaMercado 		= $objConexion->obtenerElemento($RS,$i,'FE_FechaMercado');
														$FE_Fin 				= $objConexion->obtenerElemento($RS,$i,'FE_Fin');
														$Tipo_Mercado			= $objConexion->obtenerElemento($RS,$i,'Tipo_Mercado');
														$mercado_NU_IdMercado	= $objConexion->obtenerElemento($RS,$i,'mercado_NU_IdMercado');			
														$NU_IdPedido 			= $objConexion->obtenerElemento($RS,$i,'NU_IdPedido');			
														$CantProductos 			= $objConexion->obtenerElemento($RS,$i,'CantProductos');
														$MontoBruto 			= $objConexion->obtenerElemento($RS,$i,'MontoBruto');			
														$BS_NotaCredito			= $objConexion->obtenerElemento($RS,$i,'BS_NotaCredito');
														$MontoPagar 			= $MontoBruto - $BS_NotaCredito;
														$AF_CodPedido 			= $objConexion->obtenerElemento($RS,$i,'AF_CodPedido');
													?>
													<tr>
														<td><?php echo setFechaNoSQL($FE_FechaMercado); ?></td>
														<td><?php echo 'DGRH-GBS-M0'.$mercado_NU_IdMercado; ?></td>
														<td><?php echo $AF_CodPedido; ?></td>
														<td><?php echo $CantProductos; ?></td>
														<td><?php echo number_format($MontoBruto,2,',','.').' BsF.'; ?></td>
														<!-- <td><?php echo number_format($BS_NotaCredito,2,',','.').' BsF.'; ?></td> -->
														<td><?php echo number_format($MontoPagar,2,',','.').' BsF.'; ?></td>
														<td><a href="../pedido/orden_compra.php?pedido_NU_IdPedido=<?php echo $NU_IdPedido; ?>" target="_blank"><span class="glyphicon glyphicon-search" title="Ver Mercado"></span></a></td>
														<td>
															<?php if (date("Y-m-d") <= $FE_Fin){ ?>
																<a href="editar.php?NU_IdPedido=<?php echo $NU_IdPedido; ?>">
																<span class="glyphicon glyphicon-edit" title="Editar Mercado"></span></a>
															<?php } ?>   
														</td>
													</tr>
													<?php
													}
													?>          
												</tbody>
											</table>
										</div>
										<?php
											}else{ echo '<tr align="center"><td>No se encontraron registros.</td></tr>'; 
											}
										?>	
									</div>
									<div class="col-lg-1"></div>
								</div>
							</div>
						</div>

					</div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../dist/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

</body>

</html>
