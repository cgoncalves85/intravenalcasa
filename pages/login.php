<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IntraVenalcasa">
    <meta name="author" content="Christian Goncalves">

    <title>IntraVenalcasa - Venezolana de Alimentos La Casa S.A.</title>

    <!-- Bootstrap Core CSS -->
    <link href="app/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="app/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="app/dist/css/sb-admin-2.css" rel="stylesheet">
	<link href="app/css/jquery-ui.css" rel="stylesheet" />
	<link rel="shortcut icon" href="app/images/favicon.ico"/>

    <!-- Custom Fonts -->
    <link href="app/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<script type="text/javascript">
		function abrir_dialog() {
			var mensaje = "<?php echo $_GET['mensaje']; ?>";
			if(mensaje){
			$( "#dialog" ).dialog({
				show: "blind",
				hide: "explode",
				modal: true,
				buttons: {
					Aceptar: function() {
					$( this ).dialog( "close" );
					}
				}
			});
			}
		};
	</script>	

</head>

<body onLoad="abrir_dialog();">
<div class="img-responsive"  id="dialog" title="Atención !!" style="display:none;">
    <p align="justify"><?php if (isset($_GET['mensaje'])){ echo $_GET['mensaje']; } ?></p>
</div>

<div class="container" style="margin-top:100px">
<div class="row">
	<div class="col-lg-4 col-lg-offset-4">
	<div class="panel panel-default">
    
		<div class="panel-body">
			<form role="form" method="POST" action="app/controller/autenticarController.php" >
			<fieldset>
				<div class="row">
					
					<div align="center"><img class="profile-img" src="app/images/intrave.png" class="img-responsive" alt=""> </div>
					
					<hr>
				</div>
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="form-group">
							<label>Número de Cédula:</label>
							<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-user"></i> </span>
								<input class="form-control" name="NU_Cedula" required id="NU_Cedula" size="15" type="text" autofocus>
							</div>
						</div>
						<div class="form-group">
							<label>Contraseña:</label>
							<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-lock"></i> </span>
								<input class="form-control" name="AF_Clave" required id="AF_Clave" size="15" type="password">
							</div>
						</div>
						<label>Codigo:</label>
						<img class="img-responsive"	src="app/includes/captcha/securimage_show.php?sid=<?php echo md5(uniqid(time())); ?>" style="border:solid 1px #b3b1b1">
						<br>
						<div class="form-group">
							<div class="input-group"> <span class="input-group-addon"> <i class="glyphicon glyphicon-pencil"></i> </span>
								<input class="form-control" name="code" required id="code" size="15" type="password" value="" placeholder="Escriba el Código">
							</div>
						</div>
						<div class="form-group">
							<div align="left">
								<a href="app/views/usuario/recuperacion/index.php"  />Recuperar Clave ?<br>
								<a href="app/views/usuario/crear/index.php"  />Registrarse !!
							</div>
							<div align= "right">
								<button type="submit" name="submit" id="submit" class="btn btn-primary">ENTRAR</button>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			</form>
		</div>
    </div>
	</div>
</div>

    <!-- jQuery -->
    <script src="app/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="app/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="app/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="app/dist/js/sb-admin-2.js"></script>
	
	<script type="text/javascript" src="app/js/jquery-ui.js"></script>
	<script type="text/javascript" src="app/js/maximizar.js"></script>
	<script type="text/javascript" src="app/js/desconectar.js"></script>

</body>

</html>
